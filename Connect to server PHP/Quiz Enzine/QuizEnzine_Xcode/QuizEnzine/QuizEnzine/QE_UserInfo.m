//
//  QE_UserInfo.m
//  QuizEnzine
//
//  Created by Techmaster on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QE_UserInfo.h"

@implementation QE_UserInfo
@synthesize facebookID = _facebookID;
@synthesize username = _username;
@synthesize link = _link;
@synthesize first_name = _first_name;
@synthesize last_name = _last_name;
@synthesize email = _email;
@synthesize gender = _gender;
@synthesize birthday = _birthday;
@synthesize picture = _picture;
@synthesize timezone = _timezone;
@synthesize numberFollower = _numberFollower;
@synthesize numberFollowing = _numberFollowing;

+(QE_UserInfo *)sharedUserInfo{
    static QE_UserInfo *sharedSingleton;
    
    @synchronized(self)
    {
        if (!sharedSingleton)
            sharedSingleton = [[QE_UserInfo alloc] init];
        
        return sharedSingleton;
    }
}
-(id)initWithId:(NSString *)fbid{
    self = [super init];
    if (self ){
        self.facebookID = fbid;
    }
    return self;
}

-(NSString *)fullname{
    return [NSString stringWithFormat:@"%@ %@",self.first_name, self.last_name];
}

#pragma mark - encapsulation
-(NSDictionary *)getDictionary{
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    [result setValue:self.facebookID forKey:@"facebookID"];
    [result setValue:self.username forKey:@"username"];
    [result setValue:self.link forKey:@"link"];
    [result setValue:self.first_name forKey:@"first_name"];
    [result setValue:self.last_name forKey:@"last_name"];
    [result setValue:self.email forKey:@"email"];
    [result setValue:self.gender forKey:@"gender"];
    [result setValue:self.birthday forKey:@"birthday"];
    [result setValue:self.picture forKey:@"picture"];
    [result setValue:[NSNumber numberWithInt:self.timezone] forKey:@"timezone"];
    return result;
}
@end
