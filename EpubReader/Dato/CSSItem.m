//
//  CSSItem.m
//  nexto
//
//  Created by Techmaster on 7/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//#define DEBUGX
#import "CSSItem.h"

@implementation CSSItem
@synthesize                        cssString = _cssString;
@synthesize                 replacementBlock = _replacementBlock;

-(id)initWithCSSString:(NSString*)cssString 
      replacementBlock:(CSSReplacementBlock)block
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {        
        self.cssString = cssString;
        self.replacementBlock = block;
    }
    return self;
}

-(NSString *)makeReplacement{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.replacementBlock != nil) {
        NSMutableDictionary *replacedDic = self.replacementBlock();
        if (replacedDic != nil) {
            NSMutableString *cssStringReplaced = [self.cssString mutableCopy];
            for (NSString *key in [replacedDic allKeys]) {
                id value = [replacedDic valueForKey:key];
#ifdef DEBUGX
                NSLog(@"key %@ and value:%@",key,value);
#endif
                if ([value isKindOfClass:[NSString class]]) {
                    NSRange rangeReplaced = NSMakeRange(0, cssStringReplaced.length);
                    [cssStringReplaced replaceOccurrencesOfString:key 
                                                       withString:value
                                                          options:NSLiteralSearch 
                                                            range:rangeReplaced];
                }
            }
            return cssStringReplaced;
        }
    }
    
    if (self.cssString != nil) {
        return self.cssString;
    }else {
        return @"";
    }
}
@end
