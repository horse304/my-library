//
//  ChaptersListPopover.h
//  nexto
//
//  Created by Dato - Techmaster.vn on 2012-07-27.
//  Copyright 2012 Dato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"
#import "EpubTOCItem.h"

@protocol ChaptersListDelegate

- (void)chapterSelected:(EpubTOCItem *)number;

@end

@interface ChaptersListPopover : UIViewController
{
	Book *book;
	
	NSArray *chaptersList;
    id<ChaptersListDelegate> delegate;
	
	UITableView *table;
}

@property (nonatomic, assign) Book *book;

@property (nonatomic, retain) NSArray *chaptersList;
@property (nonatomic, retain) NSIndexPath *currentTOCIndexPath;
@property (nonatomic, retain) EpubTOCItem *currentTOCItem;
@property (assign, nonatomic) int currentTOCIndexLinear;
@property (nonatomic, assign) id<ChaptersListDelegate> delegate;

@property (nonatomic, assign) IBOutlet UITableView *table;

@end