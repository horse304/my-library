//
//  ViewController.h
//  temp
//
//  Created by Jake Gundersen on 10/14/11.
//  Copyright (c) 2011 University of Utah Hospitals and Clinics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreVideo/CoreVideo.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>

@interface ViewController : GLKViewController <AVCaptureVideoDataOutputSampleBufferDelegate> {
    AVCaptureSession *session;
    
    CIContext *coreImageContext;
    
    GLuint _renderBuffer;
}

@property (strong, nonatomic) EAGLContext *context;


@end
