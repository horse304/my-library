//
//  Stuff_FetchedResultsController.m
//  SexFunFacts
//
//  Created by Techmaster on 8/4/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "EntityResultsController.h"
@interface EntityResultsController()
@property (retain, nonatomic) NSString *entityName;
@property (retain, nonatomic) NSString *sortKey;

@end

@implementation EntityResultsController
@synthesize                 entityName = _entityName;
@synthesize                    sortKey = _sortKey;
@synthesize            entityPredicate = _entityPredicate;

#pragma mark - Initialize
-(id)initWithEntityName:(NSString *)entityName sortKey:(NSString *)sortKey ascending:(BOOL)ascending managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set predicate with language code
    NSPredicate *languagePredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ == \"%@\"", LANGUAGE_CODE_KEY, [[LanguageUtil instance] getLanguageNameByCurrentLanguageCode]]];
    
    [fetchRequest setPredicate:languagePredicate];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    NSLog(@"%@",sortDescriptor);
    
    [fetchRequest setSortDescriptors:sortDescriptors];
        
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    [NSFetchedResultsController deleteCacheWithName:entityName];
    self = [super initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:entityName];
    
	NSError *error = nil;
	if (![self performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    return nil;
	}else {
        self.entityName = entityName;
        self.sortKey = sortKey;
    }
    return self;
}

#pragma mark - Utilities
-(int)getCount{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    return [self.fetchedObjects count];
}

-(BOOL)updateFetchRequest{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif    
    [NSFetchedResultsController deleteCacheWithName:self.entityName];
    
    // Set predicate with language code
    NSPredicate *languagePredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ == \"%@\"", LANGUAGE_CODE_KEY, [[LanguageUtil instance] getLanguageNameByCurrentLanguageCode]]];
    
    if (self.entityPredicate != nil) {        
        NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:languagePredicate, self.entityPredicate, nil]];
        [self.fetchRequest setPredicate:compoundPredicate];
    }else {
        [self.fetchRequest setPredicate:languagePredicate];
    }
    
    return [self performFetch:nil];
}

#pragma mark - Encapsulatioin
-(void)setEntityPredicate:(NSPredicate *)entityPredicate{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (_entityPredicate != entityPredicate) {
        _entityPredicate = entityPredicate;
        [self updateFetchRequest];
    }
}

@end
