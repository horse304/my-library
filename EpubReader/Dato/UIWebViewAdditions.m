//
//  UIWebViewAdditions
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//

#import "UIWebViewAdditions.h"

@implementation UIWebView(UIWebViewAdditions)

- (CGSize)windowSize {
    CGSize size;
    size.width = [[self stringByEvaluatingJavaScriptFromString:@"window.innerWidth"] integerValue];
    size.height = [[self stringByEvaluatingJavaScriptFromString:@"window.innerHeight"] integerValue];
    return size;
}

- (CGPoint)scrollOffset {
    CGPoint pt;
    pt.x = [[self stringByEvaluatingJavaScriptFromString:@"window.pageXOffset"] integerValue];
    pt.y = [[self stringByEvaluatingJavaScriptFromString:@"window.pageYOffset"] integerValue];
    return pt;
}

@end