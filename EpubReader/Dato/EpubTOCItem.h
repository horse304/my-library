//
//  EpubChapter.h
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//


#import <Foundation/Foundation.h>
#import "EpubUtils.h"
@class EpubTOCItem;
typedef void(^browseTOCBlock)(EpubTOCItem *tocItem, int depth);

@interface EpubTOCItem : NSObject <NSCoding>
{
    int pageNumber_Landscape;
    int pageNumber_Portrait;
}
@property (retain, nonatomic) NSString *chapterTitle;
@property (retain, nonatomic) NSURL *chapterURL;
@property (unsafe_unretained, nonatomic) EpubTOCItem *parent;
@property (retain, nonatomic) NSMutableArray *subChapters;
@property (assign, nonatomic) int pageNumber;

-(id)initWithChapterTitle:(NSString*)title chapterURL:(NSURL*)url;
-(void)browseTOCTreeWithBlock:(browseTOCBlock)browseBlock;
-(void)copyPageNumberFrom:(EpubTOCItem *)tocItem;
@end
