//
//  PolaroidImageView.h
//  SexFunFacts
//
//  Created by Techmaster on 8/7/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <UIKit/UIKit.h>
#define POLAROID_BACKGROUND_STANDARD @"polaroid_background_standard"

@interface PolaroidImageView : UIView

typedef enum{
    Standard = 0
}PolaroidStyle;

@property (retain, nonatomic) UIImage *innerImage;
@property (assign, nonatomic) PolaroidStyle polaroidStyle;

-(id)initWithStyle:(PolaroidStyle) polaroidStyle frame:(CGRect)frame;
@end
