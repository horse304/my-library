//
//  qeAppDelegate.h
//  QuizEnzine
//
//  Created by Techmaster on 6/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QE_MainController.h"
#import "QE_VCSplash.h"
#import "QE_TBCMain.h"

@class qeViewController;

@interface qeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
