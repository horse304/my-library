//
//  DATCoreImage.h
//  CareCapsule
//
//  Created by Dato on 6/2/12.
//  Copyright (c) 2012 TechMaster.vn. All rights reserved.
//
#import <CoreImage/CoreImage.h>
#import <Foundation/Foundation.h>

@interface DATCoreImage : NSObject
@property (retain, nonatomic) CIContext *cicontext;
@property (retain, nonatomic) NSArray *filterArray;

-(CGImageRef)outputCGImageOfFilter:(CIFilter *)filter;
-(UIImage *)outputUIImageOfFilter:(CIFilter *)filter;
@end
