//
//  QE_VCSplash.m
//  QuizEnzine
//
//  Created by Techmaster on 6/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QE_VCSplash.h"

@interface QE_VCSplash ()
//User Interface Properties

//User Interface Action
- (IBAction)doLogin:(id)sender;
//Class Properties
@end

@implementation QE_VCSplash

- (id)init
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {        
        self = [super initWithNibName:@"QE_VCSplash_iPhone" bundle:nil];
    } else {
        self = [super initWithNibName:@"QE_VCSplash_iPad" bundle:nil];
    }
    return self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
- (void)viewWillAppear:(BOOL)animated{
    
    
}
- (void)viewDidAppear:(BOOL)animated{
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

#pragma mark - Rotation handling
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
}

#pragma mark - User Interface Action
- (IBAction)doLogin:(id)sender {
    QE_VCLogin *vcLogin = [[QE_VCLogin alloc] init];
    [self presentModalViewController:vcLogin animated:YES];
}
@end
