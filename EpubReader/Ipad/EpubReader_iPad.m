//
//  EpubReader_iPad.m
//  nexto
//
//  Created by Dato - Techmaster.vn on 2012-07-27.
//  Copyright 2012 Dato. All rights reserved.
//

#define DEBUGX
#define FILES_DIRECTORY_NAME @"files"

#define HTML_PADDING 45.0
#define BODY_MARGIN_MAXIMUM 100
#define BODY_MARGIN_MINIMUM 20
#define BODY_MARGIN_STEP_CHANGE 5
#define BODY_FONTZOOM_MAXIMUM 200
#define BODY_FONTZOOM_MINIMUM 30
#define BODY_FONTZOOM_STEP_CHANGE 10
/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#import "EpubReader_iPad.h"
#import "ShopItem_iPad.h"
#include <sys/xattr.h>

@interface EpubReader_iPad()
@property (assign, nonatomic) BOOL isFullscreen;
@property (assign, nonatomic) EpubReaderMode readerMode;
@property (retain, nonatomic) NSMutableDictionary *itemForDownload;
@property (assign, nonatomic) BOOL isSplash;
@property (retain, nonatomic) NSString *sharingText;
@property (assign, nonatomic) BOOL viewPushed;

//UI Properties
@property (strong, nonatomic) UIButton *settingBtn;
@property (strong, nonatomic) SettingsPopover *settingsPopoverController;
@property (strong, nonatomic) UIPopoverController *settingsPopover;
@property (strong, nonatomic) UIButton *tocBtn;
@property (strong, nonatomic) ChaptersListPopover *chapterListPopoverController;
@property (strong, nonatomic) UIPopoverController *chapterListsPopover;
@property (strong, nonatomic) UISlider *pageSlider;
@property (strong, nonatomic) UILabel *pageLabelInfo;
@property (strong, nonatomic) UIView *downloadIndicatorView;
@property (strong, nonatomic) UILabel *downloadIndicatorBookTitle;
@property (strong, nonatomic) UIProgressView *downloadIndicatorProgress;
@property (strong, nonatomic) UIButton *downloadIndicatorCancelBtn;
@property (strong, nonatomic) UIImageView *coverSplashImage;
@property (strong, nonatomic) EpubImageZooming *imgZooming;
@property (strong, nonatomic) EpubShareFacebook *shareFBVC;
@end

@implementation EpubReader_iPad
@synthesize                     isFullscreen = _isFullscreen;
@synthesize                         isSplash = _isSplash;
@synthesize                       readerMode = _readerMode;
@synthesize                  itemForDownload = _itemForDownload;
@synthesize                             book = _book;
@synthesize                       settingBtn = _settingBtn;
@synthesize        settingsPopoverController = _settingsPopoverController;
@synthesize                  settingsPopover = _settingsPopover;
@synthesize                           tocBtn = _tocBtn;
@synthesize                       pageSlider = _pageSlider;
@synthesize                    pageLabelInfo = _pageLabelInfo;
@synthesize            downloadIndicatorView = _downloadIndicatorView;
@synthesize       downloadIndicatorBookTitle = _downloadIndicatorBookTitle;
@synthesize        downloadIndicatorProgress = _downloadIndicatorProgress;
@synthesize       downloadIndicatorCancelBtn = _downloadIndicatorCancelBtn;
@synthesize                 coverSplashImage = _coverSplashImage;
@synthesize     chapterListPopoverController = _chapterListPopoverController;
@synthesize              chapterListsPopover = _chapterListsPopover;
@synthesize                       imgZooming = _imgZooming;
@synthesize                      sharingText = _sharingText;
@synthesize                        shareFBVC = _shareFBVC;
@synthesize                       viewPushed = _viewPushed;

- (id)initWithSetBook: (Book*)bookToOpen splash:(bool)isSplash
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super initWithEpubFilePath:bookToOpen.path];
    if (self) {
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateFormat:@"yyyy.MM.dd HH:mm:ss"];
        
        bookToOpen.downloadDate = [[dateFormatter stringFromDate:date] stringByAppendingString:@" a"];        
        [Library saveData];
        
        self.book = bookToOpen;
        self.readerMode = READ_EPUB_FROM_FILE;
        self.isSplash = isSplash;
        self.delegate = self;
#ifdef DEBUGX
        NSLog(@"file Type:%@",[bookToOpen.path pathExtension]);
#endif
        if (![[bookToOpen.path pathExtension] isEqualToString:@"epub"]) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Your file should be epub file! File:%@",[bookToOpen.path lastPathComponent]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            return nil;
        }
    }
    return self;
}
- (id)initWithDownloadBook:(NSMutableDictionary *)item{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super initWithEpubFilePath:nil];
    if (self) {
        self.itemForDownload = item;
        self.readerMode = READ_EPUB_FROM_URL;
        self.isSplash = YES;
        self.delegate = self;
    }
    return self;
}

#pragma mark - View Life Cycle
- (void)loadView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super loadView];
    
    //Add tab on UIWebview
    UITapGestureRecognizer *tapGestureOnWebView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnWebView:)];   
    tapGestureOnWebView.delegate = self;
    [self.theWebView addGestureRecognizer:tapGestureOnWebView];
    
    //Add longpress on uiwebview, pinch to zoom
    UILongPressGestureRecognizer *longGestureOnWebView = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longpressOnWebView:)];
    longGestureOnWebView.delegate = self;
    longGestureOnWebView.minimumPressDuration = 0.5;
    [self.theWebView addGestureRecognizer:longGestureOnWebView];
    UIPinchGestureRecognizer *pinchGestureOnWebView = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchOnWebView:)];
    pinchGestureOnWebView.scale = 1.5;
    pinchGestureOnWebView.delegate = self;
    [self.theWebView addGestureRecognizer:pinchGestureOnWebView];
    
    //Add facebook option on MenuController
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    UIMenuItem *facebookItem = [[UIMenuItem alloc] initWithTitle: @"Facebook" action: @selector(shareFacebook)];
    [menuController setMenuItems: [NSArray arrayWithObjects:facebookItem, nil]];
    
    //Add About button
    UIBarButtonItem *aboutButton = [[UIBarButtonItem alloc] initWithTitle:@"Opis" style:UIBarButtonItemStylePlain target:self action:@selector(touchOnAbout:)];         
    self.navigationItem.rightBarButtonItem = aboutButton;    
    
    //Add Setting Button
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {        
        _settingBtn = [[UIButton alloc] initWithFrame:CGRectMake(642, 5, 52, 32)];
    }else {
        _settingBtn = [[UIButton alloc] initWithFrame:CGRectMake(635, 5, 52, 32)];
    }
    [self.settingBtn setImage:[UIImage imageNamed:@"reader_settings.png"] forState:UIControlStateNormal];
    self.settingBtn.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.settingBtn addTarget:self action:@selector(touchOnSettingButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.settingBtn];
    
    //Add TOC Button
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
        _tocBtn = [[UIButton alloc] initWithFrame:CGRectMake(699, 5, 52, 32)];
    }else {
        _tocBtn = [[UIButton alloc] initWithFrame:CGRectMake(690, 5, 52, 32)];
    }
    [self.tocBtn setImage:[UIImage imageNamed:@"reader_chapter_list.png"] forState:UIControlStateNormal];
    self.tocBtn.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.tocBtn addTarget:self action:@selector(touchOnTOCButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.tocBtn];
            
    //Add changing page's slider and title label
    _pageSlider = [[UISlider alloc] initWithFrame:CGRectMake(34, 980, 700, 23)];    
    _pageSlider.center = CGPointMake(self.view.bounds.size.width/2, _pageSlider.center.y);
    self.pageSlider.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    self.pageSlider.minimumValue = 0.0;
    self.pageSlider.maximumValue = 1.0;
    self.pageSlider.value = 0.0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0")) {        
        self.pageSlider.minimumTrackTintColor = [UIColor colorWithRed:(160.0/255.0) green:(182.0/255.0) blue:(53.0/255.0) alpha:1.0];
    }
    [self.pageSlider addTarget:self action:@selector(sliderEnded) forControlEvents:UIControlEventTouchUpInside];
    [self.pageSlider addTarget:self action:@selector(sliderEnded) forControlEvents:UIControlEventTouchUpOutside];
    [self.pageSlider addTarget:self action:@selector(sliderStarted) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.pageSlider];
    
    _pageLabelInfo = [[UILabel alloc] initWithFrame:CGRectMake(84, 975, 600, 21)];
    _pageLabelInfo.center = CGPointMake(self.view.bounds.size.width/2, _pageLabelInfo.center.y);
    self.pageLabelInfo.autoresizingMask = self.pageSlider.autoresizingMask;
    self.pageLabelInfo.textAlignment = UITextAlignmentCenter;
    self.pageLabelInfo.textColor = [UIColor darkTextColor];
    self.pageLabelInfo.font = [UIFont fontWithName:@"Helvetica" size:12.0];
    self.pageLabelInfo.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    self.pageLabelInfo.minimumFontSize = 10.0;
    [self.view addSubview:self.pageLabelInfo];
    self.pageLabelInfo.text = @"Test";
    
    //Add Download Indicator
    self.downloadIndicatorView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.downloadIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.downloadIndicatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    [self.view addSubview: self.downloadIndicatorView];
    
    
    UIView *downloadAlertView = [[UIView alloc] initWithFrame:CGRectMake(100, 386, 567, 231)];    
    downloadAlertView.center = CGPointMake(self.view.bounds.size.width/2, downloadAlertView.center.y);
    downloadAlertView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    downloadAlertView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    [self.downloadIndicatorView addSubview:downloadAlertView];
    
    UIImageView *imgViewBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splashAlert.png"]];
    imgViewBg.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    imgViewBg.frame = downloadAlertView.bounds;
    [downloadAlertView addSubview:imgViewBg];
    
    UILabel *waitingMessage1 = [[UILabel alloc] initWithFrame:CGRectMake(82,16,398,45)];    
    waitingMessage1.center = CGPointMake(downloadAlertView.bounds.size.width/2, waitingMessage1.center.y);
    waitingMessage1.autoresizingMask = downloadAlertView.autoresizingMask;
    waitingMessage1.textAlignment = UITextAlignmentCenter;
    waitingMessage1.textColor = [UIColor whiteColor];
    waitingMessage1.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    waitingMessage1.font = [UIFont fontWithName:@"Helvetica-Bold" size:18.0];
    waitingMessage1.text = @"Trwa pobieranie ...";
    [downloadAlertView addSubview: waitingMessage1];
    
    _downloadIndicatorBookTitle = [[UILabel alloc] initWithFrame:CGRectMake(32,65,498,45)];    
    self.downloadIndicatorBookTitle.center = CGPointMake(downloadAlertView.bounds.size.width/2, self.downloadIndicatorBookTitle.center.y);
    self.downloadIndicatorBookTitle.autoresizingMask = downloadAlertView.autoresizingMask;
    self.downloadIndicatorBookTitle.textAlignment = UITextAlignmentCenter;
    self.downloadIndicatorBookTitle.textColor = [UIColor whiteColor];
    self.downloadIndicatorBookTitle.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    self.downloadIndicatorBookTitle.highlightedTextColor = [UIColor blackColor];
    self.downloadIndicatorBookTitle.font = [UIFont fontWithName:@"System" size:17.0];
    self.downloadIndicatorBookTitle.text = @"Tytuł audiobooka";
    [downloadAlertView addSubview: self.downloadIndicatorBookTitle];
    
    _downloadIndicatorProgress = [[UIProgressView alloc] initWithFrame:CGRectMake(208, 129, 150, 9)];    
    _downloadIndicatorProgress.center = CGPointMake(downloadAlertView.bounds.size.width/2, _downloadIndicatorProgress.center.y);
    self.downloadIndicatorProgress.autoresizingMask = downloadAlertView.autoresizingMask;
    self.downloadIndicatorProgress.progress = 0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0")) {        
        self.downloadIndicatorProgress.progressTintColor = [UIColor colorWithRed:(160.0/255.0) green:(182.0/255.0) blue:(53.0/255.0) alpha:1.0];
    }
    [downloadAlertView addSubview:self.downloadIndicatorProgress];
    
    _downloadIndicatorCancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(220, 158, 127, 43)];
    _downloadIndicatorCancelBtn.center = CGPointMake(downloadAlertView.bounds.size.width/2, _downloadIndicatorCancelBtn.center.y);
    _downloadIndicatorCancelBtn.autoresizingMask = downloadAlertView.autoresizingMask;
    [self.downloadIndicatorCancelBtn addTarget:self action:@selector(touchOnCancelDownload) forControlEvents:UIControlEventTouchUpInside];
    [self.downloadIndicatorCancelBtn setImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [downloadAlertView addSubview:self.downloadIndicatorCancelBtn];
    
    self.downloadIndicatorView.hidden = YES;
    
    //Add Cover Splash
    self.coverSplashImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.coverSplashImage.backgroundColor = [UIColor blackColor];
    self.coverSplashImage.contentMode = UIViewContentModeScaleAspectFit;
    self.coverSplashImage.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.coverSplashImage.hidden = YES;
    self.coverSplashImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapOnCoverSplash = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideCoverSplash)];
    tapOnCoverSplash.delegate = self;
    [self.coverSplashImage addGestureRecognizer:tapOnCoverSplash];
    [self.view addSubview:self.coverSplashImage];
}

- (void)viewDidLoad{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewDidLoad];
    self.isFullscreen = YES;
    self.navigationItem.title = self.book.title;
    
    //if in download mode
    if (self.readerMode == READ_EPUB_FROM_URL) {
        //Download epub
        dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(aQueue, ^{
            [self showDownloadIndicator];
            [self startDownload];
        });
    }else if (self.readerMode == READ_EPUB_FROM_FILE) {
    }
}

- (void)viewWillAppear:(BOOL)animated
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.navigationController.navigationBar.translucent = NO;
    
    //Save current page
    if (self.book != nil) {
        self.book.savedSpineIndex = self.currentSpineIndex;
        self.book.pageRatio = (float)self.currentPageIndex/(float)self.pageCountForCurrentSpine;
        if(self.isCalculatingPage){
            [self stopCaculatingPage];
            self.book.savedSpineArray = nil;
            self.book.savedTOCArray = nil;
        }else {            
            self.book.savedSpineArray = self.epubUtils.spineArray;
            self.book.savedTOCArray = self.epubUtils.tocArray;
        }
        [Library saveData];
    }
    [super viewWillDisappear:animated];
}

#pragma mark - Rotation handling
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
	return [super shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    self.needToRecalculatingPage = NO;
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

#pragma mark - Download
-(void)showDownloadIndicator{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.downloadIndicatorBookTitle.text = [self.itemForDownload objectForKey:@"title"];
    [self.downloadIndicatorView setHidden:NO];
}
-(void)hideDownloadIndicator{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self.downloadIndicatorView setHidden:YES];
}
-(void)startDownload{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSString *dataKey = @"data";
    NSString *urlKey = @"url";
    
    NSDictionary *data = [self.itemForDownload objectForKey:dataKey];    
    NSURL *url = [NSURL URLWithString:[data objectForKey:urlKey]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
    
    Downloader *downloader = [[Downloader alloc] initWithRequest:urlRequest andProgress:self.downloadIndicatorProgress];
    
    NSData *urlData = [downloader getData];
    
    if(urlData != nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
		
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        NSString *nextoDirectory = [paths objectAtIndex:0];
        
        NSString *filesDirectory = [nextoDirectory stringByAppendingPathComponent:FILES_DIRECTORY_NAME];
        
        if([fileManager fileExistsAtPath:filesDirectory isDirectory:nil] == false)
        {
            [fileManager createDirectoryAtPath:filesDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        NSString *path = [nextoDirectory stringByAppendingPathComponent:FILES_DIRECTORY_NAME];
        [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:path]];
        
        path = [path stringByAppendingPathComponent:[data objectForKey:@"file"]];
        
        if([urlData writeToFile:path atomically:YES] == YES)
        {
            Book *bookTemp = [Book new];
            
            bookTemp.path = path;
            
            if([Book getMetadata:bookTemp image:[UIImage imageWithData:[self.itemForDownload objectForKey:@"image"]]] == true)
            {                
                //Set default config for new book
                [self setDefaultConfigForBook:bookTemp];
                
                self.book = bookTemp;
                self.epubFilePath = self.book.path;
                
                //Call finishDownload on main thread
                dispatch_queue_t mainQueue = dispatch_get_main_queue();
                dispatch_async(mainQueue, ^{
                    [self finishDownload];
                });
            }
            else
            {
                [fileManager removeItemAtPath:path error:NULL];
            }
        }
    }
}
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    const char* filePath = [[URL path] fileSystemRepresentation];
    
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return (result == 0);
}

- (void)setDefaultConfigForBook:(Book *)bookTemp{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if([(NSNumber *)[self.itemForDownload objectForKey:@"type"] intValue] == FILE_EPUB)
    {
        bookTemp.type = BOOK_EBOOK;
    }
    else
    {
        bookTemp.type = BOOK_PDF;
    }
    bookTemp.about = @"";
    
    bookTemp.groupId = [(NSNumber *)[self.itemForDownload objectForKey:@"idGroup"] intValue];
    bookTemp.groupTitle = [self.itemForDownload objectForKey:@"idGroupTitle"];
    
    bookTemp.itemId = [(NSNumber *)[self.itemForDownload objectForKey:@"id"] intValue];
    bookTemp.partsDownloaded = 1;
    bookTemp.partsQuantity = 1;
    NSMutableArray* arrTmp = [[NSMutableArray alloc] init];
    bookTemp.files = arrTmp;   
    bookTemp.result = false;
    bookTemp.savedSpineIndex = 1;
    bookTemp.startPage = 0;
    bookTemp.offsetPage = 0;
    bookTemp.pageRatio = 0;
    bookTemp.fontZoom = [Settings getBookFont]*1.2;
    
    if([Settings getBookNightMode] == 0)
    {
        bookTemp.nightMode = 10;
    }
    else if([Settings getBookNightMode] == 1)
    {
        bookTemp.nightMode = 0;
    }
    else
    {
        bookTemp.nightMode = -1;
    }
    
    bookTemp.bookMode = [Settings getBookBookMode];    
    bookTemp.align = 0;
    bookTemp.margin = 75;
    bookTemp.font = 0;
    bookTemp.pageCount = 0;
    bookTemp.pageIndex = 0;
    bookTemp.savedTOCArray = nil;
    bookTemp.savedSpineArray = nil;
    
    NSMutableDictionary* dicTmp = [[NSMutableDictionary alloc] init];
    bookTemp.bookmarksList = dicTmp;
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy.MM.dd HH:mm:ss"];
    
    bookTemp.downloadDate = [dateFormatter stringFromDate:date];
        
    [Library addBook:bookTemp];
    [Library saveData];
}

-(void)finishDownload{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif    
    [self unzipEpub];
    [self hideDownloadIndicator];
}

#pragma mark - Override funtions
-(void)jumpToPageIndex:(int)pageIndex isCurrentSpine:(BOOL)isCurrentSpine{
    [super jumpToPageIndex:pageIndex isCurrentSpine:isCurrentSpine];
    [self updatePageSlider];
    [self.theWebView setNeedsDisplay];
}

-(int)searchPageIndexForAnchor:(NSString *)anchor{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Search page for anchor link
    float anchor_OffsetTop = [[self.theWebView stringByEvaluatingJavaScriptFromString:
                               [NSString stringWithFormat:@"MyAppGet_OffsetTop_ForAnchor('%@');",anchor]] floatValue];
    if (anchor_OffsetTop != -1) {
        float totalHeight = [[self.theWebView stringByEvaluatingJavaScriptFromString:@"MyAppGet_Content_ScrollHeight();"] floatValue];
        totalHeight = totalHeight - 2*HTML_PADDING;
#ifdef DEBUGX
        NSLog(@"anchor position:%f",(anchor_OffsetTop/totalHeight));
        NSLog(@"scrolLHeight:%f",totalHeight);
        NSLog(@"anchor offset Top:%f",anchor_OffsetTop);
#endif
        return ceil(anchor_OffsetTop/totalHeight);
    }else {
        return NSNotFound;
    }
}

#pragma mark - Other Utilies
-(void)addEpubStyle{    
    //Add epub css
    NSString *mycssPath = [[NSBundle mainBundle] pathForResource:@"NextoEpubStyle" ofType:@"css"];
    NSString *myCSSString = [NSString stringWithContentsOfFile:mycssPath encoding:NSUTF8StringEncoding error:nil];
    CSSItem *myCSS = [[CSSItem alloc] initWithCSSString:myCSSString replacementBlock:^NSMutableDictionary *{
        NSMutableDictionary *returnDictionary = [[NSMutableDictionary alloc] init ];
        [returnDictionary setValue:[NSString stringWithFormat:@"%d",self.book.margin] forKey:@"[body-margin]"];
        [returnDictionary setValue:[NSString stringWithFormat:@"%0.0f",self.theWebView.bounds.size.width] forKey:@"[html-column-width]"];
        [returnDictionary setValue:[NSString stringWithFormat:@"%0.0f", (self.theWebView.bounds.size.height - 2*HTML_PADDING)] forKey:@"[html-height]"];
        [returnDictionary setValue:[NSString stringWithFormat:@"%0.0f", HTML_PADDING] forKey:@"[html-paddingTop]"];
        [returnDictionary setValue:[NSString stringWithFormat:@"%d",self.book.fontZoom ] forKey:@"[body-webkit-text-size-adjust]"];
        
        NSString *jsAlign;
        switch ((EpubAlignType)self.book.align) {
            case JUSTIFY:
                jsAlign = @"justify";
                break;
            case LEFT:
                jsAlign = @"left";
                break;
            case CENTER:
                jsAlign = @"center";
                break;
            case RIGHT:
                jsAlign = @"right";
                break;
            default:
                break;
        }
        [returnDictionary setValue:jsAlign forKey:@"[p-text-align]"];
        
        NSString *jsFontName;
        switch ((EpubFontFamily)self.book.font) {
            case GEORGIA:
                jsFontName = @"Georgia";
                break;
            case PALATINO:
                jsFontName = @"Palatino";
                break;
            case TIMES_NEW_ROMAN:
                jsFontName = @"Times New Roman";
                break;
            case VERDANA:
                jsFontName = @"Verdana";
                break;
            case ARIAL:
                jsFontName = @"Arial";
                break;
            case COCHIN:
                jsFontName = @"Cochin";
                break;
            case APPLEGOTHIC:
                jsFontName = @"AppleGothic";
            default:
                break;
        }
        [returnDictionary setValue:jsFontName forKey:@"[*-font-family]"];
        
        return returnDictionary;
    }];
    [self.cssArray addObject:myCSS];
    [self.cssArray removeObjectAtIndex:0];
    
    //Add Epub javascript
    NSString *myJSPath = [[NSBundle mainBundle] pathForResource:@"NextoEpubScript" ofType:@"js"];
    NSString *myJSString = [NSString stringWithContentsOfFile:myJSPath encoding:NSUTF8StringEncoding error:nil];
    JSItem *myJS = [[JSItem alloc] initWithJSString:myJSString replacementBlock:^NSMutableDictionary *{
        return nil;
    }];
    [self.jsArray addObject:myJS];
}

-(void)updatePageIndexWithCurrentRatio{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    float pageRatio = (float)self.currentPageIndex/(float)self.pageCountForCurrentSpine;
    [self updatePageCountForCurrentSpine];
    int pageIndex = round(pageRatio * self.pageCountForCurrentSpine);
    if (pageIndex < 1) {
        pageIndex = 1;
    }
    [self jumpToPageIndex:pageIndex isCurrentSpine:NO];
    self.needToRecalculatingPage = TRUE;
}

#pragma mark - DatoEpubVCDelegate
-(void)datoEpubVCDidFinishPrepare:(id)sender{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.isSplash) {
        [self showCoverSplash];
    }
    
    if (self.book.savedSpineArray != nil && self.book.savedTOCArray != nil) {
        [self copyPageNumberFromSpineArray:self.book.savedSpineArray];
        [self copyPageNumberFromTOCArray:self.book.savedTOCArray];
    }
    
    [self addEpubStyle];
    [self gotoSpineIndex:self.book.savedSpineIndex atPageRatio:self.book.pageRatio];
    
    self.needToRecalculatingPage = NO;
}
-(void)datoEpubVCDidFinishLoadSpine:(id)sender{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif    
    //Change epub mode
    [self modeSelected:self.book.nightMode];
    [self.theWebView stringByEvaluatingJavaScriptFromString:@"MyApp_Make_Img_Center();"];
}
-(void)datoEpubVCDidFailed:(id)sender withError:(NSString *)error{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Can not read this file!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)datoEpubStartCalculatePage:(id)sender{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.pageSlider.value = 0.0;    
    self.pageLabelInfo.text = @"Trwa liczenie stron ...";
}
-(void)datoEpubFinishCalculateForSpineItem:(EpubSpineItem *)spineItem{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int spineIndex = [self.epubUtils.spineArray indexOfObject:spineItem];
    self.pageSlider.value = ((float)(spineIndex+1)/(float)self.epubUtils.spineArray.count);
}
-(void)datoEpubFinishCalculatePage:(id)sender{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.totalPageCount = [self.epubUtils.spineArray.lastObject startPageNumber] + [self.epubUtils.spineArray.lastObject pageCount]-1;
    [self updateTOCIndexForCurrentPage];
    [self updatePageSlider];
}

#pragma mark - SettingPopoverDelegate
- (void)fontSizeSelected:(int)number{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int fontsize_will_change  = self.book.fontZoom;
    if (number == 1) {
        //Increment margin
        fontsize_will_change = self.book.fontZoom + BODY_FONTZOOM_STEP_CHANGE;
    }else if (number == -1) {
        //Decrement margin
        fontsize_will_change = self.book.fontZoom - BODY_FONTZOOM_STEP_CHANGE;
    }
    
    if (fontsize_will_change >= BODY_FONTZOOM_MINIMUM && fontsize_will_change <= BODY_FONTZOOM_MAXIMUM) {
        self.book.fontZoom = fontsize_will_change;
        [self.theWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Nexto_ChangeBodyFontZoom(%d)",self.book.fontZoom]];
    }
    [self updatePageIndexWithCurrentRatio];
}
- (void)modeSelected:(int)number{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.book.nightMode = number;
	
	if((number >= 5) || (number == -1))
	{
		//[bookmarksButton setBackgroundImage:[UIImage imageNamed:@"reader_bookmarks.png"] forState:UIControlStateNormal];
		//[searchButton setBackgroundImage:[UIImage imageNamed:@"reader_search.png"] forState:UIControlStateNormal];
		[self.settingBtn setBackgroundImage:[UIImage imageNamed:@"reader_settings.png"] forState:UIControlStateNormal];
		[self.tocBtn setBackgroundImage:[UIImage imageNamed:@"reader_chapter_list.png"] forState:UIControlStateNormal];
	}
	else
	{
		//[bookmarksButton setBackgroundImage:[UIImage imageNamed:@"reader_bookmarks_night.png"] forState:UIControlStateNormal];
		//[searchButton setBackgroundImage:[UIImage imageNamed:@"reader_search_night.png"] forState:UIControlStateNormal];
		[self.settingBtn setBackgroundImage:[UIImage imageNamed:@"reader_settings_night.png"] forState:UIControlStateNormal];
		[self.tocBtn setBackgroundImage:[UIImage imageNamed:@"reader_chapter_list_night.png"] forState:UIControlStateNormal];
	}
	
	NSString *textColor;
	NSString *backgroundColor;
	
	if(self.book.nightMode == -1)
	{
		textColor = @"825f4b";
		backgroundColor = @"f3e8cc";
        [self.theWebView setBackgroundColor:[UIColor colorWithRed:0xf3/255.0 green:0xe8/255.0 blue:0xcc/255.0 alpha:1.0]];
        [self.view setBackgroundColor:self.theWebView.backgroundColor];
        [self.pageLabelInfo setTextColor:[UIColor colorWithRed:0x82/255.0 green:0x5f/255.0 blue:0x4b/255.0 alpha:1.0]];
	}
	else
	{
		int color = ((22 * self.book.nightMode) + 35);
		
		backgroundColor = [NSString stringWithFormat: @"%X%X%X",color,color,color];
		
		if(self.book.nightMode >= 5)
		{
			textColor = [NSString stringWithFormat: @"%X%X%X",0x00,0x00,0x00];
            [self.pageLabelInfo setTextColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
		}
		else
		{
			textColor = [NSString stringWithFormat: @"%X%X%X",0xff,0xff,0xff];
            [self.pageLabelInfo setTextColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
		}
        [self.theWebView setBackgroundColor:[UIColor colorWithRed:color/255.0 green:color/255.0 blue:color/255.0 alpha:1.0]];
        [self.view setBackgroundColor:self.theWebView.backgroundColor];
	}
    
	
	NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor = '#%@ !important'",textColor];
	[self.theWebView stringByEvaluatingJavaScriptFromString:jsString];
	
	jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.background = '#%@ !important'",backgroundColor];
	[self.theWebView stringByEvaluatingJavaScriptFromString:jsString];
    
}
- (void)viewSelected:(int)number{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self updatePageIndexWithCurrentRatio];
}
- (void)marginSelected:(int)number{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int margin_will_change  = self.book.margin;
    if (number == 1) {
        //Increment margin
        margin_will_change = self.book.margin + BODY_MARGIN_STEP_CHANGE;
    }else if (number == -1) {
        //Decrement margin
        margin_will_change = self.book.margin - BODY_MARGIN_STEP_CHANGE;
    }
    
    if (margin_will_change >= BODY_MARGIN_MINIMUM && margin_will_change <= BODY_MARGIN_MAXIMUM) {
        self.book.margin = margin_will_change;
        [self.theWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Nexto_ChangeBodyMargin(%d)",self.book.margin]];
    }
    
    [self updatePageIndexWithCurrentRatio];
}
- (void)alignSelected:(int)number{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSString *jsAlign;
    switch ((EpubAlignType)number) {
        case JUSTIFY:
            jsAlign = @"justify";
            break;
        case LEFT:
            jsAlign = @"left";
            break;
        case CENTER:
            jsAlign = @"center";
            break;
        case RIGHT:
            jsAlign = @"right";
            break;
        default:
            break;
    }
    [self.theWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Nexto_ChangeTextAlign(\"%@\")",jsAlign]];
    
    self.book.align = number;
    [self updatePageIndexWithCurrentRatio];
}
- (void)fontSelected:(int)number{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSString *jsFontName;
    switch ((EpubFontFamily)number) {
        case GEORGIA:
            jsFontName = @"Georgia";
            break;
        case PALATINO:
            jsFontName = @"Palatino";
            break;
        case TIMES_NEW_ROMAN:
            jsFontName = @"Times New Roman";
            break;
        case VERDANA:
            jsFontName = @"Verdana";
            break;
        case ARIAL:
            jsFontName = @"Arial";
            break;
        case COCHIN:
            jsFontName = @"Cochin";
            break;
        case APPLEGOTHIC:
            jsFontName = @"AppleGothic";
        default:
            break;
    }
    [self.theWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Nexto_ChangeFontFamily(\"%@\")",jsFontName]];
    
    self.book.font = number;
    [self updatePageIndexWithCurrentRatio];
}

#pragma mark - ChapterListsPopoverDelegate
-(void)chapterSelected:(EpubTOCItem *)tocItem{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"%@",tocItem);
#endif
    NSIndexPath *tocIndexPath = [self.epubUtils indexPathOfTOCItem:tocItem];
    [self gotoTOCIndex:tocIndexPath];
    [self.chapterListsPopover dismissPopoverAnimated:YES];
}

#pragma mark - Event handling
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] || [gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]) {
        //Get tap point
        CGPoint touchPoint = [gestureRecognizer locationInView:self.theWebView];
        
        // convert point from view to HTML coordinate system
        CGSize viewSize = [self.theWebView frame].size;
        CGSize windowSize = [self.theWebView windowSize];
        
        CGFloat f = windowSize.width / viewSize.width;
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 5.) {
            touchPoint.x = touchPoint.x * f;
            touchPoint.y = touchPoint.y * f;
        } else {
            // On iOS 4 and previous, document.elementFromPoint is not taking
            // offset into account, we have to handle it
            CGPoint offset = [self.theWebView scrollOffset];
            touchPoint.x = touchPoint.x * f + offset.x;
            touchPoint.y = touchPoint.y * f + offset.y;
        }
        
        // get the Tags at the touch location
        NSString *tags = [self.theWebView stringByEvaluatingJavaScriptFromString:
                          [NSString stringWithFormat:@"MyAppGetHTMLElementsAtPoint(%i,%i);",(NSInteger)touchPoint.x,(NSInteger)touchPoint.y]];
        
        if (tags) {
            if ([tags rangeOfString:@",IMG,"].location != NSNotFound) {
                return YES;
            }else {
                return NO;
            }
        }
    }
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        //Get tap point
        CGPoint touchPoint = [gestureRecognizer locationInView:self.theWebView];
        
        // convert point from view to HTML coordinate system
        CGSize viewSize = [self.theWebView frame].size;
        CGSize windowSize = [self.theWebView windowSize];
        
        CGFloat f = windowSize.width / viewSize.width;
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 5.) {
            touchPoint.x = touchPoint.x * f;
            touchPoint.y = touchPoint.y * f;
        } else {
            // On iOS 4 and previous, document.elementFromPoint is not taking
            // offset into account, we have to handle it
            CGPoint offset = [self.theWebView scrollOffset];
            touchPoint.x = touchPoint.x * f + offset.x;
            touchPoint.y = touchPoint.y * f + offset.y;
        }
        
        // get the Tags at the touch location
        NSString *tags = [self.theWebView stringByEvaluatingJavaScriptFromString:
                          [NSString stringWithFormat:@"MyAppGetHTMLElementsAtPoint(%i,%i);",(NSInteger)touchPoint.x,(NSInteger)touchPoint.y]];
        if (tags) {
            if ([tags rangeOfString:@",A,"].location != NSNotFound) {
                return NO;
            }
        }
        
    }
    return YES;
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) || ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]])) {
        return NO;
    }
    return YES;
}

-(void)tapOnWebView:(id)sender{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    CGPoint tapPoint = [sender locationInView:self.view];
    if(tapPoint.x < self.view.bounds.size.width/3)
    {
        [self gotoLeftPage];
    }
    else if(tapPoint.x > 2*self.view.bounds.size.width/3)
    {
        [self gotoRightPage];
    }
    else {
        //Show or hide the navigation bar
        self.isFullscreen = !self.isFullscreen;
    }
}
-(void)longpressOnWebView:(UILongPressGestureRecognizer *)longPressGesture{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (longPressGesture.state == UIGestureRecognizerStateBegan) {
        //Get tap point
        CGPoint touchPoint = [longPressGesture locationInView:self.theWebView];
        
        // convert point from view to HTML coordinate system
        CGSize viewSize = [self.theWebView frame].size;
        CGSize windowSize = [self.theWebView windowSize];
        
        CGFloat f = windowSize.width / viewSize.width;
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 5.) {
            touchPoint.x = touchPoint.x * f;
            touchPoint.y = touchPoint.y * f;
        } else {
            // On iOS 4 and previous, document.elementFromPoint is not taking
            // offset into account, we have to handle it
            CGPoint offset = [self.theWebView scrollOffset];
            touchPoint.x = touchPoint.x * f + offset.x;
            touchPoint.y = touchPoint.y * f + offset.y;
        }
        
        // get the Tags at the touch location
        NSString *tags = [self.theWebView stringByEvaluatingJavaScriptFromString:
                          [NSString stringWithFormat:@"MyAppGetHTMLElementsAtPoint(%i,%i);",(NSInteger)touchPoint.x,(NSInteger)touchPoint.y]];
        
        NSString *tagsHREF = [self.theWebView stringByEvaluatingJavaScriptFromString:
                              [NSString stringWithFormat:@"MyAppGetLinkHREFAtPoint(%i,%i);",(NSInteger)touchPoint.x,(NSInteger)touchPoint.y]];
        
        NSString *tagsSRC = [self.theWebView stringByEvaluatingJavaScriptFromString:
                             [NSString stringWithFormat:@"MyAppGetLinkSRCAtPoint(%i,%i);",(NSInteger)touchPoint.x,(NSInteger)touchPoint.y]];
        
#ifdef DEBUGX
        NSLog(@"tags : %@",tags);
        NSLog(@"href : %@",tagsHREF);
        NSLog(@"src : %@",tagsSRC);
#endif
        if (tags) {
            NSString *url = nil;
            if ([tags rangeOfString:@",IMG,"].location != NSNotFound) {
                url = tagsSRC;
            }
            if ((url != nil) && ([url length] != 0)) {                
                [self showImgZooming:url];
            }
        }
    }
}

-(void)pinchOnWebView:(UIPinchGestureRecognizer *)pinchGesture{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if ((pinchGesture.state == UIGestureRecognizerStateChanged) && pinchGesture.scale > 1.0) {
        //Get tap point
        CGPoint touchPoint = [pinchGesture locationInView:self.theWebView];
        
        // convert point from view to HTML coordinate system
        CGSize viewSize = [self.theWebView frame].size;
        CGSize windowSize = [self.theWebView windowSize];
        
        CGFloat f = windowSize.width / viewSize.width;
        if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 5.) {
            touchPoint.x = touchPoint.x * f;
            touchPoint.y = touchPoint.y * f;
        } else {
            // On iOS 4 and previous, document.elementFromPoint is not taking
            // offset into account, we have to handle it
            CGPoint offset = [self.theWebView scrollOffset];
            touchPoint.x = touchPoint.x * f + offset.x;
            touchPoint.y = touchPoint.y * f + offset.y;
        }
        
        // get the Tags at the touch location
        NSString *tags = [self.theWebView stringByEvaluatingJavaScriptFromString:
                          [NSString stringWithFormat:@"MyAppGetHTMLElementsAtPoint(%i,%i);",(NSInteger)touchPoint.x,(NSInteger)touchPoint.y]];
        
        NSString *tagsHREF = [self.theWebView stringByEvaluatingJavaScriptFromString:
                              [NSString stringWithFormat:@"MyAppGetLinkHREFAtPoint(%i,%i);",(NSInteger)touchPoint.x,(NSInteger)touchPoint.y]];
        
        NSString *tagsSRC = [self.theWebView stringByEvaluatingJavaScriptFromString:
                             [NSString stringWithFormat:@"MyAppGetLinkSRCAtPoint(%i,%i);",(NSInteger)touchPoint.x,(NSInteger)touchPoint.y]];
        
#ifdef DEBUGX
        NSLog(@"tags : %@",tags);
        NSLog(@"href : %@",tagsHREF);
        NSLog(@"src : %@",tagsSRC);
#endif
        if (tags) {
            NSString *url = nil;
            if ([tags rangeOfString:@",IMG,"].location != NSNotFound) {
                url = tagsSRC;
            }
            if ((url != nil) && ([url length] != 0)) {                
                [self showImgZooming:url];
            }
        }
    }
}

-(void)touchOnSettingButton{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Show setting popover controller
    if(self.settingsPopoverController == nil)
	{
		_settingsPopoverController = [[SettingsPopover alloc] initWithNibName:@"SettingsPopover" bundle:[NSBundle mainBundle]];
		
		self.settingsPopoverController.book = self.book;
		
		self.settingsPopoverController.navigationItem.title = @"Ustawienia";
		
		UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.settingsPopoverController];
		
        self.settingsPopoverController.delegate = self;
		
		self.settingsPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
	}
	
	[self.settingsPopover setPopoverContentSize:CGSizeMake(300, 322)];
	[self.settingsPopover presentPopoverFromRect:self.settingBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)touchOnTOCButton{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Show TOC Popover controller
    if(self.chapterListPopoverController == nil)
	{
		self.chapterListPopoverController = [[ChaptersListPopover alloc] initWithNibName:@"ChaptersListPopover" bundle:[NSBundle mainBundle]];   
		
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.chapterListPopoverController];
		self.chapterListsPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
	}
    self.chapterListPopoverController.book = self.book;
    
    self.chapterListPopoverController.navigationItem.title = @"Spis treści";
    
    
    self.chapterListPopoverController.delegate = self;
    //Dato @ Techmaster.vn-------------//        
    self.chapterListPopoverController.chaptersList = self.epubUtils.tocArray;

	
    self.chapterListPopoverController.currentTOCItem = self.currentTOCItem;
    self.chapterListPopoverController.currentTOCIndexPath = self.currentTOCIndexPath;
	int height = 400;
	
	[self.chapterListsPopover setPopoverContentSize:CGSizeMake(360, height)];
	[self.chapterListsPopover presentPopoverFromRect:self.tocBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
-(void)touchOnAbout:(id)sender{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    
    ShopItem_iPad *shopItemControler = [[ShopItem_iPad alloc] initWithId:self.book.itemId andDepth:1 fromShop:false];
    shopItemControler.epubReader_VC = self;
    
    UINavigationController *navController = self.navigationController;
    
    [navController pushViewController:shopItemControler animated:YES];
}

-(void)touchOnCancelDownload{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)sliderStarted{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int pageIndexToJump = round(self.pageSlider.value * self.totalPageCount);
    if (pageIndexToJump == 0) {
        pageIndexToJump = 1;
    }    
    
    self.pageLabelInfo.text = [NSString stringWithFormat:@"Strona %d/%d", pageIndexToJump, self.totalPageCount];
    
    
}
-(void)sliderEnded{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int pageIndexToJump = round(self.pageSlider.value * self.totalPageCount);
    if (pageIndexToJump == 0) {
        pageIndexToJump = 1;
    }
    
    for (int i=self.epubUtils.spineArray.count-1; i>=0; i--) {
        EpubSpineItem *spineItem = [self.epubUtils.spineArray objectAtIndex:i];
        if (spineItem.startPageNumber<=pageIndexToJump) {
            if (spineItem == self.currentSpineItem) {
#ifdef DEBUGX
                NSLog(@"page index to jump:%d",pageIndexToJump);
#endif
                [self jumpToPageIndex:(pageIndexToJump-spineItem.startPageNumber + 1) isCurrentSpine:YES];
                break;
            }else {
                float pageRatioToJump = (float)(pageIndexToJump-spineItem.startPageNumber + 1)/(float)spineItem.pageCount;
#ifdef DEBUGX
                NSLog(@"page Ratio To Jump:%f",pageRatioToJump);
#endif
                [self gotoSpineIndex:i atPageRatio:pageRatioToJump];
                break;
            }
        }
    }
}

-(void)shareFacebook{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif    
    // get the Tags at the touch location
    NSString *selectedText = [self.theWebView stringByEvaluatingJavaScriptFromString:
                              [NSString stringWithFormat:@"MyAppGetSelectedString();"]];
#ifdef DEBUGX
    NSLog(@"Selected Text: %@",selectedText);
#endif
    if (self.shareFBVC == nil) {        
        self.shareFBVC = [[EpubShareFacebook  alloc] init];
        [self.view addSubview:self.shareFBVC.view];
        self.shareFBVC.view.frame = self.view.bounds;
        [self.shareFBVC share:selectedText inBook:self.book];
    }else {
        [self.shareFBVC share:selectedText inBook:self.book];
    }
}

#pragma mark - User Interface
- (void)changeViewToFullscreen:(BOOL)isFullScreen{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (isFullScreen == NO) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationCurveEaseInOut animations:^{                
            self.navigationController.navigationBarHidden = NO;
            self.settingBtn.hidden = NO;
            //self.bookmarksBtn.hidden = YES;
            self.tocBtn.hidden = NO;
            self.settingBtn.center = CGPointMake(self.settingBtn.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
            //self.bookmarksButton.center = CGPointMake(self.bookmarksButton.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
            //self.btnBookmark.center = CGPointMake(self.btnBookmark.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
            self.tocBtn.center = CGPointMake(self.tocBtn.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
            self.pageSlider.hidden = NO;
            self.pageSlider.center = CGPointMake(self.pageSlider.center.x, self.theWebView.bounds.size.height - 25 + self.pageSlider.frame.size.height/2);
            self.pageLabelInfo.hidden = NO;
            self.pageLabelInfo.center = CGPointMake(self.pageLabelInfo.center.x, self.theWebView.bounds.size.height - 40 + self.pageLabelInfo.frame.size.height/2);
        } completion:^(BOOL finished) {
            if (finished == FALSE) {                    
                self.navigationController.navigationBarHidden = NO;
                self.settingBtn.hidden = NO;
                //self.bookmarksButton.hidden = YES;
                self.tocBtn.hidden = NO;
                self.settingBtn.center = CGPointMake(self.settingBtn.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5 - self.settingBtn.frame.size.height/2);
                //self.bookmarksButton.center = CGPointMake(self.bookmarksButton.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
                //self.btnBookmark.center = CGPointMake(self.btnBookmark.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
                self.tocBtn.center = CGPointMake(self.tocBtn.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
                self.pageSlider.hidden = NO;
                self.pageSlider.center = CGPointMake(self.pageSlider.center.x, self.theWebView.bounds.size.height - 15 + self.pageSlider.frame.size.height/2);
                self.pageLabelInfo.hidden = NO;
                self.pageLabelInfo.center = CGPointMake(self.pageLabelInfo.center.x, self.theWebView.bounds.size.height - 40 + self.pageLabelInfo.frame.size.height/2);
            }
        }];
    }else {        
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationCurveEaseInOut animations:^{  
            self.navigationController.navigationBar.center = CGPointMake(self.navigationController.navigationBar.center.x, self.navigationController.navigationBar.center.y-44);
            self.settingBtn.center = CGPointMake(self.settingBtn.center.x, self.settingBtn.center.y-44);
            //self.bookmarksButton.center = CGPointMake(self.bookmarksButton.center.x, self.bookmarksButton.center.y-44);
            //self.btnBookmark.center = CGPointMake(self.btnBookmark.center.x, self.btnBookmark.center.y-44);
            self.tocBtn.center = CGPointMake(self.tocBtn.center.x, self.tocBtn.center.y-44);
            self.pageSlider.center = CGPointMake(self.pageSlider.center.x, self.theWebView.bounds.size.height + 15);
            self.pageLabelInfo.center = CGPointMake(self.pageLabelInfo.center.x, self.theWebView.bounds.size.height + 30);
        } completion:^(BOOL finished) {
            if (finished) {                    
                [self.navigationController setNavigationBarHidden:YES];
                self.settingBtn.hidden = YES;
                //self.bookmarksButton.hidden = YES;
                self.tocBtn.hidden = YES;
                self.pageSlider.hidden = YES;
                self.pageLabelInfo.hidden = YES;
            }else {
                [self.navigationController setNavigationBarHidden:YES];
                self.settingBtn.hidden = YES;
                //self.bookmarksButton.hidden = YES;
                self.tocBtn.hidden = YES;
                self.pageSlider.hidden = YES;
                self.pageSlider.center = CGPointMake(self.pageSlider.center.x, self.theWebView.bounds.size.height + 15);
                self.pageLabelInfo.hidden = YES;
                self.pageLabelInfo.center = CGPointMake(self.pageLabelInfo.center.x, self.theWebView.bounds.size.height + 30);
                self.navigationController.navigationBar.center = CGPointMake(self.navigationController.navigationBar.center.x, -22);
                self.settingBtn.center = CGPointMake(self.settingBtn.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
                //self.bookmarksButton.center = CGPointMake(self.bookmarksButton.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
                //self.btnBookmark.center = CGPointMake(self.btnBookmark.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
                self.tocBtn.center = CGPointMake(self.tocBtn.center.x, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height+5);
            }
        }];
    }
}
-(void)showCoverSplash{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    UIImage *coverImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.epubUtils.coverImagePath]]];
    self.coverSplashImage.image = coverImage;
    self.coverSplashImage.hidden = NO;
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(hideCoverSplash) userInfo:nil repeats:NO];
}
-(void)hideCoverSplash{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationCurveEaseInOut animations:^{
        self.coverSplashImage.alpha = 0.3;
        self.coverSplashImage.transform = CGAffineTransformMakeScale(2.0, 2.0);
    } completion:^(BOOL finished) {
        self.coverSplashImage.hidden = YES;
        self.coverSplashImage.alpha = 1.0;
        self.coverSplashImage.transform = CGAffineTransformIdentity;
        self.isSplash = NO;
    }];
}

-(void)updatePageSlider{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.isCalculatingPage  == FALSE) {        
        int currentPageIndex = self.currentPageIndex + self.currentSpineItem.startPageNumber - 1;
        
        if (self.currentTOCItem != nil && self.currentTOCItem.chapterTitle != nil) {
            self.pageSlider.value = (float)currentPageIndex/(float)self.totalPageCount;
            self.pageLabelInfo.text = [NSString stringWithFormat:@"%@ , str %d z %d",self.currentTOCItem.chapterTitle,currentPageIndex,self.totalPageCount];
        }else {
            self.pageSlider.value = (float)currentPageIndex/(float)self.totalPageCount;
            self.pageLabelInfo.text = [NSString stringWithFormat:@"%@ , str %d z %d",self.book.title,currentPageIndex,self.totalPageCount];
        }
    }
}

-(void)showImgZooming:(NSString *)imgPath{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.imgZooming == nil) {        
        self.imgZooming = [[EpubImageZooming alloc] initWithImagePath:imgPath];
        self.imgZooming.view.frame = self.view.bounds;
        [self.view addSubview:self.imgZooming.view];
        [self.imgZooming switchDisplay];
    }else {
        if (self.imgZooming.view.hidden == YES) {            
            NSURL *imgURL = [NSURL URLWithString:imgPath];
            self.imgZooming.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imgURL]];
            [self.imgZooming switchDisplay];
        }
    }
}

#pragma mark - Encapsulation for properties
- (void)setIsFullscreen:(BOOL)isFullscreen{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    _isFullscreen = isFullscreen;
    [self changeViewToFullscreen:_isFullscreen];
}

#pragma mark - Memory management
- (void)didReceiveMemoryWarning
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSLog(@"Receive memory warning!");
    [super didReceiveMemoryWarning];
}
@end