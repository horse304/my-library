//
//  CoreDataController.h
//  SexFunFacts
//
//  Created by Techmaster on 8/4/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "LanguageUtil.h"
#import "EntityResultsController.h"
#import "ModelDefine.h"
#import "Category.h"
#import "Factitem.h"

#define NOTIFICATION_COREDATA_UPDATED @"Notification_Coredata_Updated"
#define IMAGES_DIRECTORY_NAME @"images" 


@interface CoreDataController : NSObject

/**
    
	@returns The singleton instant of CoreDataController
 */
+ (CoreDataController *) shared;

/**
 The MyFetchedResultsController of Factitem
 */
@property (strong, nonatomic) EntityResultsController *fact_FetchedResultsController;

/**
 The MyFetchedResultsController of Category
 */
@property (strong, nonatomic) EntityResultsController *category_FetchedResultsController;

-(void)changeFactResultsControllerWithCategory:(Category *)category;
-(NSString *)getPathOfImageResources;
@end
