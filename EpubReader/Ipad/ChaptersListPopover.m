//
//  ChaptersListPopover.m
//  nexto
//
//  Created by Dato - Techmaster.vn on 2012-07-27.
//  Copyright 2012 Dato. All rights reserved.
//

#import "ChaptersListPopover.h"
#import "ChaptersListCell.h"
@interface ChaptersListPopover()
@property (retain, nonatomic) NSMutableArray *chaptersListLinear;
@end

@implementation ChaptersListPopover

@synthesize book;

@synthesize table;

@synthesize chaptersList;
@synthesize delegate;
@synthesize currentTOCIndexPath = _currentTOCIndexPath;
@synthesize chaptersListLinear = _chaptersListLinear;
@synthesize currentTOCIndexLinear = _currentTOCIndexLinear;
@synthesize currentTOCItem = _currentTOCItem;

-(void)viewWillAppear:(BOOL)animated{
    self.chaptersListLinear = [self getChaptersListLinear];
    [self.table reloadData];
    for(int i=0;i<self.chaptersListLinear.count;i++){
        if ([[self.chaptersListLinear objectAtIndex:i] tocItem] == self.currentTOCItem) {
            self.currentTOCIndexLinear = i;
            break;
        }
    }
    
    [self.table selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentTOCIndexLinear inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
}

-(NSMutableArray *)getChaptersListLinear{
    __block NSMutableArray *chapterListResult = [[NSMutableArray alloc] init ];
    __block int depth=0;
    __block void (^loopTitle)(EpubTOCItem *) = ^(EpubTOCItem *chapterItem){
        NSString *tab = @"";
        for(int i=0;i<depth;i++){
            tab = [tab stringByAppendingFormat:@"   "];
        }
        //NSLog(@"%@",[NSString stringWithFormat:@"%@%@",tab,chapterItem.chapterTitle]);
        ChaptersListCell *chapterCell = [[ChaptersListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
        chapterCell.tocItem = chapterItem;
        if (chapterItem.chapterTitle != nil) {            
            chapterCell.textLabel.text = [NSString stringWithFormat:@"%@%@",tab,chapterItem.chapterTitle];
        }else {
            chapterCell.textLabel.text = self.book.title;
        }
        //if(chapterItem.subChapters.count > 0) chapterCell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        [chapterListResult addObject:chapterCell];
        for (__block EpubTOCItem *subChapter in chapterItem.subChapters) {
            depth = depth+1;
            loopTitle(subChapter);
        }
        [chapterCell release];
        depth--;
    };
    for (EpubTOCItem *chapter in self.chaptersList) {
        depth = 0;
        loopTitle(chapter);
    }
    return [chapterListResult autorelease];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
	return(self.chaptersListLinear.count);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	return([self.chaptersListLinear objectAtIndex:indexPath.row]);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if(delegate != nil)
	{
		[delegate chapterSelected:[[self.chaptersListLinear objectAtIndex:indexPath.row] tocItem]];
	}
}


- (void)dealloc
{
	self.chaptersList = nil;
	self.delegate = nil;
	
	[super dealloc];
}


@end
