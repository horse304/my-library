//
//  TBCAuthentication.h
//  QuizEnzine
//
//  Created by Dato on 6/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizAPIDelegate.h"
#import "VCLogin.h"
#import "VCCreateAccount.h"

@interface TBCAuthentication : UITabBarController <QuizAPIDelegate>
@property (strong, nonatomic) VCLogin *vcLogin;
@property (strong, nonatomic) VCCreateAccount *vcCreateAccount;

@end
