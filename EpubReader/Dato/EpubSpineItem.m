//
//  DatoEpubSpineItem.m
//  nexto
//
//  Created by Techmaster on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//#define DEBUGX
#import "EpubSpineItem.h"
#import "EpubTOCItem.h"

#define SPINE_KEY_spineURL @"spine_key_spineurl"
#define SPINE_KEY_isLinear @"spine_key_islinear"
#define SPINE_KEY_pageCount_Landscape @"spine_key_pagecount_landscape"
#define SPINE_KEY_pageCount_Portrait @"spine_key_pagecount_portrait"
#define SPINE_KEY_startPageNumber_Landscape @"spine_key_startpagenumber_landscape"
#define SPINE_KEY_startPageNumber_Portrait @"spine_key_startpagenumber_portrait"

@interface EpubSpineItem(){
    UIWebView *webViewForCalculate;
}
@end

@implementation EpubSpineItem
@synthesize                 spineURL = _spineURL;
@synthesize                 isLinear = _isLinear;
@synthesize     finishCalculateBlock = _finishCalculateBlock;
-(id)init{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        [self resetPageOfSpine];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        pageCount_Landscape = [aDecoder decodeIntForKey:SPINE_KEY_pageCount_Landscape];
        pageCount_Portrait = [aDecoder decodeIntForKey:SPINE_KEY_pageCount_Portrait];
        startPageNumber_Landscape = [aDecoder decodeIntForKey:SPINE_KEY_startPageNumber_Landscape];
        startPageNumber_Portrait = [aDecoder decodeIntForKey:SPINE_KEY_startPageNumber_Portrait];
        self.spineURL = (NSURL *)[aDecoder decodeObjectForKey:SPINE_KEY_spineURL];
        self.isLinear = [aDecoder decodeBoolForKey:SPINE_KEY_isLinear];
#ifdef DEBUGX
        NSLog(@"spineURL:%@",self.spineURL.absoluteString);
#endif
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [aCoder encodeObject:self.spineURL forKey:SPINE_KEY_spineURL];
    [aCoder encodeBool:self.isLinear forKey:SPINE_KEY_isLinear];
    [aCoder encodeInt:pageCount_Landscape forKey:SPINE_KEY_pageCount_Landscape];
    [aCoder encodeInt:pageCount_Portrait forKey:SPINE_KEY_pageCount_Portrait];
    [aCoder encodeInt:startPageNumber_Landscape forKey:SPINE_KEY_startPageNumber_Landscape];
    [aCoder encodeInt:startPageNumber_Portrait forKey:SPINE_KEY_startPageNumber_Portrait];
}

-(void)calculatePageCountWithSize:(CGSize)webViewSize target:(id)target renderSEL:(SEL)renderSEL finishBlock:(FinishCalculate)finishBlock{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.finishCalculateBlock = finishBlock;     
    if (webViewForCalculate == nil) {        
        webViewForCalculate = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, webViewSize.width, webViewSize.height)];
        webViewForCalculate.delegate = self;
    }else {
        webViewForCalculate.frame = CGRectMake(0, 0, webViewSize.width, webViewSize.height);
    }
    NSString *htmlString = [NSString stringWithContentsOfURL:self.spineURL encoding:NSUTF8StringEncoding error:nil];
    htmlString = [target performSelector:renderSEL withObject:htmlString];
    [webViewForCalculate loadHTMLString:htmlString baseURL:self.spineURL];
}

-(void)calculatePageForTocActive:(NSMutableArray *)tocActiveArray{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    
    //Caculate toc page for tocActiveArray
    for (EpubTOCItem *tocItem in tocActiveArray) {
        if (tocItem.chapterURL.fragment != nil) {
            //Is anchor link
            tocItem.pageNumber = self.startPageNumber + [self searchPageIndexForAnchor:tocItem.chapterURL.fragment] - 1;
        }else {
            tocItem.pageNumber = self.startPageNumber;
        }
    }
}

-(int)searchPageIndexForAnchor:(NSString *)anchor{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Search page for anchor link
    float anchor_OffsetTop = [[webViewForCalculate stringByEvaluatingJavaScriptFromString:
                               [NSString stringWithFormat:@"MyAppGet_OffsetTop_ForAnchor('%@');",anchor]] floatValue];
    if (anchor_OffsetTop != -1) {
        float totalHeight = [[webViewForCalculate stringByEvaluatingJavaScriptFromString:@"MyAppGet_Content_ScrollHeight();"] floatValue];
        totalHeight = totalHeight - 90;
        return ceil(anchor_OffsetTop/totalHeight);
    }else {
        return NSNotFound;
    }
}

#pragma mark - UIWebViewDelegate
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    return YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int totalWidth = [[webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] intValue];
	self.pageCount = (int)((float)totalWidth/webView.bounds.size.width);
    self.finishCalculateBlock(TRUE, self);
    webViewForCalculate = nil;
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.finishCalculateBlock(FALSE, self);
}

#pragma mark - Encapsulation for properties
-(int)pageCount{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        return pageCount_Landscape;
    }else {
        return pageCount_Portrait;
    }
}

-(void)setPageCount:(int)pageCount{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        pageCount_Landscape = pageCount;
    }else {
        pageCount_Portrait = pageCount;
    }
}

-(int)startPageNumber{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        return startPageNumber_Landscape;
    }else {
        return startPageNumber_Portrait;
    }
}

-(void)setStartPageNumber:(int)startPageNumber{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        startPageNumber_Landscape = startPageNumber;
    }else {
        startPageNumber_Portrait = startPageNumber;
    }
}
-(void)resetPageOfSpine{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    startPageNumber_Portrait = startPageNumber_Landscape = NSNotFound;
    pageCount_Portrait = pageCount_Landscape = NSNotFound;
}
-(void)copyPageNumberFrom:(EpubSpineItem*)spineItem{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    pageCount_Portrait = spineItem->pageCount_Portrait;
    pageCount_Landscape = spineItem->pageCount_Landscape;
    startPageNumber_Portrait = spineItem->startPageNumber_Portrait;
    startPageNumber_Landscape = spineItem->startPageNumber_Landscape;
}
@end
