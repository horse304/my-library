//
//  DatoEpubViewController.m
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 7/17/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//
#define DEBUGX
#define UNZIP_TEMPORARY_DIRECTORY @"nexto"
#define ERROR_UNZIP_FAILED @"Error unzip failed!"

//Default File Name
#define kEPUB_DEFAULT_STYLE_FILE @"EpubDefaultStyle"
#define kEPUB_DEFAULT_JS_FILE @"EpubDefaultJS"

#import "DatoEpubVC.h"

@interface DatoEpubVC ()
@property (retain, nonatomic) NSURL *epubURL;
@property (copy, nonatomic) WebViewFinishBlock finishLoadBlock;
@property (retain, nonatomic) NSString *hashUnzip;
@property (assign, nonatomic) UIInterfaceOrientation currentOrientation;

//UI Properties
@property (strong, nonatomic) MBProgressHUD *HUD;
@end

@implementation DatoEpubVC
@synthesize                                 delegate = _delegate;
@synthesize                         isCalculatingPage = _isCalculatingPage;
@synthesize                                 cssArray = _cssArray;
@synthesize                                  jsArray = _jsArray;
@synthesize                             epubFilePath = _epubFilePath;
@synthesize                                  epubURL = _epubURL;
@synthesize                                epubUtils = _epubUtils;
@synthesize                               theWebView = _theWebView;
@synthesize                            theScrolLView = _theScrolLView;
@synthesize                        currentSpineIndex = _currentSpineIndex;
@synthesize                         currentSpineItem = _currentSpineItem;
@synthesize                      currentTOCIndexPath = _currentTOCIndexPath;
@synthesize                           currentTOCItem = _currentTOCItem;
@synthesize                 pageCountForCurrentSpine = _pageCountForCurrentSpine;
@synthesize                         currentPageIndex = _currentPageIndex;
@synthesize                          finishLoadBlock = _finishLoadBlock;
@synthesize                                      HUD = _HUD;
@synthesize                                hashUnzip = _hashUnzip;
@synthesize                     isStopCaculatingPage = _isStopCaculatingPage;
@synthesize                  needToRecalculatingPage = _needToRecalculatingPage;
@synthesize                           totalPageCount = _totalPageCount;
@synthesize                       currentOrientation = _currentOrientation;

-(id)initWithEpubFilePath:(NSString *)filePath{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        self.epubFilePath = filePath;
        [self initializeProperties];
    }
    return self;
}
-(id)initWithEpubURL:(NSURL *)epubURL{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        self.epubURL = epubURL;
        [self initializeProperties];
    }
    return self;
}

-(void)initializeProperties{
    _currentSpineIndex = -1;
    _currentTOCIndexPath = nil;
    _currentPageIndex = -1;
    _pageCountForCurrentSpine = 0;
    _isStopCaculatingPage = FALSE;
}

#pragma mark - View Life Cycle
- (void)loadView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    
    //Add uiwebview
    _theWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.theWebView.delegate = self;
    self.theWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.theWebView.backgroundColor = [UIColor whiteColor];
    [self.theWebView setMultipleTouchEnabled:YES];
    [self.theWebView setUserInteractionEnabled:YES];
    [self.theWebView setScalesPageToFit:NO];
    [self.view addSubview:self.theWebView];
    
    //Configue Scrollview
    self.theScrolLView = [self.theWebView.subviews objectAtIndex:0];    
    [self.theScrolLView setScrollEnabled:NO];
    
    //Add Swipe for webview
    UISwipeGestureRecognizer *swipeFromLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoLeftPage)];
    swipeFromLeft.direction = UISwipeGestureRecognizerDirectionRight;
    [self.theWebView addGestureRecognizer:swipeFromLeft];
    UISwipeGestureRecognizer *swipeFromRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRightPage)];
    swipeFromRight.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.theWebView addGestureRecognizer:swipeFromRight];
}
- (void)viewDidLoad
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewDidLoad];
    self.currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
    if (self.epubFilePath != nil) {        
        //Unzip epub
        [self unzipEpub];
    }else if(self.epubURL != nil){
        //Temporary disable support init epub from URL
    }
}
-(void)viewWillAppear:(BOOL)animated{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
}

-(void)viewDidAppear:(BOOL)animated{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.currentOrientation != [UIApplication sharedApplication].statusBarOrientation) {
        [self didRotateFromInterfaceOrientation:self.currentOrientation];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
}
- (void)viewDidUnload
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark - Rotation handling
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    return YES;
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
    float pageRatio = (float)self.currentPageIndex/(float)self.pageCountForCurrentSpine;
    
    [self.theWebView reload];
    [self.theWebView stopLoading];
    
    int savedCurrentSpineIndex = self.currentSpineIndex;
    _currentSpineIndex = NSNotFound;
    
    [self gotoSpineIndex:savedCurrentSpineIndex atPageRatio:pageRatio];
}

#pragma mark - Unzip epub and prepare toc, spine
-(void)unzipEpub{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self showWaiting];
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.epubFilePath]) {
        dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(aQueue, ^{        
            static BOOL resultUnzip;
            NSString *tempDirectory = [NSTemporaryDirectory() stringByAppendingPathComponent:UNZIP_TEMPORARY_DIRECTORY];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            [fileManager removeItemAtPath:tempDirectory error:NULL];
            
            CFUUIDRef theUUID = CFUUIDCreate(NULL);
            self.hashUnzip = (__bridge_transfer NSString *) CFUUIDCreateString(NULL, theUUID);
            CFRelease(theUUID);
            NSString *unzipTempDirectory = [tempDirectory stringByAppendingPathComponent:self.hashUnzip];
            
            ZipArchive *zipArchive = [[ZipArchive alloc] init];
            
            if([zipArchive UnzipOpenFile:self.epubFilePath])
            {
                resultUnzip = [zipArchive UnzipFileTo:unzipTempDirectory overWrite:YES];
                
                [zipArchive UnzipCloseFile];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //Finished unzip epub file
                if (resultUnzip) {
                    [self unzipFinished:unzipTempDirectory];
                }else {
                    [self unzipFailed];
                }
            });
        });
    }
}
-(void)unzipFinished:(NSString *)unzipedDirectory{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //prepare epub
    _epubUtils = [[EpubUtils alloc] initWithDirectory:unzipedDirectory withDelegate:self];
    [self.epubUtils prepareEpub];
}
-(void)unzipFailed{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if ([self.delegate respondsToSelector:@selector(datoEpubVCDidFailed:withError:)]) {
        [self.delegate datoEpubVCDidFailed:self withError:ERROR_UNZIP_FAILED];
    }
    [self hideWaiting];
}

-(void)epubUtilsDidFinishPrepare:(id)sender{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif    
    
    //Call delegate
    if ([self.delegate respondsToSelector:@selector(datoEpubVCDidFinishPrepare:)]) {
        [self.delegate datoEpubVCDidFinishPrepare:self];
    }
    
    [self hideWaiting];
}

#pragma mark - Render style for epub
-(void)openLink:(NSURL *)url enableRender:(BOOL)enableRender{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (enableRender) {
        //Get HTML String and embed css, javascript
        NSError *error;
        NSString *htmlString = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
        htmlString = [self renderHTML:htmlString];
        if (error == NULL) {
            [self.theWebView loadHTMLString:htmlString baseURL:url];
        }else {
#ifdef DEBUGX
            NSLog(@"Can not load html string for url with error:%@",error);
#endif
#warning Dev should show the error page
        }
    }
}
-(NSString *)renderHTML:(NSString *)html{ 
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    HTMLUtils *htmlUtils = [[HTMLUtils alloc] initWithHTMLString:html];
    
    //Add CSS Style for Epub
    NSString *cssString = [self getEpubCSS];
    [htmlUtils addString:cssString to:HTMLPartTypeHead];
    
    //Add Javascritp for Epub
    NSString *javascriptString = [self getEpubJavascript];
    [htmlUtils addString:javascriptString to:HTMLPartTypeHead];
    
    return [htmlUtils getHTMLString];
}

-(NSString *)getEpubCSS{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.cssArray.count != 0) {
        NSString *returnCSS = @"";
        for (id cssItem in self.cssArray) {
            if([cssItem isKindOfClass:[CSSItem class]]){
                NSString *cssReplaced = [cssItem makeReplacement];
                returnCSS = [returnCSS stringByAppendingString:cssReplaced];
            }
        }
        
        if (![returnCSS isEqualToString:@""]) {
            returnCSS = [NSString stringWithFormat:@"<style type=\"text/css\">%@</style>",returnCSS];
            return returnCSS;
        }
    }
    
    return nil;
}
-(NSString *)getEpubJavascript{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.jsArray.count != 0) {
        NSString *returnJS = @"";
        for (id jsItem in self.jsArray) {
            if([jsItem isKindOfClass:[JSItem class]]){
                NSString *jsReplaced = [jsItem makeReplacement];
                returnJS = [returnJS stringByAppendingString:jsReplaced];
            }
        }
        
        if (![returnJS isEqualToString:@""]) {
            returnJS = [NSString stringWithFormat:@"<script type=\"text/javascript\">%@</script>",returnJS];
            return returnJS;
        }
    }
    
    return nil;
}



#pragma mark - UIWebview Delegate
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {        
        if([[request.URL scheme] isEqualToString:@"http"])
        {
            [[UIApplication sharedApplication] openURL: request.URL];
        }
        else if([[request.URL scheme] isEqualToString:@"file"])
        {
            if (![[NSFileManager defaultManager] contentsEqualAtPath:request.URL.path andPath:self.currentSpineItem.spineURL.path]) {
                //Get spine index
                int spineIndex = NSNotFound;
                
                //Check request path exist in spine array
                for (EpubSpineItem *spineItem in self.epubUtils.spineArray) {
                    if ([[NSFileManager defaultManager] contentsEqualAtPath:request.URL.path andPath:spineItem.spineURL.path]) {
                        spineIndex = [self.epubUtils.spineArray indexOfObject:spineItem];
                        break;
                    }
                }
                
                if (spineIndex!=NSNotFound) {
                    //Check request path is anchor link or not
                    if (request.URL.fragment != nil) {
                        [self gotoSpineIndex:spineIndex atAnchor:request.URL.fragment];
                    }else {                        
                        [self gotoSpineIndex:spineIndex atPageRatio:0];
                    }
                }
            }else {
                if (request.URL.fragment != nil) 
                    [self gotoAnchor:request.URL.fragment];
                else {
                    [self jumpToPageIndex:0 isCurrentSpine:YES];
                }
            }
        }
        return NO;
    }
    
    return YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webView{ 
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.theWebView.alpha = 0.0;
    //Show Waiting
    [self showWaiting];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.finishLoadBlock != nil) {
        self.finishLoadBlock();
    }else {
        //Run default finishLoad Block
        WebViewFinishBlock defaultFinish = ^(){
            [self jumpToPageIndex:1 isCurrentSpine:NO];
        };
        defaultFinish();
    }
    self.finishLoadBlock = nil;
    
    if ([self.delegate respondsToSelector:@selector(datoEpubVCDidFinishLoadSpine:)]) {
        [self.delegate datoEpubVCDidFinishLoadSpine:self];
    }
    [self jumpToPageIndex:self.currentPageIndex isCurrentSpine:YES];
    
    //Hide waiting
    [self hideWaiting];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.theWebView.alpha = 1.0;
    }];
}

#pragma mark - Paginating
-(void)updatePageCountForCurrentSpine{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    float webViewScrollWidth = [[self.theWebView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] floatValue];
    int pageCount = ceil(webViewScrollWidth/self.theWebView.bounds.size.width);
    _pageCountForCurrentSpine = pageCount;
#ifdef DEBUGX
    //NSLog(@"page Count:%d",self.pageCountForCurrentSpine);
#endif
}

-(void)updateTOCIndexForCurrentPage{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    __block NSIndexPath *foundTOCIndex;
    __block int closestPageNumber = 1;
    for (EpubTOCItem *tocItem in self.epubUtils.tocArray) {
#ifdef DEBUGX
        NSLog(@"toc pagenumber:%d title:%@",tocItem.pageNumber,tocItem.chapterTitle);
#endif
        [tocItem browseTOCTreeWithBlock:^(EpubTOCItem *tocItem, int depth) {            
            int currentPageIndex = (self.currentPageIndex + self.currentSpineItem.startPageNumber - 1);
            if (tocItem.pageNumber == currentPageIndex) {
                foundTOCIndex = [self.epubUtils indexPathOfTOCItem:tocItem];
            }else if (tocItem.pageNumber < currentPageIndex && tocItem.pageNumber>=closestPageNumber) {
                foundTOCIndex = [self.epubUtils indexPathOfTOCItem:tocItem];
                closestPageNumber = tocItem.pageNumber;
            }
        }];
    }
    
    if (foundTOCIndex != nil) {
        self.currentTOCIndexPath = foundTOCIndex;
#ifdef DEBUGX
        NSLog(@"current TOC IndexPath:%@ title:%@",self.currentTOCIndexPath, self.currentTOCItem.chapterTitle);
#endif
    }
}

-(void)calculatePageForAllSpine{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.isCalculatingPage) {
        [self stopCaculatingPage];
    }
    
    if ([self.delegate respondsToSelector:@selector(datoEpubStartCalculatePage:)]) {
        [self.delegate datoEpubStartCalculatePage:self];
        self.isCalculatingPage = YES;
    }
    
    //Start caculate pagecount for each spineItem
    __block int spineCaculatingIndex = 0;
    __block int totalCaculatedPageCount = 1;
    
    FinishCalculate finishCaculateBlock = ^(BOOL isFinished, EpubSpineItem *spineItem){
        if (self.isStopCaculatingPage) {
            //Stop and reset spine array
            for (EpubSpineItem *spineItem in self.epubUtils.spineArray) {
                spineItem.pageCount = NSNotFound;
                spineItem.startPageNumber = NSNotFound;
            }
            spineCaculatingIndex = 0;
            totalCaculatedPageCount = 1;
            self.isStopCaculatingPage = FALSE;
            self.isCalculatingPage = FALSE;
        }else {            
            spineItem.startPageNumber = totalCaculatedPageCount;
            totalCaculatedPageCount += spineItem.pageCount;
                        
            //Caculate page number for TOC in spine
            NSMutableArray *tocActiveArray = [[NSMutableArray alloc] init ];
            for (EpubTOCItem *tocItem in self.epubUtils.tocArray) {
                [tocItem browseTOCTreeWithBlock:^(EpubTOCItem *tocItem, int depth){                
                    if ([[NSFileManager defaultManager] contentsEqualAtPath:tocItem.chapterURL.path andPath:spineItem.spineURL.path]){
                        [tocActiveArray addObject:tocItem];
                    }
                }];
            }
            
            [spineItem calculatePageForTocActive:tocActiveArray];
            
            if ([self.delegate respondsToSelector:@selector(datoEpubFinishCalculateForSpineItem:)]) {
                [self.delegate datoEpubFinishCalculateForSpineItem:spineItem];
            }
            
            //Start next spine item
            if (spineCaculatingIndex<self.epubUtils.spineArray.count-1) {
                spineCaculatingIndex++;
                EpubSpineItem *nextSpineItem = [self.epubUtils.spineArray objectAtIndex:spineCaculatingIndex];
                [nextSpineItem calculatePageCountWithSize:self.theWebView.frame.size target:self renderSEL:@selector(renderHTML:) finishBlock:spineItem.finishCalculateBlock];
            }else {
                if ([self.delegate respondsToSelector:@selector(datoEpubFinishCalculatePage:)]) {
                    self.isCalculatingPage = NO;
                    self.totalPageCount = spineItem.startPageNumber + spineItem.pageCount - 1;
                    [self.delegate datoEpubFinishCalculatePage:self];
                }
            }
        }
    };
    
    EpubSpineItem *startSpineItem = [self.epubUtils.spineArray objectAtIndex:spineCaculatingIndex];
    [startSpineItem calculatePageCountWithSize:self.theWebView.frame.size target:self renderSEL:@selector(renderHTML:) finishBlock:finishCaculateBlock];
}
-(void)stopCaculatingPage{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.isStopCaculatingPage = YES;
}

-(void)jumpToPageIndex:(int)pageIndex isCurrentSpine:(BOOL)isCurrentSpine{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (pageIndex >= 1 && pageIndex <= self.pageCountForCurrentSpine) {
        if (isCurrentSpine) {            
            _currentPageIndex = pageIndex;            
            [self.theScrolLView setContentOffset:CGPointMake((self.currentPageIndex-1) * self.theScrolLView.bounds.size.width, 0) animated:YES];
        }else {
            _currentPageIndex = pageIndex;            
            [self.theScrolLView setContentOffset:CGPointMake((self.currentPageIndex-1) * self.theScrolLView.bounds.size.width, 0) animated:NO];
        }
        
        [self updateTOCIndexForCurrentPage];
    }else {
#ifdef DEBUGX
        NSLog(@"pageIndex out of range");
#endif
    }
}
-(void)gotoSpineIndex:(int)spineIndex atPageRatio:(float)pageRatio{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (spineIndex != self.currentSpineIndex) {        
        if (spineIndex >= 0 && spineIndex < self.epubUtils.spineArray.count) {
            self.currentSpineIndex = spineIndex;
            [self openLink:self.currentSpineItem.spineURL enableRender:YES];
            __block DatoEpubVC *myEpubVC = self;
            self.finishLoadBlock = ^(){
                [myEpubVC updatePageCountForCurrentSpine];
                
                int pageIndex = 1;
                if (pageRatio > 0 &&  pageRatio <= 1) {
                    pageIndex = round(pageRatio * self.pageCountForCurrentSpine);
                    if (pageIndex == 0) {
                        pageIndex = 1;
                    }
                }
#ifdef DEBUGX
                NSLog(@"page Index:%d",pageIndex);
#endif
                [myEpubVC jumpToPageIndex:pageIndex isCurrentSpine:NO];
            };
        }
    }else {
        int pageIndex = 1;
        if (pageRatio > 0 &&  pageRatio <= 1) {
            pageIndex = round(pageRatio * self.pageCountForCurrentSpine);
            if (pageIndex == 0) {
                pageIndex = 1;
            }
        }
#ifdef DEBUGX
        NSLog(@"page Index:%d",pageIndex);
#endif
        [self jumpToPageIndex:pageIndex isCurrentSpine:YES];
    }
}
-(void)gotoSpineIndex:(int)spineIndex atAnchor:(NSString *)anchor{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (spineIndex != self.currentSpineIndex) {        
        if (spineIndex >= 0 && spineIndex < self.epubUtils.spineArray.count) {
            self.currentSpineIndex = spineIndex;
            [self openLink:self.currentSpineItem.spineURL enableRender:YES];
            __block DatoEpubVC *myEpubVC = self;
            self.finishLoadBlock = ^(){
                [myEpubVC updatePageCountForCurrentSpine];
                
                [myEpubVC gotoAnchor:anchor];
            };
        }
    }else {
        [self gotoAnchor:anchor];
    }
}

-(void)gotoTOCIndex:(NSIndexPath *)tocIndex{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    EpubTOCItem *tocItem = [self.epubUtils getTOCItemAtIndexPath:tocIndex];
#ifdef DEBUGX
    NSLog(@"toc path:%@",tocItem.chapterURL.path);
#endif
    if (tocItem != nil) {        
        
        //Search spine index
        int spineIndex = -1;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        for (EpubSpineItem *spineItem in self.epubUtils.spineArray) {
            if ([fileManager contentsEqualAtPath:spineItem.spineURL.path andPath:tocItem.chapterURL.path]) {
                spineIndex = [self.epubUtils.spineArray indexOfObject:spineItem];
                break;
            }
        }
        
        if (spineIndex > -1) {
            if (tocItem.chapterURL.fragment != nil) {
                [self gotoSpineIndex:spineIndex atAnchor:tocItem.chapterURL.fragment];
            }else {
                [self gotoSpineIndex:spineIndex atPageRatio:0];
            }
        }
    }
}

-(void)gotoLeftPage{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.currentPageIndex > 1) {
        [self jumpToPageIndex:self.currentPageIndex-1 isCurrentSpine:YES];
    }else {
        //Load Previous Spine
        [self gotoPrevSpine];
    }
}
-(void)gotoRightPage{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.currentPageIndex < self.pageCountForCurrentSpine) {
        [self jumpToPageIndex:self.currentPageIndex+1 isCurrentSpine:YES];
    }else {
        //Load Next Spine
        [self gotoNextSpine];
    }
}

-(void)gotoNextSpine{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.currentSpineIndex >= 0 && self.currentSpineIndex < (self.epubUtils.spineArray.count - 1)) {
        [self gotoSpineIndex:(self.currentSpineIndex+1) atPageRatio:0];
    }
}
-(void)gotoPrevSpine{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.currentSpineIndex > 0 && self.currentSpineIndex < self.epubUtils.spineArray.count) {
        [self gotoSpineIndex:(self.currentSpineIndex-1) atPageRatio:1];
    }
}

-(int)searchPageIndexForAnchor:(NSString *)anchor{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Search page for anchor link
    float anchor_OffsetTop = [[self.theWebView stringByEvaluatingJavaScriptFromString:
                               [NSString stringWithFormat:@"MyAppGet_OffsetTop_ForAnchor('%@');",anchor]] floatValue];
    if (anchor_OffsetTop != -1) {
        float totalHeight = [[self.theWebView stringByEvaluatingJavaScriptFromString:@"MyAppGet_Content_ScrollHeight();"] floatValue];
        totalHeight = totalHeight - 90;
#ifdef DEBUGX
        NSLog(@"anchor position:%f",(anchor_OffsetTop/totalHeight));
        NSLog(@"scrolLHeight:%f",totalHeight);
        NSLog(@"anchor offset Top:%f",anchor_OffsetTop);
#endif
        return ceil(anchor_OffsetTop/totalHeight);
    }else {
        return NSNotFound;
    }
}
-(void)gotoAnchor:(NSString *)anchor{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int anchorPageIndex = [self searchPageIndexForAnchor:anchor];
    if (anchorPageIndex != NSNotFound) {        
        //Jump to that page
        [self jumpToPageIndex:anchorPageIndex isCurrentSpine:NO];
    }
}

#pragma mark - MBProgessHUD
-(void)showWaiting{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    // The hud will dispable all input on the view (use the higest view possible in the view hierarchy)
    if (self.HUD == nil) {        
        _HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:self.HUD];
        
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        self.HUD.delegate = self;
    }
	
	// Show the HUD while the provided method executes in a new thread
	[self.HUD show:YES];
}

-(void)hideWaiting{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self.HUD hide:YES];
}

#pragma mark - Getter and Setter for properties
-(NSMutableArray *)cssArray{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (_cssArray == nil) {
        _cssArray = [[NSMutableArray alloc] init ];
        
        //Get Default CSS
        NSString *defaultCSSPath = [[NSBundle mainBundle] pathForResource:kEPUB_DEFAULT_STYLE_FILE ofType:@"css"];
        NSError *error;
        NSString *defaultCSS = [NSString stringWithContentsOfFile:defaultCSSPath encoding:NSUTF8StringEncoding error:&error];
        if (error == nil) {
            CSSItem *defaultCSSItem = [[CSSItem alloc] initWithCSSString:defaultCSS replacementBlock:^NSMutableDictionary *() {
                NSMutableDictionary *replacementDic = [[NSMutableDictionary alloc] init];
                [replacementDic setValue:[NSString stringWithFormat:@"%f",self.theScrolLView.bounds.size.width] forKey:@"[webview-width]"];
                [replacementDic setValue:[NSString stringWithFormat:@"%f",self.theScrolLView.bounds.size.height] forKey:@"[webview-height]"];
                return replacementDic;
            }];
            [_cssArray addObject:defaultCSSItem];
        }else {
#ifdef DEBUGX
            NSLog(@"Error getting default css:%@",error);
#endif
        }
    }
    
    return _cssArray;
}

-(NSMutableArray *)jsArray{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (_jsArray == nil) {
        _jsArray = [[NSMutableArray alloc] init ];
        
        //Get Default JS
        NSString *defaultJSPath = [[NSBundle mainBundle] pathForResource:kEPUB_DEFAULT_JS_FILE ofType:@"js"];
        NSError *error;
        NSString *defaultJS = [NSString stringWithContentsOfFile:defaultJSPath encoding:NSUTF8StringEncoding error:&error];
        if (error == nil) {  
            JSItem *defaultJSItem = [[JSItem alloc] initWithJSString:defaultJS replacementBlock:^NSMutableDictionary *{
                return nil;
            }];
            [_jsArray addObject:defaultJSItem];
        }else {
#ifdef DEBUGX
            NSLog(@"Error getting default js:%@",error);
#endif
        }
    }
    
    return _jsArray;
}

-(void)setCurrentSpineIndex:(int)currentSpineIndex{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (currentSpineIndex >=0 && currentSpineIndex <self.epubUtils.spineArray.count) {
        _currentSpineIndex = currentSpineIndex;
        _currentSpineItem = [self.epubUtils.spineArray objectAtIndex:currentSpineIndex];
    }
}
-(void)setCurrentTOCIndexPath:(NSIndexPath *)currentTOCIndexPath{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (currentTOCIndexPath != nil) {
        _currentTOCIndexPath = currentTOCIndexPath;
        _currentTOCItem = [self.epubUtils getTOCItemAtIndexPath:currentTOCIndexPath];
    }
}

- (void)setNeedToRecalculatingPage:(BOOL)needToRecalculatingPage{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    _needToRecalculatingPage = needToRecalculatingPage;
    if (needToRecalculatingPage == TRUE) {
        //Reset pagecount for spineItem
        for (EpubSpineItem *spineItem in self.epubUtils.spineArray) {
            [spineItem resetPageOfSpine];
        }
        
        [self calculatePageForAllSpine];
    }else {
        if ([[self.epubUtils.spineArray objectAtIndex:0] startPageNumber] == NSNotFound) {
            [self calculatePageForAllSpine];
        }else {
            if ([self.delegate respondsToSelector:@selector(datoEpubStartCalculatePage:)]) {
                [self.delegate datoEpubStartCalculatePage:self];
            }
            if ([self.delegate respondsToSelector:@selector(datoEpubFinishCalculatePage:)]) {
                [self.delegate datoEpubFinishCalculatePage:self];
            }
        }
    }
}

-(int)totalPageCount{
    _totalPageCount = [self.epubUtils.spineArray.lastObject startPageNumber] + [self.epubUtils.spineArray.lastObject pageCount] - 1;
    return _totalPageCount;
}

#pragma mark - Others
-(void)copyPageNumberFromSpineArray:(NSArray *)spineArray{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.epubUtils.spineArray.count == spineArray.count) {
        for (int i=0; i<self.epubUtils.spineArray.count; i++) {
            [[self.epubUtils.spineArray objectAtIndex:i] copyPageNumberFrom:[spineArray objectAtIndex:i]];
        }
    }
}

-(void)copyPageNumberFromTOCArray:(NSArray *)tocArray{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.epubUtils.tocArray.count == tocArray.count) {
        for(int i=0; i<self.epubUtils.tocArray.count; i++){
            EpubTOCItem *tocItem = [self.epubUtils.tocArray objectAtIndex:i];
            [tocItem browseTOCTreeWithBlock:^(EpubTOCItem *tocItem, int depth) {
                NSIndexPath *tocIndexPath = [self.epubUtils indexPathOfTOCItem:tocItem];
                EpubTOCItem *savedTOCItem;
                for (int j=tocIndexPath.length-1;j>=0;j--) {
                    int indexValue = [tocIndexPath indexAtPosition:j];
                    if (j==tocIndexPath.length-1) {
                        savedTOCItem = [tocArray objectAtIndex:indexValue];
                    }else {
                        savedTOCItem = [savedTOCItem.subChapters objectAtIndex:indexValue];
                    }
                }
                
                [tocItem copyPageNumberFrom:savedTOCItem];
            }];
        }
    }
}

@end
