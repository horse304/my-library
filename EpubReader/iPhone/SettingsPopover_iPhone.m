//
//  SettingsPopover.m
//  nexto
//
//  Created by demoh on 3/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SettingsPopover_iPhone.h"
#import "EpubReader_iPhone.h"


@implementation SettingsPopover_iPhone

@synthesize book;

@synthesize delegate;

@synthesize sizeDecButton;
@synthesize sizeIncButton;
@synthesize modeDayButton;
@synthesize modeNightButton;
@synthesize modeSepiaButton;
@synthesize brightnessSlider;
@synthesize viewBookButton;
@synthesize viewLinearButton;
@synthesize marginesDecButton;
@synthesize marginesIncButton;
@synthesize align0Button;
@synthesize align1Button;
@synthesize align2Button;
@synthesize align3Button;
@synthesize font0Button;
@synthesize font1Button;
@synthesize font2Button;
@synthesize font3Button;
@synthesize font4Button;
@synthesize font5Button;
@synthesize font6Button;


- (IBAction)sizeDecClick:(id)sender
{
	[delegate fontSizeSelected:-1];
}


- (IBAction)sizeIncClick:(id)sender
{
	[delegate fontSizeSelected:1];
}


- (IBAction)modeDayClick:(id)sender
{
	[modeDayButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_push_iPad.png"] forState:UIControlStateNormal];
	[modeNightButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
	[modeSepiaButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	
	brightnessSlider.enabled = YES;
	brightnessSlider.value = 1.0;
	
	[delegate modeSelected:10];
}


- (IBAction)modeNightClick:(id)sender
{
	[modeDayButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
	[modeNightButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_push_iPad.png"] forState:UIControlStateNormal];
	[modeSepiaButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	
	brightnessSlider.enabled = YES;
	brightnessSlider.value = 0.0;
	
	[delegate modeSelected:0];
}


- (IBAction)modeSepiaClick:(id)sender
{
	[modeDayButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
	[modeNightButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
	[modeSepiaButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_push_iPad.png"] forState:UIControlStateNormal];
	
	brightnessSlider.enabled = NO;
	
	[delegate modeSelected:-1];
}


- (IBAction)modeChanged:(id)sender
{
	int value = (int)((brightnessSlider.value * 10.0) + 0.5);
	
	if(book.nightMode < 5)
	{
		[modeDayButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[modeNightButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_push_iPad.png"] forState:UIControlStateNormal];
		[modeSepiaButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
		
		brightnessSlider.enabled = YES;
	}
	else
	{
		[modeDayButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_push_iPad.png"] forState:UIControlStateNormal];
		[modeNightButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[modeSepiaButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
		
		brightnessSlider.enabled = YES;
	}
	
	[delegate modeSelected:value];
}


- (IBAction)viewBookClick:(id)sender
{
	[viewBookButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button_left_push_iPad.png"] forState:UIControlStateNormal];
	[viewLinearButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button_right_iPad.png"] forState:UIControlStateNormal];

	[delegate viewSelected:1];
	
}


- (IBAction)viewLinearClick:(id)sender
{
	[viewBookButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button_left_iPad.png"] forState:UIControlStateNormal];
	[viewLinearButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button_right_push_iPad.png"] forState:UIControlStateNormal];
	
	[delegate viewSelected:0];
}


- (IBAction)marginesDecClick:(id)sender
{
	[delegate marginSelected:-1];
}


- (IBAction)marginesIncClick:(id)sender
{
	[delegate marginSelected:1];
}


- (IBAction)align0Click:(id)sender
{
	[align0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_push_iPad.png"] forState:UIControlStateNormal];
	[align1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[align2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[align3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	
	[delegate alignSelected:0];
}


- (IBAction)align1Click:(id)sender
{
	[align0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[align1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_push_iPad.png"] forState:UIControlStateNormal];
	[align2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[align3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	
	[delegate alignSelected:1];
}


- (IBAction)align2Click:(id)sende
{
	[align0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[align1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[align2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_push_iPad.png"] forState:UIControlStateNormal];
	[align3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	
	[delegate alignSelected:2];
}


- (IBAction)align3Click:(id)sender
{
	[align0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[align1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[align2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[align3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_push_iPad.png"] forState:UIControlStateNormal];
	
	[delegate alignSelected:3];
}


- (IBAction)font0Click:(id)sender
{
	[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_push_iPad.png"] forState:UIControlStateNormal];
	[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
	[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
	[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	
	[delegate fontSelected:0];
}


- (IBAction)font1Click:(id)sender
{
	[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_push_iPad.png"] forState:UIControlStateNormal];
	[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
	[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
	[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	
	[delegate fontSelected:1];
}


- (IBAction)font2Click:(id)sender
{
	[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_push_iPad.png"] forState:UIControlStateNormal];
	[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
	[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
	[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	
	[delegate fontSelected:2];
}


- (IBAction)font3Click:(id)sender
{
	[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_push_iPad.png"] forState:UIControlStateNormal];
	[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
	[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
	[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	
	[delegate fontSelected:3];
}


- (IBAction)font4Click:(id)sender
{
	[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_push_iPad.png"] forState:UIControlStateNormal];
	[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
	[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	
	[delegate fontSelected:4];
}


- (IBAction)font5Click:(id)sender
{
	[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
	[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_push_iPad.png"] forState:UIControlStateNormal];
	[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	
	[delegate fontSelected:5];
}


- (IBAction)font6Click:(id)sender
{
	[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
	[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
	[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
	[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
	[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_push_iPad.png"] forState:UIControlStateNormal];
	
	[delegate fontSelected:6];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return(YES);
}


- (void)viewDidLoad
{
	if(book.bookMode == 1)
	{
		[viewBookButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button_left_push_iPad.png"] forState:UIControlStateNormal];
		[viewLinearButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button_right_iPad.png"] forState:UIControlStateNormal];
	}
	else
	{
		[viewBookButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button_left_iPad.png"] forState:UIControlStateNormal];
		[viewLinearButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button_right_push_iPad.png"] forState:UIControlStateNormal];
	}
	
	if(book.nightMode == -1)
	{
		[modeDayButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[modeNightButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[modeSepiaButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_push_iPad.png"] forState:UIControlStateNormal];
		
		brightnessSlider.enabled = NO;
	}
	else if(book.nightMode < 5)
	{
		[modeDayButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[modeNightButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_push_iPad.png"] forState:UIControlStateNormal];
		[modeSepiaButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
		
		brightnessSlider.enabled = YES;
		
		brightnessSlider.value = ((double)book.nightMode / 10.0);
	}
	else
	{
		[modeDayButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_push_iPad.png"] forState:UIControlStateNormal];
		[modeNightButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[modeSepiaButton setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
		
		brightnessSlider.enabled = YES;
		
		brightnessSlider.value = ((double)book.nightMode / 10.0);
	}
	
	if(book.align == 0)
	{
		[align0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_push_iPad.png"] forState:UIControlStateNormal];
		[align1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[align2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[align3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	}
	else if(book.align == 1)
	{
		[align0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[align1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_push_iPad.png"] forState:UIControlStateNormal];
		[align2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[align3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	}
	else if(book.align == 2)
	{
		[align0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[align1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[align2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_push_iPad.png"] forState:UIControlStateNormal];
		[align3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
	}
	else
	{
		[align0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[align1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[align2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[align3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_push_iPad.png"] forState:UIControlStateNormal];
	}

	if(book.font == 0)
	{
		[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_push_iPad.png"] forState:UIControlStateNormal];
		[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
		[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	}
	else if(book.font == 1)
	{
		[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_push_iPad.png"] forState:UIControlStateNormal];
		[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
		[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	}
	else if(book.font == 2)
	{
		[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_push_iPad.png"] forState:UIControlStateNormal];
		[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
		[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	}
	else if(book.font == 3)
	{
		[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_push_iPad.png"] forState:UIControlStateNormal];
		[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	}
	else if(book.font == 4)
	{
		[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
		[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_push_iPad.png"] forState:UIControlStateNormal];
		[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	}
	else if(book.font == 5)
	{
		[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
		[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_push_iPad.png"] forState:UIControlStateNormal];
		[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_iPad.png"] forState:UIControlStateNormal];
	}
	else
	{
		[font0Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button11_iPad.png"] forState:UIControlStateNormal];
		[font1Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font2Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button12_iPad.png"] forState:UIControlStateNormal];
		[font3Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button13_iPad.png"] forState:UIControlStateNormal];
		[font4Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button1_iPad.png"] forState:UIControlStateNormal];
		[font5Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button2_iPad.png"] forState:UIControlStateNormal];
		[font6Button setBackgroundImage:[UIImage imageNamed:@"settings_popover_button3_push_iPad.png"] forState:UIControlStateNormal];
	}
}
-(void)viewWillDisappear:(BOOL)animated{
#warning need to fix
    //((EpubReader_iPhone*)self.delegate)->isDismissFromSettingPopover = TRUE;
}

- (void)dealloc
{
	self.delegate = nil;
	
	[super dealloc];
}


@end

