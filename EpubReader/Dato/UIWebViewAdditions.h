//
//  UIWebViewAdditions
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIWebView(UIWebViewAdditions)
- (CGSize)windowSize;
- (CGPoint)scrollOffset;
@end
