//
//  SettingsPopover.h
//  nexto
//
//  Created by demoh on 3/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"


@protocol SettingsPopoverDelegate_iPhone

- (void)fontSizeSelected:(int)number;
- (void)modeSelected:(int)number;
- (void)viewSelected:(int)number;
- (void)marginSelected:(int)number;
- (void)alignSelected:(int)number;
- (void)fontSelected:(int)number;

@end

@interface SettingsPopover_iPhone : UIViewController
{
    Book *book;
	
	id<SettingsPopoverDelegate_iPhone> delegate;
	
	UIButton *sizeDecButton;
	UIButton *sizeIncButton;
	UIButton *modeDayButton;
	UIButton *modeNightButton;
	UIButton *modeSepiaButton;
	UISlider *brightnessSlider;
	UIButton *viewBookButton;
	UIButton *viewLinearButton;
	UIButton *marginesDecButton;
	UIButton *marginesIncButton;
	UIButton *align0Button;
	UIButton *align1Button;
	UIButton *align2Button;
	UIButton *align3Button;
	UIButton *font0Button;
	UIButton *font1Button;
	UIButton *font2Button;
	UIButton *font3Button;
	UIButton *font4Button;
	UIButton *font5Button;
	UIButton *font6Button;
}

@property (nonatomic, assign) Book *book;

@property (nonatomic, assign) id<SettingsPopoverDelegate_iPhone> delegate;

@property (nonatomic, assign) IBOutlet UIButton *sizeDecButton;
@property (nonatomic, assign) IBOutlet UIButton *sizeIncButton;
@property (nonatomic, assign) IBOutlet UIButton *modeDayButton;
@property (nonatomic, assign) IBOutlet UIButton *modeNightButton;
@property (nonatomic, assign) IBOutlet UIButton *modeSepiaButton;
@property (nonatomic, assign) IBOutlet UISlider *brightnessSlider;
@property (nonatomic, assign) IBOutlet UIButton *viewBookButton;
@property (nonatomic, assign) IBOutlet UIButton *viewLinearButton;
@property (nonatomic, assign) IBOutlet UIButton *marginesDecButton;
@property (nonatomic, assign) IBOutlet UIButton *marginesIncButton;
@property (nonatomic, assign) IBOutlet UIButton *align0Button;
@property (nonatomic, assign) IBOutlet UIButton *align1Button;
@property (nonatomic, assign) IBOutlet UIButton *align2Button;
@property (nonatomic, assign) IBOutlet UIButton *align3Button;
@property (nonatomic, assign) IBOutlet UIButton *font0Button;
@property (nonatomic, assign) IBOutlet UIButton *font1Button;
@property (nonatomic, assign) IBOutlet UIButton *font2Button;
@property (nonatomic, assign) IBOutlet UIButton *font3Button;
@property (nonatomic, assign) IBOutlet UIButton *font4Button;
@property (nonatomic, assign) IBOutlet UIButton *font5Button;
@property (nonatomic, assign) IBOutlet UIButton *font6Button;

- (IBAction)sizeDecClick:(id)sender;
- (IBAction)sizeIncClick:(id)sender;
- (IBAction)modeDayClick:(id)sender;
- (IBAction)modeNightClick:(id)sender;
- (IBAction)modeSepiaClick:(id)sender;
- (IBAction)modeChanged:(id)sender;
- (IBAction)viewBookClick:(id)sender;
- (IBAction)viewLinearClick:(id)sender;
- (IBAction)marginesDecClick:(id)sender;
- (IBAction)marginesIncClick:(id)sender;
- (IBAction)align0Click:(id)sender;
- (IBAction)align1Click:(id)sender;
- (IBAction)align2Click:(id)sender;
- (IBAction)align3Click:(id)sender;
- (IBAction)font0Click:(id)sender;
- (IBAction)font1Click:(id)sender;
- (IBAction)font2Click:(id)sender;
- (IBAction)font3Click:(id)sender;
- (IBAction)font4Click:(id)sender;
- (IBAction)font5Click:(id)sender;
- (IBAction)font6Click:(id)sender;

@end;