//
//  SpringBoardItemDelegate.h
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpringBoardItem.h"

@class SpringBoardItem;
@protocol SpringBoardItemDelegate <NSObject>
-(void)springBoardItemDidSelected:(SpringBoardItem *)sender;
@end
