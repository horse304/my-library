//
//  ModelDefine.h
//  SexFunFacts
//
//  Created by Techmaster on 8/4/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#define COREDATA_FILE_NAME @"SFF_Data"
#define LANGUAGE_CODE_KEY @"language_code"

#define ENTITY_CATEGORY @"Category"
#define ENTITY_CATEGORY_category_id @"category_id"
#define ENTITY_CATEGORY_SORTKEYDEFAULT @"category_id"

#define ENTITY_FACTITEM @"Factitem"
#define ENTITY_FACTITEM_toCategory @"toCategory"
#define ENTITY_FACTITEM_SORTKEYDEFAULT @"fact_createdDate"
