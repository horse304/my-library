//
//  XMLDOM.m
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//

#import "DatoXMLNode.h"

@implementation DatoXMLNode
@synthesize nodeName = _nodeName;
@synthesize nodeValue = _nodeValue;
@synthesize parentNode = _parentNode;
@synthesize childNodes = _childNodes;
@synthesize attributes = _attributes;

-(id)initWithNodeName:(NSString *)nodeName{
    self = [super init];
    if (self) {
        self.nodeName = nodeName;
        _childNodes = [[NSMutableArray alloc] init ];
        _attributes = [[NSMutableDictionary alloc] init ] ;
    }
    return self;
}

-(NSArray*)getElementsByTagName:(NSString *)tagName{
    NSMutableArray *itemArray = [[NSMutableArray alloc] init ];
    for (id i in self.childNodes) {
        if ([[i nodeName].lowercaseString isEqualToString:tagName.lowercaseString]) {
            [itemArray addObject:i];
        }
    }
    return [NSArray arrayWithArray:itemArray] ;
}
-(NSArray *)getElementsByValue:(NSString *)value OfAttribute:(NSString *)attribute{
    NSMutableArray *result = [[NSMutableArray alloc] init ];
    for (DatoXMLNode *childNode in self.childNodes) {
        NSString *attributeValue = [childNode.attributes valueForKey:attribute];
        if ([attributeValue.lowercaseString isEqualToString:value.lowercaseString]) {
            [result addObject:childNode];
        }
    }
    if (result.count != 0) {
        return result;
    }else {
        return nil;
    }
}

-(void)appendChild:(DatoXMLNode *)node{
    [self.childNodes addObject:node];
}

-(DatoXMLNode*)firstChild{
    return [self.childNodes objectAtIndex:0];
}

-(DatoXMLNode*)lastChild{
    return self.childNodes.lastObject;
}

-(void)removeChild:(DatoXMLNode *)node{
    [self.childNodes removeObject:node];
}
@end
