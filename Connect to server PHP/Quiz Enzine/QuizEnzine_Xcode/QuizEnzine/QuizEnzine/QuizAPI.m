//
//  QuizAPI.m
//  QuizEnzine
//
//  Created by Techmaster on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define kAppId 9999
#define kQuizAPIURL @"http://localhost/quizenzine/api/index.php?XDEBUG_SESSION_START=netbeans-xdebug"
#define kUserDefaultPropertyQuizAccesstoken @"QuizAccesstoken"
#define kUserDefaultPropertyExpirationDate @"QuizAccessTokenExpirationDate"

#import "QuizAPI.h"

@implementation QuizAPI
@synthesize afClient = _afClient;
@synthesize quizAccessToken = _quizAccessToken;
@synthesize quizAccessTokenExpirationDate = _quizAccessTokenExpirationDate;
@synthesize quizUserId = _quizUserId;

+(QuizAPI *)sharedQuizAPI{
    static QuizAPI *sharedSingleton;
    
    @synchronized(self)
    {
        if (!sharedSingleton)
            sharedSingleton = [[QuizAPI alloc] init];
        
        return sharedSingleton;
    }
}
-(id)init{
    self = [super init];
    if (self) {
        self.afClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kQuizAPIURL]];
        //Get user default
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:kUserDefaultPropertyQuizAccesstoken] && [defaults objectForKey:kUserDefaultPropertyExpirationDate]) {
            self.quizAccessToken = [defaults objectForKey:kUserDefaultPropertyQuizAccesstoken];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            self.quizAccessTokenExpirationDate = [dateFormatter dateFromString:[defaults objectForKey:kUserDefaultPropertyExpirationDate]];
        }
    }
    return self;
}

-(id)parseJSON:(NSData *)jsondata{
    NSError *error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:jsondata options:NSJSONReadingAllowFragments error:&error];
    if (error){
        return nil;
    }
    
    return result;
}

-(BOOL)isSessionValid{
    //Check Expiration Date
    if (self.quizAccessTokenExpirationDate != nil) {        
        if ([self.quizAccessTokenExpirationDate timeIntervalSinceNow]>0) {
            return TRUE;
        }else {
            return FALSE;
        }
    }else {
        return FALSE;
    }
}

-(void)saveAccessToken:(NSString *)accesstoken expiration:(NSDate *)date{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accesstoken forKey:kUserDefaultPropertyQuizAccesstoken];
    [defaults setObject:date forKey:kUserDefaultPropertyExpirationDate];
    [defaults synchronize];
}

-(void)requestLoginWithFacebook:(QE_UserInfo *)fbinfo delegate:(id<QuizAPIDelegate>)delegate{
    //Send request to server
    NSDictionary *postParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"authentication",@"class",
                                @"login_facebook", @"action",
                                [fbinfo getDictionary],@"fbinfo",
                                nil];
    [self.afClient postPath:@"" parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([delegate respondsToSelector:@selector(quizAPIResponseFacebookRequest:)]) {
            NSDictionary *result = [self parseJSON:responseObject];
            if ([[result valueForKey:@"result"] intValue] == 1) {
                NSString *accesstoken = [[result valueForKey:@"access_token"] valueForKey:@"accesstoken_id"];
                NSDate *expirationDate = [[result valueForKey:@"access_token"] valueForKey:@"expire_date"];
                [self saveAccessToken:accesstoken expiration:expirationDate];
            }
            [delegate quizAPIResponseFacebookRequest:result];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed");
    }];
}



/*
-(BOOL)checkAccessToken{
    NSDictionary *postParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"authentication",@"class",
                                @"check_access_token", @"action",
                                self.quizAccessToken,@"access_token",
                                self.quizUserId,@"uid",
                                [NSNumber numberWithInt:kAppId],@"appid",
                                nil];
    [self.afClient postPath:@"" parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self.delegate respondsToSelector:@selector(quizAPIResponseCheckAccessToken:sender:)]) {
            
            Boolean result = [[self parseJSON:responseObject] boolValue];
            [self.delegate quizAPIResponseCheckAccessToken:result sender:self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed");
    }];
    return FALSE;
}*/
@end
