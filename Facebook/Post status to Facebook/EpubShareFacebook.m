//
//  EpubShareFacebook.m
//  nexto
//
//  Created by Techmaster on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//#define DEBUGX
#define kFacebookAppId @"395600063814619"
#import "EpubShareFacebook.h"

@interface EpubShareFacebook ()
@property (retain, nonatomic) NSString *shareText;
@property (retain, nonatomic) Book *book;

//UI Properties
@property (strong, nonatomic) UILabel *textLabel;
@end

@implementation EpubShareFacebook
@synthesize               shareText = _shareText;
@synthesize                    book = _book;
@synthesize               textLabel = _textLabel;

-(id)init{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)loadView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super loadView];
    self.view.frame = self.view.superview.bounds;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view.hidden = YES;
    
    //Add Tapgesture on view
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideStatusAlert)];
    [self.view addGestureRecognizer:tapGesture];
    
    //Add Alert View
    UIView *alertView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 235, 81)];
    alertView.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    alertView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.view addSubview:alertView];
    
    //Add Image BackgroundView
    UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"alert_bg_iPad.png"]];
    [alertView addSubview:bgView];
    bgView.frame = alertView.bounds;
    
    //Add Text Label
    _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 100, 30)];
    self.textLabel.center = CGPointMake(alertView.bounds.size.width/2, alertView.bounds.size.height/2);
    self.textLabel.textAlignment = UITextAlignmentCenter;
    self.textLabel.textColor = [UIColor whiteColor];
    self.textLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    [alertView addSubview:self.textLabel];
}

-(void)share:(NSString *)textForSharing inBook:(Book *)book{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.shareText = textForSharing;
    self.book = book;
    if (((AppDelegate_iPad*)[UIApplication sharedApplication].delegate).internetReachable.currentReachabilityStatus != NotReachable) {
        Facebook *facebook = [[Facebook alloc] initWithAppId:kFacebookAppId andDelegate:self];
        ((AppDelegate_iPad *)[UIApplication sharedApplication].delegate).facebook = facebook; 
        //Check access token
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"FBAccessTokenKey"] && [defaults objectForKey:@"FBExpirationDateKey"]) {
            facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
            facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
        }
        if ([facebook isSessionValid]) {        
            [self postFacebook];
        }else {
            NSArray *permissionFB = [NSArray arrayWithObjects:
                                     @"publish_stream",
                                     nil]; 
            [facebook authorize:permissionFB];
        }
    }else {
        [self showStatusAlertWithText:@"Brak połączenia z Internetem ..."];
    }
}

-(void)hideStatusAlert{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.view.alpha = 0.9;
    [UIView animateWithDuration:0.5 animations:^{
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {        
        self.view.hidden = YES;
    }];
}
-(void)showStatusAlertWithText:(NSString *)text{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.textLabel.text = text;    
    self.view.hidden = NO;
    self.view.alpha = 0.0;
    self.view.transform = CGAffineTransformMakeScale(0.5, 0.5);
    [UIView animateWithDuration:0.5 animations:^{
        self.view.alpha = 0.9;
        self.view.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        if (finished == FALSE) {            
            self.view.alpha = 0.9;
            self.view.transform = CGAffineTransformIdentity;
        }
    }];
}

#pragma mark - Facebook
-(void)postFacebook{        
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"\"%@\"",self.shareText],@"message", 
                                   @"http://nexto.pl", @"link",
                                   @"https://lh5.googleusercontent.com/-u_FaTM4Tvc8/T_z8BsluVXI/AAAAAAAAKPg/HAqc0bb9AoA/s170/nextoreader.png",@"picture",
                                   [NSString stringWithFormat:@"Czyta ebook %@ na Nexto Reader dla iPad",self.book.title],@"name",
                                   self.book.title, @"caption",
                                   self.book.author, @"description",
                                   nil];
    Facebook *facebook = ((AppDelegate_iPad *)[UIApplication sharedApplication].delegate).facebook;
    [facebook requestWithGraphPath:@"me/feed" andParams:params andHttpMethod:@"POST" andDelegate:self];
    
}
#pragma mark - Facebook delegates
-(void)fbDidLogin{
    Facebook *facebook = ((AppDelegate_iPad *)[UIApplication sharedApplication].delegate).facebook;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    [self postFacebook];
}
-(void)fbDidLogout{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"]) {
        [defaults removeObjectForKey:@"FBAccessTokenKey"];
        [defaults removeObjectForKey:@"FBExpirationDateKey"];
        [defaults synchronize];
        
    }
}
-(void)fbDidNotLogin:(BOOL)cancelled{
    NSLog(@"Did cancelled");
}
-(void)fbDidExtendToken:(NSString *)accessToken expiresAt:(NSDate *)expiresAt{
    
}
- (void)fbSessionInvalidated{
    NSLog(@"Session Invalidate!");
}


#pragma mark - FBRequestDelegate
- (void)requestLoading:(FBRequest *)request{
    //Show MBHud Loading
}

/**
 * Called when an error prevents the request from completing successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error{
    //Show failed message
    //Show alert successfully
    [self showStatusAlertWithText:@"Nie udało się, spróbuj jeszcze raz!"];
    [self fbDidLogout];
}

/**
 * Called when a request returns and its response has been parsed into
 * an object.
 *
 * The resulting object may be a dictionary, an array or a string, depending
 * on the format of the API response. If you need access to the raw response,
 * use:
 *
 * (void)request:(FBRequest *)request
 *      didReceiveResponse:(NSURLResponse *)response
 */
- (void)request:(FBRequest *)request didLoad:(id)result{
    //NSLog(@"%@",result);
    //Show alert successfully
    [self showStatusAlertWithText:@"Udało się!"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    return YES;
}

@end
