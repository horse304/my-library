//
//  SpringBoardDelegate.h
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpringBoardViewController.h"
#import "SpringBoardItem.h"

@class SpringBoardViewController;

@protocol SpringBoardDatasource <NSObject>
-(int)springBoardNumberColumnOf:(SpringBoardViewController *)springBoardVC;
-(int)springBoardNumberRowOf:(SpringBoardViewController *)springBoardVC;
-(NSArray *)springBoardItemArrayOf:(SpringBoardViewController *)springBoardVC;
@end
