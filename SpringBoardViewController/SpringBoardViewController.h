//
//  SpringBoardViewController.h
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpringBoardDatasource.h"
#import "SpringBoardDelegate.h"

@interface SpringBoardViewController : UIViewController <UIScrollViewDelegate>
-(id)initWithDataSource:(id<SpringBoardDatasource>)datasource frame:(CGRect)frame;

@property (unsafe_unretained, nonatomic) id<SpringBoardDatasource> datasource;
@property (unsafe_unretained, nonatomic) id<SpringBoardDelegate> delegate;
@property (retain, nonatomic) NSMutableArray *itemArray;

-(void)reloadSpringBoard;
@end
