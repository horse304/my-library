<?php
class Authentication{
    public static function processAction($request){
        $requestVars = NULL;
        if($request->getData()!=NULL){
            $requestVars = $request->getData();
        }else{
            $requestVars = $request->getRequestVars();
        }
        if(isset ($requestVars['action'])){
            switch ($requestVars['action']){
                case 'check_access_token':
                    $uid = $requestVars['uid'];
                    $access_token = $requestVars['access_token'];
                    $appid = $requestVars['appid'];
                    return Authentication::check_access_token($uid, $access_token, $appid);
                    break;                case 'get_access_token':
                    break;
                case 'login':
                    $email = $requestVars['email'];
                    $password = $requestVars['password'];
                    return Authentication::login($email, $password);
                    break;
                case 'login_facebook':
                    $fbid = $requestVars['fbid'];
                    return Authentication::login_facebook($fbid);
                    break;
                case 'create_account':
                    $fullname = $requestVars['fullname'];
                    $email = $requestVars['email'];
                    $password = $requestVars['password'];
                    break;
            }
        }
    }
    
    private static function check_access_token($uid, $access_token_id, $appid){
        $result = FALSE;
        
        $config = $GLOBALS['Application']['config'];
        $database = $GLOBALS['Application']['database'];
        $access_token = $database->getAccessToken($access_token_id);
        
        if ($access_token != NULL){
            if ($access_token['status'] == TRUE && $uid == $access_token['userid'] && strtotime($access_token['expire_date']) >= time() && $appid == $config->config_appid){
                $result = TRUE;
            }
        }
        
        return $result;
    }

    private static function login($email, $password){
        $result = NULL;
        $result['result'] = FALSE;
        
        $database = $GLOBALS['Application']['database'];
        $user = $database->getUser($email);
        
        if($user != NULL){
            if($password == $user['user_password'] && $user['user_status'] == TRUE){
                $result['result'] = TRUE;
                //Create new access token and return to user
                $result['access_token'] = $database->create_access_token($user['userid']);
                $result['user'] = $user;
                unset($result['user']['user_password']);
            }
        }
        
        return $result;
    }
    
    private static function login_facebook($fbid){
        $result = NULL;
        $result['result'] = FALSE;
        $database = $GLOBALS['Application']['database'];
        $user_facebook = $database->getUserByFacebook($fbid);
        
        if($user_facebook != NULL && $user_facebook['user_status'] == TRUE){
            $result['result']=TRUE;
            //Create new access token for user
            $result['access_token'] = $database->create_access_token($user_facebook['userid']);
            $result['user'] = $user_facebook;
            unset($result['user']['user_password']);
        }
        
        return $result;
    }
}
?>
