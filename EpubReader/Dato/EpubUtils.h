//
//  EpubUtils.h
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//


#import <Foundation/Foundation.h>
#import "TBXML.h"
#import "DatoXMLParser.h"
#import "DatoXMLNode.h"
#import "EpubTOCItem.h"
#import "EpubSpineItem.h"
@protocol EpubUtilsDelegate<NSObject>
-(void)epubUtilsDidFinishPrepare:(id)sender;
@end

@class EpubTOCItem;

@interface EpubUtils : NSObject<DatoXMLParserDelegate>
-(id)initWithDirectory:(NSString*)directoryPath withDelegate:(id<EpubUtilsDelegate>)delegate;

@property (nonatomic, unsafe_unretained)  id<EpubUtilsDelegate>delegate;
@property (readonly, strong, nonatomic) DatoXMLNode *package;
@property (strong, nonatomic) NSMutableArray *tocArray;
@property (strong, nonatomic) NSMutableArray *spineArray;
@property (readonly, strong, nonatomic) NSString *coverImagePath;

-(void)prepareEpub;
-(NSIndexPath *)indexPathOfTOCItem:(EpubTOCItem *)tocItem;
-(EpubTOCItem *)getTOCItemAtIndexPath:(NSIndexPath *)indexPath;
//-(NSString*)getRootFilePath;
//+(BOOL)isAnchorLink:(NSString *)filePath;
@end
