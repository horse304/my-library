//
//  Shader.fsh
//  temp
//
//  Created by Jake Gundersen on 10/14/11.
//  Copyright (c) 2011 Interrobang Software, LLC. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
