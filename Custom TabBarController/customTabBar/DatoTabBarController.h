//
//  DatoTabBarController.h
//  customTabBar
//
//  Created by Techmaster on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+scaleToSize.h"

@interface DatoTabBarController : UITabBarController
//Custom properties for main view
@property (retain, nonatomic) UIColor *customMainBackgroundColor;
//Custom properties for tabbar
@property (retain, nonatomic) UIImage *customTabBarBackground;
@property (retain, nonatomic) UIColor *customTabBarTintColor;
//Custom properties for tab bar item
@property (retain, nonatomic) UIImage *customSelectionIndicatorImage;
@property (retain, nonatomic) UIColor *customSelectedImageTintColor;
@property (retain, nonatomic) UIFont  *customTabBarItemTextFont;
@property (retain, nonatomic) UIColor *customTabBarItemSelectedTextColor;
@property (retain, nonatomic) UIColor *customTabBarItemUnselectedTextColor;
@property (retain, nonatomic) UIColor *customTabBarItemSelectedTextShadowColor;
@property (retain, nonatomic) UIColor *customTabBarItemUnselectedTextShadowColor;
@property (retain, nonatomic) NSValue *customTabBarItemTextShadowOffset;

//Custom properties for separation
-(void)makeCustomize;
-(void)customizeTabBarItem:(UITabBarItem *)tabBarItem withSelectedImage:(UIImage *)selectedImg unselectedImage:(UIImage *)unselectedImg;
@end
