//
//  DatoEpubSpineItem.h
//  nexto
//
//  Created by Techmaster on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EpubSpineItem;
typedef void (^FinishCalculate)(BOOL isFinished, EpubSpineItem *spineItem);

@interface EpubSpineItem : NSObject <UIWebViewDelegate, NSCoding>{
    int pageCount_Landscape;
    int pageCount_Portrait;
    int startPageNumber_Landscape;
    int startPageNumber_Portrait;
}
@property (retain, nonatomic) NSURL *spineURL;
@property (assign, nonatomic) BOOL isLinear;
@property (assign, nonatomic) int pageCount;
@property (assign, nonatomic) int startPageNumber;
@property (copy, nonatomic) FinishCalculate finishCalculateBlock;

-(void)calculatePageCountWithSize:(CGSize)webViewSize target:(id)target renderSEL:(SEL)renderSEL finishBlock:(FinishCalculate)finishBlock;
-(void)calculatePageForTocActive:(NSMutableArray *)tocActiveArray;
-(void)resetPageOfSpine;
-(void)copyPageNumberFrom:(EpubSpineItem*)spineItem;
@end
