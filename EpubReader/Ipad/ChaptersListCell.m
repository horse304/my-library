//
//  ChaptersListCell.m
//  nexto
//
//  Created by Dato - Techmaster.vn on 2012-07-27.
//  Copyright 2012 Dato. All rights reserved.
//

#import "ChaptersListCell.h"

@implementation ChaptersListCell
@synthesize tocItem = _tocItem;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
