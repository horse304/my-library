//
//  AppDelegate.m
//  customTabBar
//
//  Created by Techmaster on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "DatoTabBarController.h"

@implementation AppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    DatoTabBarController *tbc = [[DatoTabBarController alloc] init];
    tbc.customMainBackgroundColor = [UIColor whiteColor];
    tbc.customTabBarBackground = [UIImage imageNamed:@"tabBar_bg.png"];
    //tbc.customSelectionIndicatorImage = [UIImage imageNamed:@"tabBarItemSelectionIndicator.png"];
    tbc.customSelectedImageTintColor = [UIColor blueColor];
    tbc.customTabBarItemTextFont = [UIFont fontWithName:@"Chalkduster" size:10];
    tbc.customTabBarItemSelectedTextColor = [UIColor redColor];
    tbc.customTabBarItemUnselectedTextColor = [UIColor whiteColor];
    NSArray *arrayViewControllers = [NSArray arrayWithObjects:
                                     [[UIViewController alloc] init],
                                     [[UIViewController alloc] init],
                                     [[UIViewController alloc] init],    
                                     nil];    
    tbc.viewControllers = arrayViewControllers;
    ((UITabBarItem *)[tbc.tabBar.items objectAtIndex:0]).title = @"Home";
    ((UITabBarItem *)[tbc.tabBar.items objectAtIndex:0]).image = [UIImage imageNamed:@"icon1.png"];  
    ((UITabBarItem *)[tbc.tabBar.items objectAtIndex:1]).title = @"VC 2";
    ((UITabBarItem *)[tbc.tabBar.items objectAtIndex:1]).image = [UIImage imageNamed:@"icon2.png"];
    ((UITabBarItem *)[tbc.tabBar.items objectAtIndex:2]).title = @"VC 3";
    ((UITabBarItem *)[tbc.tabBar.items objectAtIndex:2]).image = [UIImage imageNamed:@"icon3.png"];
    
    [tbc makeCustomize];
    self.window.rootViewController = tbc;
    // Override point for customization after application launch.
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
