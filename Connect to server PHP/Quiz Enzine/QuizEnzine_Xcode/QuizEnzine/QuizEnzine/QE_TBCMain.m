//
//  QE_TBCExploreQuiz.m
//  QuizEnzine
//
//  Created by Techmaster on 6/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QE_TBCMain.h"
@interface QE_TBCMain()
//User Interface Properties

//User Interface Action

//Class Properties
@property (strong, nonatomic) QE_VCExploreQuiz *vcExploreQuiz;
@property (strong, nonatomic) QE_VCMyQuiz *vcMyQuiz;
@property (strong, nonatomic) QE_VCProgress *vcProgress;
@end

@implementation QE_TBCMain
@synthesize vcExploreQuiz = _vcExploreQuiz, vcMyQuiz = _vcMyQuiz, vcProgress = _vcProgress;

-(id)init{
    self = [super init];
    if (self) {
        self.vcExploreQuiz = [[QE_VCExploreQuiz alloc] init];
        self.vcMyQuiz = [[QE_VCMyQuiz alloc] init];
        self.vcProgress = [[QE_VCProgress alloc] init];
        
        self.viewControllers = [NSArray arrayWithObjects:self.vcMyQuiz, self.vcExploreQuiz, self.vcProgress, nil];
        
        //Set title for tab bar button
        self.vcMyQuiz.tabBarItem.title = @"My Quiz";
        self.vcExploreQuiz.tabBarItem.title = @"Explore Quiz";
        self.vcProgress.tabBarItem.title = @"Progress";
    }
    return self;
}
@end
