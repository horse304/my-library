//
//  ViewController.m
//  DemoCoreImage
//
//  Created by Techmaster on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "LoadPlist.h"

@interface ViewController ()
@property (strong, nonatomic) CIContext *ciContext;
@property (strong, nonatomic) CIFilter *currentFilter;
@property (strong, nonatomic) UIImage *normalImage;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@end

@implementation ViewController
@synthesize currentFilter = _currentFilter;
@synthesize ciContext = _ciContext;
@synthesize normalImage = _normalImage;
@synthesize scrollView = _scrollView;
@synthesize imgView = _imgView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Create CIContext
    self.ciContext = [CIContext contextWithOptions:nil];
    
    //Open normal image
    self.normalImage = [UIImage imageNamed:@"Obama_Face.jpg"];
    self.imgView.image = self.normalImage;
    
    //Create filter objects
    [self addFilters];
}

- (void)viewDidUnload
{
    [self setImgView:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)addFilters{
    //Load plist and add to scrollview
    FilterView *normal = [[FilterView alloc] initWithFilter:nil 
                                                    context:self.ciContext
                                                  thumbnail:[UIImage imageNamed:@"Obama_Face.jpg"]
                                                      frame:CGRectMake(0, 0, 80, 80)];
    CIImage *ciImage = [CIImage imageWithCGImage:self.normalImage.CGImage];
    normal.delegate = self;
    [self.scrollView addSubview:normal];
    
    
    NSArray *filterArray = [LoadPlist getFiltersArray];
    int count = 1;
    for (CIFilter *fltr in filterArray) {
        if (fltr != nil) {  
            NSLog(@"%@",fltr.attributes);
            if ([fltr.attributes objectForKey:kCIInputImageKey] != nil) {
                [fltr setValue:ciImage forKey:kCIInputImageKey];
                FilterView *filterView = [[FilterView alloc] initWithFilter:fltr 
                                                                    context:self.ciContext
                                                                  thumbnail:[UIImage imageNamed:@"Obama_Face.jpg"]
                                                                      frame:CGRectMake(count * 80, 0, 80, 80)];
                [self.scrollView addSubview:filterView];
                filterView.delegate = self;
                count++;
            }
        }
    }
    self.scrollView.contentSize = CGSizeMake(count*80, self.scrollView.frame.size.height);
}
#pragma mark - FilterViewDelegate
-(void)filterViewDidTap:(id)sender{
    self.currentFilter = [sender ciFilter];
    if (self.currentFilter != nil) {        
        [self.currentFilter.attributes setValue:self.normalImage forKey:kCIInputImageKey];
        CIImage *ciOutputImg = [self.currentFilter outputImage];
        CGImageRef cgOutputImg = [self.ciContext createCGImage:ciOutputImg fromRect:[ciOutputImg extent]];
        self.imgView.image = [UIImage imageWithCGImage:cgOutputImg];
    }else {        
        self.imgView.image = self.normalImage;
    }
}

@end
