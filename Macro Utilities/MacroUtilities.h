//
//  MacroUtilities.h
//  MKNetworkKitExtend
//
//  Created by Dato - horseuvn@gmail.com on 9/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

/************************ Enable hoac disable cac nhom macro ************************/

//Comment dong duoi de disable chuc nang show log
//#define MU_DEBUG

/************************ Khai bao cac macro ************************/

/*
 * Macro duong dan thu muc dac biet
 */
#ifndef MU_DOCUMENT_DIRECTORY_PATH
#   define MU_DOCUMENT_DIRECTORY_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#endif

#ifndef MU_LIBRARY_DIRECTORY_PATH
#   define MU_LIBRARY_DIRECTORY_PATH [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#endif

#ifndef MU_CREATE_UUID
#   define MU_CREATE_UUID ((__bridge NSString *)CFUUIDCreateString(NULL, CFUUIDCreate(NULL)))
#endif

/*
 *  Macro de xac dinh kieu cua NSNumber
 */
#ifndef MU_NSNUMBER_IS_BOOLEAN
#   define MU_NSNUMBER_IS_BOOLEAN(v) (strcmp([v objCType], @encode(BOOL)) == 0)
#endif

#ifndef MU_NSNUMBER_IS_INTERGER
#   define MU_NSNUMBER_IS_INTERGER(v) (strcmp([v objCType], @encode(NSInteger)) == 0)
#endif

#ifndef MU_NSNUMBER_IS_DOUBLE
#   define MU_NSNUMBER_IS_DOUBLE(v) (strcmp([v objCType], @encode(double)) == 0)
#endif

#ifndef MU_NSNUMBER_IS_FLOAT
#   define MU_NSNUMBER_IS_FLOAT(v) (strcmp([v objCType], @encode(float)) == 0)
#endif

/*
 *  Macro cho viec debug
 */

#ifdef MU_DEBUG
#   ifndef MU_DLog
#       define MU_DLog(fmt, ...) {NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);}
#   endif
#   ifndef MU_ELog
#       define MU_ELog(err) {if(err) DLog(@"%@", err)}
#   endif
#else
#   ifndef MU_DLog
#       define MU_DLog(...)
#   endif
#   ifndef MU_ELog
#       define MU_ELog(err)
#   endif
#endif


/*
 *  System Versioning Preprocessor Macros
 */
#ifndef MU_SYSTEM_VERSION_EQUAL_TO
#   define MU_SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#endif

#ifndef MU_SYSTEM_VERSION_GREATER_THAN
#   define MU_SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#endif

#ifndef MU_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO
#   define MU_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#endif

#ifndef MU_SYSTEM_VERSION_LESS_THAN
#   define MU_SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#endif

#ifndef MU_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO
#   define MU_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#endif    

/*
 *  Singleton Pattern Preprocessor Macros
 */

#ifndef MU_SINGLETON_INTERFACE_PATTERN
#   define MU_SINGLETON_INTERFACE_PATTERN(class_name) + (class_name *) shared;
#endif  

#ifndef MU_SINGLETON_IMPLEMENTATION_PATTERN
#   define MU_SINGLETON_IMPLEMENTATION_PATTERN(class_name) \
    static class_name *_instance = nil;\
    + (class_name *) shared\
    {\
        @synchronized([class_name class])\
        {\
            if (!_instance)\
            {\
                _instance = [[self alloc] init];\
            }\
            \
            return _instance;\
        }\
        \
        return nil;\
    }\
    + (id) alloc\
    {\
        @synchronized([class_name class])\
        {\
            NSAssert(_instance == nil, @"Attempted to allocate a second instance of a singleton.");\
            _instance = [super alloc];\
            return _instance;\
        }\
        return nil;\
    }
#endif  

/*
 *  Macros cho phep so sanh
 */

#ifndef AND
#   define AND &&
#endif

#ifndef OR
#   define OR ||
#endif

#ifndef EQUALS
#   define EQUALS ==
#endif

/*
 *  Macros kiem tra nam nhuan
 */
#ifndef MU_IS_LEAP_YEAR
#   define MU_IS_LEAP_YEAR(year) (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0))
#endif