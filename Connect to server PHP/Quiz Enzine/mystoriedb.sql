-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2012 at 05:21 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mystoriedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ms_accesstoken`
--

CREATE TABLE IF NOT EXISTS `ms_accesstoken` (
  `accesstoken_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `userid` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`accesstoken_id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ms_audio`
--

CREATE TABLE IF NOT EXISTS `ms_audio` (
  `audio_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `audio_url` varchar(254) NOT NULL,
  `userid` mediumint(8) unsigned DEFAULT NULL,
  `audio_created_date` date NOT NULL,
  PRIMARY KEY (`audio_id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ms_location`
--

CREATE TABLE IF NOT EXISTS `ms_location` (
  `location_id` mediumint(8) unsigned NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `location_name` varchar(100) DEFAULT NULL,
  `location_created_date` date NOT NULL,
  `userid` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`location_id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_note`
--

CREATE TABLE IF NOT EXISTS `ms_note` (
  `note_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `story_id` mediumint(8) unsigned NOT NULL,
  `note_title` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `note_content` text CHARACTER SET utf8,
  `pic_id` mediumint(8) unsigned DEFAULT NULL,
  `audio_id` mediumint(8) unsigned DEFAULT NULL,
  `location_id` mediumint(8) unsigned DEFAULT NULL,
  `video_id` mediumint(8) unsigned DEFAULT NULL,
  `note_created_date` date NOT NULL,
  `note_modified_date` date NOT NULL,
  PRIMARY KEY (`note_id`),
  KEY `story_id` (`story_id`,`pic_id`,`location_id`),
  KEY `audio_id` (`audio_id`),
  KEY `pic_id` (`pic_id`,`location_id`,`video_id`),
  KEY `location_id` (`location_id`,`video_id`),
  KEY `video_id` (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ms_picture`
--

CREATE TABLE IF NOT EXISTS `ms_picture` (
  `pic_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pic_original_link` varchar(254) NOT NULL,
  `pic_thumbnail_link` varchar(254) NOT NULL,
  `userid` mediumint(8) unsigned DEFAULT NULL,
  `pic_created_date` date NOT NULL,
  PRIMARY KEY (`pic_id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ms_story`
--

CREATE TABLE IF NOT EXISTS `ms_story` (
  `story_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) unsigned DEFAULT NULL,
  `story_title` varchar(254) CHARACTER SET utf8 DEFAULT NULL,
  `story_description` text CHARACTER SET utf8,
  `story_thumbnail_id` mediumint(8) unsigned DEFAULT NULL,
  `story_created_date` date NOT NULL,
  `story_update_date` date NOT NULL,
  PRIMARY KEY (`story_id`),
  KEY `userid` (`userid`),
  KEY `story_thumbnail_id` (`story_thumbnail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ms_tag`
--

CREATE TABLE IF NOT EXISTS `ms_tag` (
  `tag_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(254) CHARACTER SET utf8 NOT NULL,
  `tag_created_date` date NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ms_user`
--

CREATE TABLE IF NOT EXISTS `ms_user` (
  `userid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User id',
  `user_fullname` varchar(30) CHARACTER SET utf8 DEFAULT 'Unknown' COMMENT 'User full name',
  `user_email` varchar(254) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL COMMENT 'User email address',
  `user_password` char(32) DEFAULT NULL COMMENT 'User MD5 password',
  `user_pic` mediumint(8) unsigned DEFAULT NULL,
  `user_fbid` varchar(254) DEFAULT NULL COMMENT 'User Facebook Id',
  `user_join_date` date NOT NULL COMMENT 'User Join Date',
  `user_status` int(11) NOT NULL DEFAULT '1' COMMENT 'User Status',
  PRIMARY KEY (`userid`),
  KEY `user_pic` (`user_pic`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ms_video`
--

CREATE TABLE IF NOT EXISTS `ms_video` (
  `video_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `video_url` varchar(254) NOT NULL,
  `video_thumbnail_id` mediumint(8) unsigned DEFAULT NULL,
  `userid` mediumint(8) unsigned DEFAULT NULL,
  `video_created_date` date NOT NULL,
  PRIMARY KEY (`video_id`),
  KEY `video_thumbnail_id` (`video_thumbnail_id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ms_accesstoken`
--
ALTER TABLE `ms_accesstoken`
  ADD CONSTRAINT `ms_accesstoken_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `ms_user` (`userid`) ON DELETE SET NULL;

--
-- Constraints for table `ms_audio`
--
ALTER TABLE `ms_audio`
  ADD CONSTRAINT `ms_audio_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `ms_user` (`userid`) ON DELETE SET NULL;

--
-- Constraints for table `ms_location`
--
ALTER TABLE `ms_location`
  ADD CONSTRAINT `ms_location_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `ms_user` (`userid`) ON DELETE SET NULL;

--
-- Constraints for table `ms_note`
--
ALTER TABLE `ms_note`
  ADD CONSTRAINT `ms_note_ibfk_5` FOREIGN KEY (`video_id`) REFERENCES `ms_video` (`video_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `ms_note_ibfk_1` FOREIGN KEY (`story_id`) REFERENCES `ms_story` (`story_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ms_note_ibfk_2` FOREIGN KEY (`pic_id`) REFERENCES `ms_picture` (`pic_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `ms_note_ibfk_3` FOREIGN KEY (`audio_id`) REFERENCES `ms_audio` (`audio_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `ms_note_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `ms_location` (`location_id`) ON DELETE SET NULL;

--
-- Constraints for table `ms_picture`
--
ALTER TABLE `ms_picture`
  ADD CONSTRAINT `ms_picture_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `ms_user` (`user_pic`) ON DELETE SET NULL;

--
-- Constraints for table `ms_story`
--
ALTER TABLE `ms_story`
  ADD CONSTRAINT `ms_story_ibfk_4` FOREIGN KEY (`story_thumbnail_id`) REFERENCES `ms_picture` (`pic_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `ms_story_ibfk_3` FOREIGN KEY (`userid`) REFERENCES `ms_user` (`userid`) ON DELETE SET NULL;

--
-- Constraints for table `ms_user`
--
ALTER TABLE `ms_user`
  ADD CONSTRAINT `ms_user_ibfk_1` FOREIGN KEY (`user_pic`) REFERENCES `ms_picture` (`pic_id`) ON DELETE SET NULL;

--
-- Constraints for table `ms_video`
--
ALTER TABLE `ms_video`
  ADD CONSTRAINT `ms_video_ibfk_4` FOREIGN KEY (`userid`) REFERENCES `ms_user` (`userid`) ON DELETE SET NULL,
  ADD CONSTRAINT `ms_video_ibfk_3` FOREIGN KEY (`video_thumbnail_id`) REFERENCES `ms_picture` (`pic_id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
