//
//  QuizAPI.m
//  QuizEnzine
//
//  Created by Techmaster on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define kAppId 9999
#define kQuizAPIURL @"http://localhost/quizenzine/api/index.php?XDEBUG_SESSION_START=netbeans-xdebug"

#import "QuizAPI.h"

@implementation QuizAPI
@synthesize delegate = _delegate;
@synthesize afClient = _afClient;
@synthesize quizAccessToken = _quizAccessToken;
@synthesize quizUserId = _quizUserId;

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.afClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kQuizAPIURL]];
    }
    return self;
}

-(id)parseJSON:(NSData *)jsondata{
    NSError *error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:jsondata options:NSJSONReadingAllowFragments error:&error];
    if (error){
        return nil;
    }
    
    return result;
}

-(BOOL)checkAccessToken{
    NSDictionary *postParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"authentication",@"class",
                                @"check_access_token", @"action",
                                self.quizAccessToken,@"access_token",
                                self.quizUserId,@"uid",
                                [NSNumber numberWithInt:kAppId],@"appid",
                                nil];
    [self.afClient postPath:@"" parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self.delegate respondsToSelector:@selector(quizAPIResponseCheckAccessToken:sender:)]) {
            
            Boolean result = [[self parseJSON:responseObject] boolValue];
            [self.delegate quizAPIResponseCheckAccessToken:result sender:self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed");
    }];
    return FALSE;
}
@end
