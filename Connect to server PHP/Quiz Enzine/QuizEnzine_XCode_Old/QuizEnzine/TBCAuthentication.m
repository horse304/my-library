//
//  TBCAuthentication.m
//  QuizEnzine
//
//  Created by Dato on 6/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TBCAuthentication.h"
#import "AppDelegate.h"

@interface TBCAuthentication ()

@end

@implementation TBCAuthentication
@synthesize         vcLogin = _vcLogin,
            vcCreateAccount = _vcCreateAccount;

- (id)init
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {        
        self = [super initWithNibName:@"TBCAuthentication_iPhone" bundle:nil];
    } else {
        self = [super initWithNibName:@"TBCAuthentication_iPad" bundle:nil];
    }
    if (self) {
    }
    return self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];   
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    //Check session
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"QuizAccessToken"] 
        && [defaults objectForKey:@"QuizExpirationDate"]) {
        appdelegate.quizAPI.quizAccessToken = [defaults objectForKey:@"QuizAccessToken"];
        appdelegate.quizAPI.quizUserId = [defaults objectForKey:@"QuizUserId"];
    }
    appdelegate.quizAPI = [[QuizAPI alloc] initWithDelegate:self];
    [appdelegate.quizAPI checkAccessToken];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setVcLogin:nil];
    [self setVcCreateAccount:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


#pragma mark - QuizAPIDelegate
-(void)quizAPIResponseCheckAccessToken:(BOOL)result sender:(id)sender{
    if (result) {
        //Show logged in
        
    }
}
@end
