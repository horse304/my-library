//
//  ViewController.h
//  DemoCoreImage
//
//  Created by Techmaster on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <CoreImage/CoreImage.h>
#import <UIKit/UIKit.h>
#import "FilterView.h"

@interface ViewController : UIViewController<UIScrollViewDelegate,FilterViewDelegate>

@end
