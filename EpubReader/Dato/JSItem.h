//
//  JSItem.h
//  nexto
//
//  Created by Techmaster on 7/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface JSItem : NSObject
typedef NSMutableDictionary *(^JSReplacementBlock)();
@property (retain, nonatomic) NSString *jsString;
@property (copy, nonatomic) JSReplacementBlock replacementBlock;

-(id)initWithJSString:(NSString*)jsString 
      replacementBlock:(JSReplacementBlock)block;
-(NSString *)makeReplacement;
@end
