//
//  Category.h
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CoreDataController.h"

@class Factitem;

@interface Category : NSManagedObject

@property (nonatomic, retain) NSNumber * category_id;
@property (nonatomic, retain) NSString * category_name;
@property (nonatomic, retain) NSString * language_code;
@property (nonatomic, retain) NSString * category_image_path;
@property (nonatomic, retain) NSString * full_category_image_path;
@property (nonatomic, retain) NSSet *toFactitem;
@end

@interface Category (CoreDataGeneratedAccessors)

- (void)addToFactitemObject:(Factitem *)value;
- (void)removeToFactitemObject:(Factitem *)value;
- (void)addToFactitem:(NSSet *)values;
- (void)removeToFactitem:(NSSet *)values;

@end
