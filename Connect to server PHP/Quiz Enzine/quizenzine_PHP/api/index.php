<?php
include 'RestUtils.php';
include 'database/database.php';
include 'config.php';

//Initial request
$Application = NULL;
$Application['config'] = new Config();
$Application['request'] = RestUtils::processRequest(); 
$Application['database'] = new Database();

//Processing
$result = NULL;
switch($Application['request']->getMethod())  
{
    case 'get':  
        $requestVars = $Application['request']->getRequestVars();
        if (isset ($requestVars['class']))
        {
            switch ($requestVars['class']){
                case 'authentication':
                    include 'class/authentication.php';
                    $result = Authentication::processAction($Application['request']);
                    break;
                case 'user':
                    include 'class/user.php';
                    //User::process
                    break;
            }
        }
        break;  
    case 'post':  
        if($Application['request']->getData()==NULL){
            $requestVars['appid'] = json_decode($requestVars['appid']);
            $requestVars = $Application['request']->getRequestVars();
            if (isset ($requestVars['class']))
            {
                switch ($requestVars['class']){
                    case 'authentication':
                        include 'class/authentication.php';
                        $result = Authentication::processAction($Application['request']);
                        break;
                    case 'user':
                        include 'class/user.php';
                        //User::process
                        break;
                }
            }
        }  else {
            $data = $Application['request']->getData();
            if (isset ($data['class']))
            {
                switch ($data['class']){
                    case 'authentication':
                        include 'class/authentication.php';
                        $result = Authentication::processAction($Application['request']);
                        break;
                    case 'user':
                        include 'class/user.php';
                        //User::process
                        break;
                }
            }
        }
        break;  
    // etc, etc, etc...  
}

//Send response
if(isset ($result)){
    RestUtils::sendResponse(200, $result);
}  else {
    RestUtils::sendResponse(401);
}
?>
