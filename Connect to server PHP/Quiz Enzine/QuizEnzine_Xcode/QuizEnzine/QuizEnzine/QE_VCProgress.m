//
//  QE_VCProgress.m
//  QuizEnzine
//
//  Created by Techmaster on 6/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QE_VCProgress.h"

@interface QE_VCProgress ()
//User Interface Properties

//User Interface Action

//Class Properties
@end

@implementation QE_VCProgress

- (id)init
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {        
        self = [super initWithNibName:@"QE_VCProgress_iPhone" bundle:nil];
    } else {
        self = [super initWithNibName:@"QE_VCProgress_iPad" bundle:nil];
    }
    if (self) {
    }
    return self;
}
#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];    
}
- (void)viewWillAppear:(BOOL)animated{
    
}
- (void)viewDidAppear:(BOOL)animated{
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}
#pragma mark - Rotation handling
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    
}

#pragma mark - Getter and Setter

@end
