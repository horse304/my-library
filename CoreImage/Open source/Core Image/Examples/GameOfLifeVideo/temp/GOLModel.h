//
//  GOLModel.h
//  imagines
//
//  Created by Jake Gundersen on 10/13/11.
//  Copyright (c) 2011 University of Utah Hospitals and Clinics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GOLModel : NSObject {
    NSMutableArray *cells;
    NSUInteger width;
    NSUInteger height;
}

@property NSUInteger width;
@property NSUInteger height;
@property (nonatomic, retain) NSMutableArray *cells;

-(void)update;
-(BOOL)aliveForCellX:(int)x andY:(int)y;
-(id)initGameWidth:(NSUInteger)gWidth andHeight:(NSUInteger)gHeight;
-(void)toggleCellX:(NSUInteger)x andY:(NSUInteger)y;
- (int)findNeighboursForCellAtColumn:(NSUInteger)col andRow:(NSUInteger)row;
-(void)randomPopulate;
-(void)spawnWalkerAtCellX:(int)x andY:(int)y;

@end
