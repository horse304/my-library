//
//  DatoEpubViewController.h
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 7/17/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EpubUtils.h"
#import "CSSItem.h"
#import "JSItem.h"
#import "HTMLUtils.h"
#import "ZipArchive.h"
#import "MBProgressHUD.h"
#import "NSDataAdditions.h"

@protocol DatoEpubVCDelegate <NSObject>
-(void)datoEpubVCDidFinishPrepare:(id)sender;
-(void)datoEpubVCDidFailed:(id)sender withError:(NSString *)error;
-(void)datoEpubVCDidFinishLoadSpine:(id)sender;
-(void)datoEpubStartCalculatePage:(id)sender;
-(void)datoEpubFinishCalculateForSpineItem:(EpubSpineItem *)spineItem;
-(void)datoEpubFinishCalculatePage:(id)sender;
@end

typedef void (^WebViewFinishBlock)();

@interface DatoEpubVC : UIViewController <EpubUtilsDelegate , UIWebViewDelegate, MBProgressHUDDelegate>
-(id)initWithEpubFilePath:(NSString *)filePath;
//-(id)initWithEpubURL:(NSURL *)epubURL;

//UI Properties
@property (strong, nonatomic) UIWebView *theWebView;
@property (retain, nonatomic) UIScrollView *theScrolLView;

//Public Properties
@property (unsafe_unretained, nonatomic) id<DatoEpubVCDelegate> delegate;
@property (retain, nonatomic) EpubUtils *epubUtils;
@property (strong, nonatomic) NSMutableArray *cssArray;
@property (strong, nonatomic) NSMutableArray *jsArray;
@property (retain, nonatomic) NSString *epubFilePath;
@property (readonly, nonatomic) int currentSpineIndex;
@property (readonly, retain, nonatomic) EpubSpineItem *currentSpineItem;
@property (readonly, nonatomic) NSIndexPath * currentTOCIndexPath;
@property (readonly, retain, nonatomic) EpubTOCItem *currentTOCItem;
@property (readonly, assign, nonatomic) int pageCountForCurrentSpine;
@property (readonly, assign, nonatomic) int currentPageIndex;
@property (assign, nonatomic) BOOL needToRecalculatingPage;
@property (assign, nonatomic) int totalPageCount; //Pagecount for all epub
@property (assign, nonatomic) BOOL isCalculatingPage;
@property (assign, nonatomic) BOOL isStopCaculatingPage;

//Public Functions
-(void)unzipEpub;
-(void)gotoLeftPage;
-(void)gotoRightPage;
-(void)gotoSpineIndex:(int)spineIndex atAnchor:(NSString *)anchor;
-(void)gotoSpineIndex:(int)spineIndex atPageRatio:(float)pageRatio;
-(void)jumpToPageIndex:(int)pageIndex isCurrentSpine:(BOOL)isCurrentSpine;
-(void)updatePageCountForCurrentSpine;
-(void)gotoTOCIndex:(NSIndexPath *)tocIndex;
-(int)searchPageIndexForAnchor:(NSString *)anchor;
-(NSString *)renderHTML:(NSString *)html;
-(void)calculatePageForAllSpine;
-(void)stopCaculatingPage;
-(void)updateTOCIndexForCurrentPage;
-(void)copyPageNumberFromSpineArray:(NSArray *)spineArray;
-(void)copyPageNumberFromTOCArray:(NSArray *)tocArray;
@end
