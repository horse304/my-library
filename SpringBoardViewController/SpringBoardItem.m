//
//  SpringBoardItem.m
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "SpringBoardItem.h"

@interface SpringBoardItem ()
@property (retain, nonatomic) NSString *itemTitle;
@property (retain, nonatomic) NSString *itemIconPath;

@end

@implementation SpringBoardItem
@synthesize             itemImageView = _itemImageView;
@synthesize            itemTitleLabel = _itemTitleLabel;
@synthesize                 itemTitle = _itemTitle;
@synthesize              itemIconPath = _itemIconPath;
@synthesize                  delegate = _delegate;
@synthesize              objectOfItem = _objectOfItem;

- (id)initWithTitle:(NSString *)title iconPath:(NSString *)iconPath{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super initWithNibName:@"SpringBoardItem" bundle:nil];
    if (self) {
        self.itemTitle = title;
        self.itemIconPath = iconPath;
    }
    
    return self;
}

#pragma mark - View Life Cycle
-(void)loadView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super loadView];
}

-(void)viewWillAppear:(BOOL)animated{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
}
- (void)viewDidLoad
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewDidLoad];
#ifdef DEBUGX
    NSLog(@"iconPath:%@",self.itemIconPath);
#endif
    self.itemImageView.image = [UIImage imageWithContentsOfFile:self.itemIconPath];
    self.itemTitleLabel.text = self.itemTitle;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self setItemImageView:nil];
    [self setItemTitleLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Rotation Handling

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - UIEvent Handling
- (IBAction)tapOnSpringItem:(id)sender {
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if ([self.delegate respondsToSelector:@selector(springBoardItemDidSelected:)]) {
        [self.delegate springBoardItemDidSelected:self];
    }
}
@end
