//
//  EpubReader_iPhone.h
//  nexto
//
//  Created by Dato - Techmaster.vn on 7/27/12.
//  Copyright (c) 2012 Dato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"
#import "Downloader.h"
#import "DatoEpubVC.h"
#import "ItemDefinitions.h"
#import "Settings.h"
#import "Library.h"
#import "SettingsPopover_iPhone.h"
#import "ChaptersListPopover_iPhone.h"
#import "UIWebViewAdditions.h"
#import "EpubImageZooming.h"
#import "AppDelegate_iPhone.h"
#import "EpubShareFacebook.h"


//Dato @ Techmaster.vn
@interface EpubReader_iPhone : DatoEpubVC <UIGestureRecognizerDelegate, DatoEpubVCDelegate, SettingsPopoverDelegate_iPhone, ChaptersListDelegate_iPhone>

- (id)initWithSetBook:(Book *)bookToOpen splash:(bool)isSplash;
- (id)initWithDownloadBook:(NSMutableDictionary *)item;

@property (nonatomic, retain) Book *book;

@end