//
//  TOCItem.h
//  Reader
//
//  Created by Techmaster on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TOCItem : NSObject
@property (retain, nonatomic) NSString *title;
@property (retain, nonatomic) TOCItem *parent;
@property (retain, nonatomic) NSMutableArray *childs;
@end
