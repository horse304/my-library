//
//  GOLModel.m
//  imagines
//
//  Created by Jake Gundersen on 10/13/11.
//  Copyright (c) 2011 University of Utah Hospitals and Clinics. All rights reserved.
//

#import "GOLModel.h"

@implementation GOLModel 

@synthesize width, height;
@synthesize cells;

-(id)initGameWidth:(NSUInteger)gWidth andHeight:(NSUInteger)gHeight {
    if (([super init] == self)) {
        self.height = gHeight;
        self.width = gWidth;
        
        cells = [NSMutableArray array];
        
        for (int i = 0; i < gWidth * gHeight; i++) {
            [cells addObject:[NSNumber numberWithBool:NO]];
        }
    }
    return self;
}

-(void)update {
    NSMutableArray *newStates = [NSMutableArray array];
    
    for (int i = 0; i < width * height; i++) {
        int x = i % width;
        int y = (int)(i / width);
        
        int neighbors = [self findNeighboursForCellAtColumn:x andRow:y];
        
        BOOL ALIVE = [self aliveForCellX:x andY:y];
        
        BOOL NEWSTATE;
        
        if (ALIVE) {
            if (neighbors > 3 || neighbors < 2) {
                NEWSTATE = NO;
            } else {
                NEWSTATE = YES;
            }
        } else if (neighbors == 3) {
            NEWSTATE = YES;
        } else {
            NEWSTATE = NO;
        }
        [newStates addObject:[NSNumber numberWithBool:NEWSTATE]];
    }
    cells = newStates;
}

- (int)findNeighboursForCellAtColumn:(NSUInteger)col andRow:(NSUInteger)row
{
    int total = 0;
    
    if ([self aliveForCellX:(col - 1) andY:(row + 1)]) {
        total++;
    }
    if ([self aliveForCellX:col andY:(row + 1)]) {
        total++;
    }
    if ([self aliveForCellX:(col + 1) andY:(row + 1)]) {
        total++;
    }
    
    
    if ([self aliveForCellX:(col - 1) andY:(row - 1)]) {
        total++;
    }
    if ([self aliveForCellX:col andY:(row - 1)]) {
        total++;
    }
    if ([self aliveForCellX:(col + 1) andY:(row - 1)]) {
        total++;
    }
    
    
    if ([self aliveForCellX:(col - 1) andY:row]) {
        total++;
    }
    if ([self aliveForCellX:(col + 1) andY:row]) {
        total++;
    }
    
    return total;
}

-(BOOL)aliveForCellX:(int)x andY:(int)y {
    if (x < 0 || x >= width || y < 0 || y >= height) {
        return NO;
    }
    
    NSUInteger indx = (y * width) + x;
    return [[cells objectAtIndex:indx] boolValue];
}

-(void)toggleCellX:(NSUInteger)x andY:(NSUInteger)y {
    NSUInteger indx = (y * width) + x;
    BOOL SELECT = ![[cells objectAtIndex:indx] boolValue];
    [cells replaceObjectAtIndex:indx withObject:[NSNumber numberWithBool:SELECT]];
}

-(void)randomPopulate {
    for (int i = 0; i < width * height; i++) {
        BOOL ACTCELL = (arc4random() % 3 > 1);
        [cells replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:ACTCELL]];
    }
}

-(void)spawnWalkerAtCellX:(int)x andY:(int)y {
    if (x < 1 || x > width || y < 1 || y > height) {
        return;
    } else {
        [self toggleCellX:x - 1 andY:y - 1];
        [self toggleCellX:x + 1 andY:y];
        [self toggleCellX:x + 1 andY:y - 1];
        [self toggleCellX:x andY:y + 1];
        [self toggleCellX:x andY:y];
    }
}

@end
