//
//  QuizAPI.h
//  QuizEnzine
//
//  Created by Techmaster on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFJSONRequestOperation.h"
#import "QuizAPIDelegate.h"
#import "QE_UserInfo.h"

@interface QuizAPI : NSObject

@property (strong, nonatomic) AFHTTPClient *afClient;
@property (retain, nonatomic) NSString *quizAccessToken;
@property (retain, nonatomic) NSDate *quizAccessTokenExpirationDate;
@property (retain, nonatomic) NSString *quizUserId;

+(QuizAPI *)sharedQuizAPI;
-(BOOL)isSessionValid;
-(void)requestLoginWithFacebook:(QE_UserInfo *)fbinfo delegate:(id<QuizAPIDelegate>)delegate;
@end
