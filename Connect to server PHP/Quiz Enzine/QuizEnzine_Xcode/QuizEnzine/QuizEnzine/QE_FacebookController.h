//
//  QE_FacebookController.h
//  QuizEnzine
//
//  Created by Techmaster on 6/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import "QuizAPI.h"
#import "QE_FacebookControllerDelegate.h"
#import "QE_UserInfo.h"

enum QE_FacebookControllerRequestType{
    RequestUserInfo = 1
};

@interface QE_FacebookController : NSObject <FBSessionDelegate,FBRequestDelegate, QuizAPIDelegate>

@property (weak, nonatomic) id<QE_FacebookControllerDelegate> delegate;
@property (weak, nonatomic) QuizAPI *quizAPI;

-(id)initWithDelegate:(id)delegate;
-(void)login;

-(BOOL)handlerURL:(NSURL*)url;

@end
