//
//  JSItem.m
//  nexto
//
//  Created by Techmaster on 7/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "JSItem.h"

@implementation JSItem
@synthesize                        jsString = _jsString;
@synthesize                 replacementBlock = _replacementBlock;
-(id)initWithJSString:(NSString*)jsString 
      replacementBlock:(JSReplacementBlock)block
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {        
        self.jsString = jsString;
        self.replacementBlock = block;
    }
    return self;
}

-(NSString *)makeReplacement{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.replacementBlock != nil) {
        NSMutableDictionary *replacedDic = self.replacementBlock();
        if (replacedDic != nil) {
            NSMutableString *jsStringReplaced = [self.jsString mutableCopy];
            for (NSString *key in [replacedDic allKeys]) {
                id value = [replacedDic valueForKey:key];
#ifdef DEBUGX
                NSLog(@"key %@ and value:%@",key,value);
#endif
                if ([value isKindOfClass:[NSString class]]) {
                    NSRange rangeReplace = NSMakeRange(0, jsStringReplaced.length);
                    [jsStringReplaced replaceOccurrencesOfString:key 
                                                       withString:value
                                                          options:NSLiteralSearch 
                                                            range:rangeReplace];
                }
            }
            return jsStringReplaced;
        }
    }
    if (self.jsString != nil) {        
        return self.jsString;
    }else {
        return @"";
    }
}
@end
