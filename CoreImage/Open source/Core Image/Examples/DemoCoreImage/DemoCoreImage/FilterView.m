//
//  FilterView.m
//  DemoCoreImage
//
//  Created by Techmaster on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FilterView.h"

@implementation FilterView
@synthesize delegate = _delegate;
@synthesize ciFilter = _ciFilter;
@synthesize ciContext = _ciContext;
- (id)initWithFilter:(CIFilter *)filter context:(CIContext *)context thumbnail:(UIImage *)thumbnailImg frame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        if (filter != nil) {
            self.ciFilter = filter;
            self.ciContext = context;
            UIImageView *thumbnailView = [[UIImageView alloc] initWithFrame:self.bounds];
            thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
            CIImage *ciOutputImg = [self.ciFilter outputImage];
            CGImageRef cgOutputImg = [self.ciContext createCGImage:ciOutputImg fromRect:ciOutputImg.extent];
            thumbnailView.image = [UIImage imageWithCGImage:cgOutputImg];
            [self addSubview:thumbnailView];
        }   else {
            UIImageView *thumbnailView = [[UIImageView alloc] initWithFrame:self.bounds];
            thumbnailView.contentMode = UIViewContentModeScaleAspectFill;
            thumbnailView.image = thumbnailImg;
            [self addSubview:thumbnailView];
        }
        UITapGestureRecognizer *tabGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tabOnView)];
        tabGesture.numberOfTapsRequired = 1;
        [self addGestureRecognizer:tabGesture];
    }
    return self;
}

-(void)tabOnView{
    if ([self.delegate respondsToSelector:@selector(filterViewDidTap:)]) {
        [self.delegate filterViewDidTap:self];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
