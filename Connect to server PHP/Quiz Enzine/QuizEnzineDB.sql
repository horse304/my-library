-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2012 at 05:03 AM
-- Server version: 5.5.25-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `QuizEnzineDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `qe_accesstoken`
--

CREATE TABLE IF NOT EXISTS `qe_accesstoken` (
  `accesstoken_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expire_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `userid` mediumint(8) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`accesstoken_id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `qe_accesstoken`
--

INSERT INTO `qe_accesstoken` (`accesstoken_id`, `start_date`, `expire_date`, `userid`, `status`) VALUES
(1, '2012-06-18 03:45:20', '2012-06-20 17:00:00', 1, 0),
(2, '2012-06-17 21:24:06', '2012-06-24 21:24:06', 1, 0),
(3, '2012-06-17 21:25:42', '2012-06-24 21:25:42', 1, 0),
(4, '2012-06-17 21:26:42', '2012-06-24 21:26:42', 1, 0),
(5, '2012-06-17 21:27:34', '2012-06-24 21:27:34', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `qe_picture`
--

CREATE TABLE IF NOT EXISTS `qe_picture` (
  `pic_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pic_original_link` varchar(254) NOT NULL,
  `pic_thumbnail_link` varchar(254) NOT NULL,
  `userid` mediumint(8) unsigned DEFAULT NULL,
  `pic_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pic_id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `qe_user`
--

CREATE TABLE IF NOT EXISTS `qe_user` (
  `userid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'User id',
  `user_fullname` varchar(30) CHARACTER SET utf8 DEFAULT 'Unknown' COMMENT 'User full name',
  `user_email` varchar(254) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL COMMENT 'User email address',
  `user_password` char(32) DEFAULT NULL COMMENT 'User MD5 password',
  `user_pic` mediumint(8) unsigned DEFAULT NULL,
  `user_fbid` varchar(254) DEFAULT NULL COMMENT 'User Facebook Id',
  `user_join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'User Join Date',
  `user_status` int(11) NOT NULL DEFAULT '1' COMMENT 'User Status',
  PRIMARY KEY (`userid`),
  KEY `user_pic` (`user_pic`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `qe_user`
--

INSERT INTO `qe_user` (`userid`, `user_fullname`, `user_email`, `user_password`, `user_pic`, `user_fbid`, `user_join_date`, `user_status`) VALUES
(1, 'Dat Nguyen', 'horseuvn@gmail.com', '123456', NULL, NULL, '2012-06-17 17:00:00', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `qe_accesstoken`
--
ALTER TABLE `qe_accesstoken`
  ADD CONSTRAINT `qe_accesstoken_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `qe_user` (`userid`) ON DELETE SET NULL;

--
-- Constraints for table `qe_picture`
--
ALTER TABLE `qe_picture`
  ADD CONSTRAINT `qe_picture_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `qe_user` (`user_pic`) ON DELETE SET NULL;

--
-- Constraints for table `qe_user`
--
ALTER TABLE `qe_user`
  ADD CONSTRAINT `qe_user_ibfk_1` FOREIGN KEY (`user_pic`) REFERENCES `qe_picture` (`pic_id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
