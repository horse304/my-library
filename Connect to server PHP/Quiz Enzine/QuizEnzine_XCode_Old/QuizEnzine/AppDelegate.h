//
//  AppDelegate.h
//  QuizEnzine
//
//  Created by Dato on 6/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizAPI.h"
#import "VCSplash.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) VCSplash *vcSplash;
@property (strong, nonatomic) QuizAPI *quizAPI;
@end
