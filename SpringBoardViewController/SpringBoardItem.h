//
//  SpringBoardItem.h
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpringBoardItemDelegate.h"

@interface SpringBoardItem : UIViewController <UIGestureRecognizerDelegate>
-(id)initWithTitle:(NSString *)title iconPath:(NSString *)iconPath;
@property (strong, nonatomic) IBOutlet UIImageView *itemImageView;
@property (strong, nonatomic) IBOutlet UILabel *itemTitleLabel;
@property (unsafe_unretained, nonatomic) id<SpringBoardItemDelegate> delegate;

@property (retain, nonatomic) NSObject *objectOfItem;
@end
