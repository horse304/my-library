//
//  TOCItem.m
//  Reader
//
//  Created by Techmaster on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TOCItem.h"

@implementation TOCItem
@synthesize title = _title;
@synthesize childs = _childs;
@synthesize parent = _parent;
-(id)init{
    self = [super init];
    if (self) {
        _childs = [[NSMutableArray alloc] init];
    }
    return self;
}
-(void)dealloc{
    self.childs = nil;
    self.parent = nil;
    [super dealloc];
}
@end
