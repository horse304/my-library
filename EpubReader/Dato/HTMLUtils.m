//
//  HTMLUtils.m
//  nexto
//
//  Created by Techmaster on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define kReplace_Head_Part @"<!-- HTMLUTILS_HEAD_PART -->"
#define kReplace_Body_Part @"<!-- HTMLUTILS_BODY_PART -->"
#import "HTMLUtils.h"
@interface HTMLUtils()
@property (retain, nonatomic) NSString *orginalHTML;
@end

@implementation HTMLUtils
@synthesize              orginalHTML = _orginalHTML;
@synthesize                     headPart = _headPart;
@synthesize                     bodyPart = _bodyPart;
@synthesize                     htmlPart = _htmlPart;
#pragma mark - Construct
-(id)initWithHTMLString:(NSString *)html{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        _orginalHTML = html;
        _headPart = [self takeHeadPart];
        _bodyPart = [self takeBodyPart];
        _htmlPart = [self takeHTMLPart];
    }
    return self;
}

#pragma mark - Parsing html to multi parts
-(NSString *)takeHeadPart{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int startHeadIndex = [self.orginalHTML rangeOfString:@"<head"].location;
    NSRange rangeOfEndHead = [self.orginalHTML rangeOfString:@"</head>"];
    int endHeadIndex = rangeOfEndHead.location == NSNotFound ? NSNotFound : (rangeOfEndHead.location + rangeOfEndHead.length);
#ifdef DEBUGX
    NSLog(@"startIndex:%d, endIndex:%d",startHeadIndex, endHeadIndex);
#endif
    
    //Check head exist and return head part
    if (startHeadIndex != NSNotFound && endHeadIndex != NSNotFound) {
        NSRange rangeOfHeadPart = NSMakeRange(startHeadIndex, endHeadIndex - startHeadIndex);
#ifdef DEBUGX
        NSLog(@"head part: %@",[self.orginalHTML substringWithRange:rangeOfHeadPart]);
#endif
        return [self.orginalHTML substringWithRange:rangeOfHeadPart];
    }else {
        return nil;
    }
}

-(NSString *)takeBodyPart{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int startBodyIndex = [self.orginalHTML rangeOfString:@"<body"].location;
    NSRange rangeOfEndBody = [self.orginalHTML rangeOfString:@"</body>"];
    int endBodyIndex = rangeOfEndBody.location == NSNotFound ? NSNotFound : (rangeOfEndBody.location + rangeOfEndBody.length);
#ifdef DEBUGX
    NSLog(@"startIndex:%d, endIndex:%d",startBodyIndex, endBodyIndex);
#endif
    
    //Check head exist and return head part
    if (startBodyIndex != NSNotFound && endBodyIndex != NSNotFound) {
        NSRange rangeOfBodyPart = NSMakeRange(startBodyIndex, endBodyIndex - startBodyIndex);
#ifdef DEBUGX
        NSLog(@"body part: %@",[self.orginalHTML substringWithRange:rangeOfBodyPart]);
#endif
        return [self.orginalHTML substringWithRange:rangeOfBodyPart];
    }else {
        return nil;
    }
}
-(NSString *)takeHTMLPart{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif    
    NSString *headPart = [self takeHeadPart];
    NSString *bodyPart = [self takeBodyPart];
    NSString *htmlPartReturn = [self.orginalHTML stringByReplacingOccurrencesOfString:headPart withString:kReplace_Head_Part];
    htmlPartReturn = [htmlPartReturn stringByReplacingOccurrencesOfString:bodyPart withString:kReplace_Body_Part];
#ifdef DEBUGX
    NSLog(@"html part:%@",htmlPartReturn);
#endif
    return htmlPartReturn;
}

#pragma mark - Public Utilites
-(void)addString:(NSString *)stringToAdd to:(HTMLPartType)partType{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (stringToAdd != nil) {
        switch (partType) {
            case HTMLPartTypeHead:{            
                if (self.headPart != nil) {
                    NSString *newEndHeadPart = [NSString stringWithFormat:@"%@</head>",stringToAdd];
                    self.headPart = [self.headPart stringByReplacingOccurrencesOfString:@"</head>" withString:newEndHeadPart];
                }
                break;
            }
            case HTMLPartTypeBody:{
                if (self.bodyPart != nil) {
                    NSString *newEndBodyPart = [NSString stringWithFormat:@"%@</body>",stringToAdd];
                    self.bodyPart = [self.bodyPart stringByReplacingOccurrencesOfString:@"</body>" withString:newEndBodyPart];
                }
                break;
            }
                
            default:
                break;
        }
    }
}
-(NSString *)getHTMLString{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSString *htmlReturn = [self.htmlPart stringByReplacingOccurrencesOfString:kReplace_Head_Part withString:self.headPart];
    htmlReturn = [htmlReturn stringByReplacingOccurrencesOfString:kReplace_Body_Part withString:self.bodyPart];
#ifdef DEBUGX
    NSLog(@"%@",htmlReturn);
#endif
    return htmlReturn;
}
@end
