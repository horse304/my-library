//
//  CSSItem.h
//  nexto
//
//  Created by Techmaster on 7/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSSItem : NSObject
typedef NSMutableDictionary *(^CSSReplacementBlock)();
@property (retain, nonatomic) NSString *cssString;
@property (copy, nonatomic) CSSReplacementBlock replacementBlock;

-(id)initWithCSSString:(NSString*)cssString 
      replacementBlock:(CSSReplacementBlock)block;
-(NSString *)makeReplacement;
@end
