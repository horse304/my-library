//
//  EpubShareFacebook.h
//  nexto
//
//  Created by Techmaster on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate_iPad.h"
#import "Facebook.h"

@interface EpubShareFacebook : UIViewController <FBSessionDelegate, FBRequestDelegate>
-(id)init;
-(void)share:(NSString *)textForSharing inBook:(Book*)book;
@end
