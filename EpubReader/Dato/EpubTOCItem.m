//
//  EpubChapter.m
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//

#import "EpubTOCItem.h"

#define TOCITEM_KEY_chapterTitle @"tocitem_key_chaptertitle"
#define TOCITEM_KEY_chapterURL @"tocitem_key_chapterurl"
#define TOCITEM_KEY_subChapters @"tocitem_key_subchapters"
#define TOCITEM_KEY_parent @"tocitem_key_parent"
#define TOCItem_Key_pageNumber_Portrait @"tocitem_key_pagenumber_portrait"
#define TOCItem_Key_pageNumber_Landscape @"tocitem_key_pagenumber_landscape"

@implementation EpubTOCItem
@synthesize chapterTitle = _chapterTitle;
@synthesize chapterURL = _chapterURL;
@synthesize subChapters = _subChapters;
@synthesize parent = _parent;

-(id)initWithChapterTitle:(NSString *)title chapterURL:(NSURL *)url{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        self.chapterTitle = title;
        self.chapterURL = url;
        _subChapters = [[NSMutableArray alloc] init];
    }
    return (self);
}

-(id)initWithCoder:(NSCoder *)aDecoder{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        self.chapterURL = [aDecoder decodeObjectForKey:TOCITEM_KEY_chapterURL];
        self.chapterTitle = [aDecoder decodeObjectForKey:TOCITEM_KEY_chapterTitle];
        self.subChapters = [aDecoder decodeObjectForKey:TOCITEM_KEY_subChapters];
        self.parent = [aDecoder decodeObjectForKey:TOCITEM_KEY_parent];
        pageNumber_Portrait = [aDecoder decodeIntForKey:TOCItem_Key_pageNumber_Portrait];
        pageNumber_Landscape = [aDecoder decodeIntForKey:TOCItem_Key_pageNumber_Landscape];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [aCoder encodeObject:self.chapterURL forKey:TOCITEM_KEY_chapterURL];
    [aCoder encodeObject:self.chapterTitle forKey:TOCITEM_KEY_chapterTitle];
    [aCoder encodeObject:self.subChapters forKey:TOCITEM_KEY_subChapters];
    [aCoder encodeObject:self.parent forKey:TOCITEM_KEY_parent];
    [aCoder encodeInt:pageNumber_Portrait forKey:TOCItem_Key_pageNumber_Portrait];
    [aCoder encodeInt:pageNumber_Landscape forKey:TOCItem_Key_pageNumber_Landscape];
}

static int depth = 0;
-(void)browseTOCTreeWithBlock:(browseTOCBlock)browseBlock{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    browseBlock(self, depth);
    for (EpubTOCItem *subItem in self.subChapters) {
        [self browseSubTOCTreeWithBlock:browseBlock forItem:subItem];
    }
}

-(void)browseSubTOCTreeWithBlock:(browseTOCBlock)browseBlock forItem:(EpubTOCItem *)currentTOCItem{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    depth ++;
    browseBlock(currentTOCItem,depth);
    for (EpubTOCItem *subItem in currentTOCItem.subChapters) {
        [self browseSubTOCTreeWithBlock:browseBlock forItem:subItem];
    }
    depth--;
}

#pragma mark - Encapsulation
-(void)setPageNumber:(int)pageNumber{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        pageNumber_Landscape = pageNumber;
    }else {
        pageNumber_Portrait = pageNumber;
    }
}

-(int)pageNumber{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        return pageNumber_Landscape;
    }else {
        return pageNumber_Portrait;
    }
}

-(void)copyPageNumberFrom:(EpubTOCItem *)tocItem{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    pageNumber_Portrait = tocItem->pageNumber_Portrait;
    pageNumber_Landscape = tocItem->pageNumber_Landscape;
}
@end
