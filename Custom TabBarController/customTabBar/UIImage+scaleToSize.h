//
//  UIImage+scaleToSize.h
//  customTabBar
//
//  Created by Techmaster on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//



@interface UIImage (scaleToSize)
- (UIImage *) scaledToSize:(CGSize)newSize;
@end
