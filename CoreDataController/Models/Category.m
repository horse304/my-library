//
//  Category.m
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "Category.h"
#import "Factitem.h"


@implementation Category

@dynamic category_id;
@dynamic category_name;
@dynamic language_code;
@dynamic category_image_path;
@synthesize full_category_image_path = _full_category_image_path;
@dynamic toFactitem;

-(NSString *)full_category_image_path{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    return [[[CoreDataController shared] getPathOfImageResources] stringByAppendingPathComponent:self.category_image_path];
}

@end
