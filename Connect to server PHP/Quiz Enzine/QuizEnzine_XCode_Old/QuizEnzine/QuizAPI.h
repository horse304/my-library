//
//  QuizAPI.h
//  QuizEnzine
//
//  Created by Techmaster on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFJSONRequestOperation.h"
#import "QuizAPIDelegate.h"

@interface QuizAPI : NSObject

@property (weak, nonatomic) id<QuizAPIDelegate> delegate;
@property (strong, nonatomic) AFHTTPClient *afClient;
@property (retain, nonatomic) NSString *quizAccessToken;
@property (retain, nonatomic) NSString *quizUserId;

-(id)initWithDelegate:(id)delegate;
-(BOOL)checkAccessToken;
@end
