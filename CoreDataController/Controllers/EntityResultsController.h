//
//  Stuff_FetchedResultsController.h
//  SexFunFacts
//
//  Created by Techmaster on 8/4/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "ModelDefine.h"
#import "LanguageUtil.h"

@interface EntityResultsController : NSFetchedResultsController <NSFetchedResultsControllerDelegate>

/**
	Init a FetchedResultsController for entity
    @param entityName The entity name
	@param sortKey The field that a default sort key of entity
	@param managedObjectContext The managed object context
	@returns an instance of EntityFetchedResultsController
 */
-(id)initWithEntityName:(NSString*) entityName sortKey:(NSString *)sortKey ascending:(BOOL)ascending managedObjectContext:(NSManagedObjectContext *)managedObjectContext;


/**
	Predicate for filter entity
 */
@property (retain, nonatomic) NSPredicate *entityPredicate;


/**
	@returns count of entity
 */
-(int)getCount;

/**
	Reset result controller with other languageCode
	@param languageCode Language Code
    @returns The result of performFetch after change languageCode.
 */
-(BOOL)updateFetchRequest;


@end
