//
//  EpubUtils.m
//  Dato
//
//  Created by Dato - horseuvn@gmail.com on 6/30/12.
//  Copyright (c) 2012 TechMaster.VN . All rights reserved.
//
//#define DEBUGX
#define kXMLParsingRootFile @"XML Parsing Root File"
#define kXMLParsing_TOC_NCX_Epub2 @"XML Parsing TOC NCX EPUB 2"
#define kXMLParsing_NAV_Epub3 @"XML Parsing NAV EPUB 3"
#import "EpubUtils.h"
@interface EpubUtils()
@property (strong, nonatomic) DatoXMLParser *datoXMLParser;

@property (retain, nonatomic) NSString *directoryPath;
@property (retain, nonatomic) NSString *epubRootDocumentPath;
@property (retain, nonatomic) NSString *epubTOCPath;

@property (strong, nonatomic) DatoXMLNode *package;
@property (strong, nonatomic) NSString *coverImagePath;
@end

@implementation EpubUtils
@synthesize                                 delegate = _delegate;
@synthesize                            directoryPath = _directoryPath;
@synthesize                                 tocArray = _tocArray;
@synthesize                               spineArray = _spineArray;
@synthesize                           coverImagePath = _coverImagePath;
@synthesize                                  package = _package;
@synthesize                            datoXMLParser = _datoXMLParser;
@synthesize                     epubRootDocumentPath = _epubRootDocumentPath;
@synthesize                              epubTOCPath = _epubTOCPath;

-(id)initWithDirectory:(NSString*)directoryPath withDelegate:(id<EpubUtilsDelegate>)delegate{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        self.directoryPath = directoryPath;
        self.delegate = delegate;
        _tocArray = [[NSMutableArray alloc] init ];
        _spineArray = [[NSMutableArray alloc] init];
    }
    return (self);
}
-(void)prepareEpub{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (![self.directoryPath isEqualToString:@""]) {
        NSString *rootFile = [self getRootFilePath];
        self.epubRootDocumentPath = [[rootFile stringByDeletingLastPathComponent] stringByAppendingString:@"/"];
        _datoXMLParser = [[DatoXMLParser alloc] initWithData:[NSData dataWithContentsOfFile:rootFile] delegate:self];
        self.datoXMLParser.identifier = kXMLParsingRootFile;
        [self.datoXMLParser start];
    }
}
#pragma mark - DatoXMLParserDelegate
-(void)datoXMLParserDidFinish:(id)sender xmlDocument:(DatoXMLNode *)xmlDOM{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if ([[sender identifier] isEqualToString:kXMLParsingRootFile]) {
        self.package = xmlDOM;
        [self parsingPackageDocument];
    }
    else if([[sender identifier] isEqualToString:kXMLParsing_TOC_NCX_Epub2]){
        [self parsingTOCEpub2:xmlDOM];
    }else if ([[sender identifier] isEqualToString:kXMLParsing_NAV_Epub3]) {
        [self parsingNAVEpub3:xmlDOM];
    }
}
#pragma mark - Parsing TOC
-(void)parsingPackageDocument{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (self.package != nil) {
        //Check version of epub
        int epubVersion = (int)floor([[self.package.attributes objectForKey:@"version"] floatValue]);
        switch (epubVersion) {
            case 2:
                [self parsingPackageDocumentEpub2];
                break;
            case 3:
                [self parsingPackageDocumentEpub3];
                break;
                
            default:
#ifdef DEBUGX
                NSLog(@"Does not support this Epub version");
#endif
                break;
        }
    }
}
-(void)parsingPackageDocumentEpub2{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Get cover image path
    [self getCoverImageEpub2];
    
    NSArray *xmlSpineArray = [self.package getElementsByTagName:@"spine"];
    if (xmlSpineArray != nil) {        
        DatoXMLNode *xmlSpine = [xmlSpineArray objectAtIndex:0];
        //Get spine array
        NSArray *xmlItemrefArray = [xmlSpine getElementsByTagName:@"itemref"];
        for (DatoXMLNode *xmlItemref in xmlItemrefArray) {
            NSString *spineItemID = [xmlItemref.attributes valueForKey:@"idref"];
            BOOL spineItemLinear = YES;
            if ([xmlItemref.attributes valueForKey:@"linear"] != nil) {                
                spineItemLinear = [[xmlItemref.attributes valueForKey:@"linear"] isEqualToString:@"yes"]?YES:NO;
            }
            NSArray *xmlManifestArray = [self.package getElementsByTagName:@"manifest"];
            if (xmlManifestArray != nil) {            
                DatoXMLNode *xmlManifest = [xmlManifestArray objectAtIndex:0];
                NSArray *xmlManifestItemArray = [xmlManifest getElementsByValue:spineItemID OfAttribute:@"id"];
                //NSLog(@"%d",tocArray.count);
                if (xmlManifestItemArray != nil) {                
                    DatoXMLNode *spineItemNode = [xmlManifestItemArray objectAtIndex:0];
                    NSString *spineItemPath = [self.epubRootDocumentPath stringByAppendingPathComponent:[spineItemNode.attributes valueForKey:@"href"]];
                    spineItemPath = [NSString stringWithFormat:@"file://%@",spineItemPath];
                    
                    //Create spine item and add to array
                    EpubSpineItem *spineItem = [[EpubSpineItem alloc] init];
                    spineItem.spineURL = [NSURL URLWithString:spineItemPath];
                    spineItem.isLinear = spineItemLinear;
                    [self.spineArray addObject:spineItem];
                    
                }else {
#ifdef DEBUGX
                    NSLog(@"Error get toc NCX epub 2");
#endif
                }
            }else {
#ifdef DEBUGX
                NSLog(@"Error parsing manifest of Epub 2");
#endif
            }
            
        }
        
        //Get toc file path
        NSString *tocID = [xmlSpine.attributes valueForKey:@"toc"];
        NSArray *xmlManifestArray = [self.package getElementsByTagName:@"manifest"];
        if (xmlManifestArray != nil) {            
            DatoXMLNode *manifest = [xmlManifestArray objectAtIndex:0];
            NSArray *tocArray = [manifest getElementsByValue:tocID OfAttribute:@"id"];
            //NSLog(@"%d",tocArray.count);
            if (tocArray != nil) {                
                DatoXMLNode *tocItem = [tocArray objectAtIndex:0];
                _epubTOCPath = [self.epubRootDocumentPath stringByAppendingPathComponent:[tocItem.attributes valueForKey:@"href"]];
                self.datoXMLParser.data = [NSData dataWithContentsOfFile:self.epubTOCPath];
                if (self.datoXMLParser.data != nil && self.datoXMLParser.data.length > 0) {                    
                    self.datoXMLParser.identifier = kXMLParsing_TOC_NCX_Epub2;
                    [self.datoXMLParser start];
                }
            }else {
#ifdef DEBUGX
                NSLog(@"Error get toc NCX epub 2");
#endif
            }
        }else {
#ifdef DEBUGX
            NSLog(@"Error parsing manifest of Epub 2");
#endif
        }
    }else {
#ifdef DEBUGX
        NSLog(@"Error parsing spine of Epub 2");
#endif
    }
}

-(void)getCoverImageEpub2{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    DatoXMLNode *xmlMetadata = [[self.package getElementsByTagName:@"metadata"] objectAtIndex:0];
    NSArray *xmlMetaArrayOfMetadata = [xmlMetadata getElementsByTagName:@"meta"];
    for (DatoXMLNode *xmlMetaItem in xmlMetaArrayOfMetadata) {
        if ([[xmlMetaItem.attributes valueForKey:@"name"] isEqualToString:@"cover"]) {
            NSString *coverManifestId = [xmlMetaItem.attributes valueForKey:@"content"];
            
            NSArray *xmlManifestArray = [self.package getElementsByTagName:@"manifest"];
            if (xmlManifestArray != nil) {            
                DatoXMLNode *xmlManifest = [xmlManifestArray objectAtIndex:0];
                NSArray *xmlManifestItemArray = [xmlManifest getElementsByValue:coverManifestId OfAttribute:@"id"];
                if (xmlManifestItemArray != nil) {                
                    DatoXMLNode *xmlCoverManifestItem = [xmlManifestItemArray objectAtIndex:0];
                    NSString *coverItemPath = [self.epubRootDocumentPath stringByAppendingPathComponent:[xmlCoverManifestItem.attributes valueForKey:@"href"]];
                    coverItemPath = [NSString stringWithFormat:@"file://%@",coverItemPath];
#ifdef DEBUGX
                    NSLog(@"cover image path:%@",coverItemPath);
#endif
                    self.coverImagePath = coverItemPath;                    
                }else {
                    NSLog(@"Error get cover image epub 2");
                }
            }else {
                NSLog(@"Error parsing manifest of Epub 2");
            }
        }
    }
}

-(void)parsingTOCEpub2:(DatoXMLNode *)rootnodeNCX{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    DatoXMLNode *navMap = [[rootnodeNCX getElementsByTagName:@"navMap"] objectAtIndex:0];
    NSArray *navPointArray = [navMap getElementsByTagName:@"navPoint"];
    for (DatoXMLNode *navPointItem in navPointArray) {
        [self parsingNavPoint:navPointItem parentChapter:nil];
    }
    
    //Finish get TOC for Epub 2
    [self finishPrepare];
}
-(void)parsingNavPoint:(DatoXMLNode *)navPoint parentChapter:(EpubTOCItem *)parentChapter{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (parentChapter == nil) {
        DatoXMLNode *navContent = [[navPoint getElementsByTagName:@"content"] objectAtIndex:0];
        if (navContent != nil) {
            DatoXMLNode *navLabel = [[navPoint getElementsByTagName:@"navLabel"] objectAtIndex:0];
            DatoXMLNode *navText = [[navLabel getElementsByTagName:@"text"] objectAtIndex:0];
            
            //Save to chapter Array
            NSString *tocDirectory = [self.epubTOCPath stringByDeletingLastPathComponent];
            NSString *chapterPath=[tocDirectory stringByAppendingPathComponent:[navContent.attributes valueForKey:@"src"]];
            EpubTOCItem *rootChapter = [[EpubTOCItem alloc] initWithChapterTitle:navText.nodeValue chapterURL:[NSURL URLWithString:chapterPath]];
            [self.tocArray addObject:rootChapter];
            //Search subchapter
            NSArray *subNavPointArray = [navPoint getElementsByTagName:@"navPoint"];
            if (subNavPointArray.count>0) {
                for (DatoXMLNode *subNavPoint in subNavPointArray) {
                    [self parsingNavPoint:subNavPoint parentChapter:rootChapter];
                }
            }
        }
    }else {
        DatoXMLNode *navContent = [[navPoint getElementsByTagName:@"content"] objectAtIndex:0];
        if (navContent != nil) {
            DatoXMLNode *navLabel = [[navPoint getElementsByTagName:@"navLabel"] objectAtIndex:0];
            DatoXMLNode *navText = [[navLabel getElementsByTagName:@"text"] objectAtIndex:0];
            
            NSString *tocDirectory = [self.epubTOCPath stringByDeletingLastPathComponent];
            NSString *chapterPath=[tocDirectory stringByAppendingPathComponent:[navContent.attributes valueForKey:@"src"]];
            EpubTOCItem *newChapter = [[EpubTOCItem alloc] initWithChapterTitle:navText.nodeValue chapterURL:[NSURL URLWithString:chapterPath]];
            //NSLog(@"%@",newChapter.chapterURL);
            newChapter.parent = parentChapter;
            [parentChapter.subChapters addObject:newChapter];
            
            //Search subchapter
            NSArray *subNavPointArray = [navPoint getElementsByTagName:@"navPoint"];
            if (subNavPointArray.count>0) {
                for (DatoXMLNode *subNavPoint in subNavPointArray) {
                    [self parsingNavPoint:subNavPoint parentChapter:newChapter];
                }
            }
        }
    }
}

-(void)parsingPackageDocumentEpub3{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Get Cover image
    [self getCoverImageEpub3];
    
    //Get spine array
    [self getSpineArrayEpub3];
    
    //Get nav item in manifest and parsing
    [self getNavPath];
}
-(void)getCoverImageEpub3{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSArray *manifestArray = [self.package getElementsByTagName:@"manifest"];
    if (manifestArray != nil) {
        DatoXMLNode *manifest = [manifestArray objectAtIndex:0];
        NSArray *itemArray =  [manifest getElementsByValue:@"cover-image" OfAttribute:@"properties"];
        if (itemArray != nil) {
            DatoXMLNode *coverManifestItem = [itemArray objectAtIndex:0];
            NSString *coverFilePath = [self.epubRootDocumentPath stringByAppendingPathComponent:[coverManifestItem.attributes valueForKey:@"href"]];
            self.coverImagePath = [NSString stringWithFormat:@"file://%@",coverFilePath];
        }else {
            NSLog(@"Erro for getting cover item of Epub 3!");
        }
    }else {
        NSLog(@"Error for getting manifest of Epub 3!");
    }
}

-(void)getSpineArrayEpub3{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSArray *spineElementArray = [self.package getElementsByTagName:@"spine"];
    if (spineElementArray != nil) {        
        DatoXMLNode *spine = [spineElementArray objectAtIndex:0];
        //Get spine array
        NSArray *itemrefArray = [spine getElementsByTagName:@"itemref"];
        for (DatoXMLNode *itemref in itemrefArray) {
            NSString *spineItemID = [itemref.attributes valueForKey:@"idref"];
            BOOL spineItemLinear = YES;
            if ([itemref.attributes valueForKey:@"linear"] != nil) {                
                spineItemLinear = [[itemref.attributes valueForKey:@"linear"] isEqualToString:@"yes"]?YES:NO;
            }
            NSArray *manifestArray = [self.package getElementsByTagName:@"manifest"];
            if (manifestArray != nil) {            
                DatoXMLNode *manifest = [manifestArray objectAtIndex:0];
                NSArray *manifestItemArray = [manifest getElementsByValue:spineItemID OfAttribute:@"id"];
                //NSLog(@"%d",tocArray.count);
                if (manifestItemArray != nil) {                
                    DatoXMLNode *spineItemNode = [manifestItemArray objectAtIndex:0];
                    NSString *spineItemPath = [self.epubRootDocumentPath stringByAppendingPathComponent:[spineItemNode.attributes valueForKey:@"href"]];
                    spineItemPath = [NSString stringWithFormat:@"file://%@",spineItemPath];
                    
                    //Create spine item and add to array
                    EpubSpineItem *spineItem = [[EpubSpineItem alloc] init];
                    spineItem.spineURL = [NSURL URLWithString:spineItemPath];
                    spineItem.isLinear = spineItemLinear;
                    [self.spineArray addObject:spineItem];
                    
                }else {
                    NSLog(@"Error get toc NCX epub 2");
                }
            }else {
                NSLog(@"Error parsing manifest of Epub 2");
            }
            
        }
#ifdef DEBUGX
        for (EpubSpineItem *spineItem in self.spineArray) {
            NSLog(@"spine Item: %@",spineItem.spineURL.absoluteString);
        }
#endif
    }else {
        NSLog(@"Error parsing spine of Epub 2");
    }
}
-(void)getNavPath{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSArray *manifestArray = [self.package getElementsByTagName:@"manifest"];
    if (manifestArray != nil) {
        DatoXMLNode *manifest = [manifestArray objectAtIndex:0];
        NSArray *itemArray =  [manifest getElementsByValue:@"nav" OfAttribute:@"properties"];
        if (itemArray != nil) {
            DatoXMLNode *navManifestItem = [itemArray objectAtIndex:0];
            _epubTOCPath = [self.epubRootDocumentPath stringByAppendingPathComponent:[navManifestItem.attributes valueForKey:@"href"]];
            self.datoXMLParser.data = [NSData dataWithContentsOfFile:self.epubTOCPath];
            self.datoXMLParser.identifier = kXMLParsing_NAV_Epub3;
            [self.datoXMLParser start];
        }else {
            NSLog(@"Erro for getting nav item of Epub 3!");
        }
    }else {
        NSLog(@"Error for getting manifest of Epub 3!");
    }
}
-(void)parsingNAVEpub3:(DatoXMLNode *)rootNAV{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (rootNAV != nil) {
        DatoXMLNode *body = [[rootNAV getElementsByTagName:@"body"] objectAtIndex:0];
        NSArray *navArray = [body getElementsByTagName:@"nav"];
        DatoXMLNode *navTOC = nil;
        for (DatoXMLNode *navElement in navArray) {
            if ([[navElement.attributes valueForKey:@"epub:type"] isEqualToString:@"toc"]) {
                navTOC = navElement;
                break;
            }
        }
        if (navTOC != nil) {
            DatoXMLNode *orderlist = [[navTOC getElementsByTagName:@"ol"] objectAtIndex:0];
            NSArray *liArray = [orderlist getElementsByTagName:@"li"];
            for (DatoXMLNode *liItem in liArray) {
                [self parsingListItem:liItem parentChapter:nil];
            }
        }      
    }
    [self finishPrepare];
}
-(void)parsingListItem:(DatoXMLNode *)liItem parentChapter:(EpubTOCItem *)parent{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (parent == nil) {
        //Root chapter
        DatoXMLNode *aElement = [[liItem getElementsByTagName:@"a"] objectAtIndex:0];
        NSString *chapterTitle = aElement.nodeValue;
        
        NSString *navDirectory = [self.epubTOCPath stringByDeletingLastPathComponent];
        NSString *chapterPath = [navDirectory stringByAppendingPathComponent:[aElement.attributes valueForKey:@"href"]];
        EpubTOCItem *rootChapter = [[EpubTOCItem alloc] initWithChapterTitle:chapterTitle chapterURL:[NSURL URLWithString:chapterPath]];
        [self.tocArray addObject:rootChapter];
        //Check subChapter
        NSArray *suborderlistArray  = [liItem getElementsByTagName:@"ol"];
        if (suborderlistArray != nil && suborderlistArray.count > 0) {
            NSArray *subliArray = [[suborderlistArray objectAtIndex:0] getElementsByTagName:@"li"];
            for (DatoXMLNode *subliItem in subliArray) {
                [self parsingListItem:subliItem parentChapter:rootChapter];
            }
        }
    }else {
        //Sub chapter
        DatoXMLNode *aElement = [[liItem getElementsByTagName:@"a"] objectAtIndex:0];
        NSString *chapterTitle = aElement.nodeValue;
        NSString *navDirectory = [self.epubTOCPath stringByDeletingLastPathComponent];
        NSString *chapterPath = [navDirectory stringByAppendingPathComponent:[aElement.attributes valueForKey:@"href"]];
        EpubTOCItem *newChapter = [[EpubTOCItem alloc] initWithChapterTitle:chapterTitle chapterURL:[NSURL URLWithString:chapterPath]];
        newChapter.parent = parent;
        [parent.subChapters addObject:newChapter];
        //Check subChapter
        NSArray *suborderlistArray  = [liItem getElementsByTagName:@"ol"];
        if (suborderlistArray != nil && suborderlistArray.count > 0) {
            NSArray *subliArray = [[suborderlistArray objectAtIndex:0] getElementsByTagName:@"li"];
            for (DatoXMLNode *subliItem in subliArray) {
                [self parsingListItem:subliItem parentChapter:newChapter];
            }
        }
    }
}

-(void)finishPrepare{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if ([self.delegate respondsToSelector:@selector(epubUtilsDidFinishPrepare:)]) {
        [self.delegate epubUtilsDidFinishPrepare:self];
    }
}

#pragma mark - Utilities
-(NSString *)getRootFilePath{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSString *containerPath = [self.directoryPath stringByAppendingPathComponent:@"META-INF/container.xml"];
    //Read fullpath
    TBXML *tbxml = [TBXML tbxmlWithURL:[NSURL fileURLWithPath:containerPath]];
    TBXMLElement *rootXML = tbxml.rootXMLElement;
    if(rootXML)
    {
        TBXMLElement *rootfilesXML = [TBXML childElementNamed:@"rootfiles" parentElement:rootXML];
        
        if(rootfilesXML != nil)
        {
            TBXMLElement *rootfileXML = [TBXML childElementNamed:@"rootfile" parentElement:rootfilesXML];
            
            if(rootfileXML != nil)
            {
                NSString *rootfile = [TBXML valueOfAttributeNamed:@"full-path" forElement:rootfileXML];
                return [self.directoryPath stringByAppendingPathComponent:rootfile];
            }
        }
    }
    return nil;
}
+(BOOL)isAnchorLink:(NSString *)filePath{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if ([filePath rangeOfString:@"#"].location != NSNotFound) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}
-(NSIndexPath *)indexPathOfTOCItem:(EpubTOCItem *)tocItem{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSIndexPath *result = [[NSIndexPath alloc] init];
    while (tocItem.parent != nil) {
        if ([tocItem.parent.subChapters indexOfObject:tocItem] != NSNotFound) {            
            result = [result indexPathByAddingIndex:[tocItem.parent.subChapters indexOfObject:tocItem]];
            tocItem = tocItem.parent;
        }else {
            return nil;
        }
    }
    result = [result indexPathByAddingIndex:[self.tocArray indexOfObject:tocItem]];
#ifdef DEBUGX
    for (int i=0;i<result.length;i++) {
        NSLog(@"index path: %d",[result indexAtPosition:i]);
    }
#endif
    return result;
}
-(EpubTOCItem *)getTOCItemAtIndexPath:(NSIndexPath *)indexPath{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    EpubTOCItem *result = nil;
    for (int i=(indexPath.length-1); i>=0; i--) {
        if (i==(indexPath.length-1)) {
            int index = [indexPath indexAtPosition:i];
            if (index>=0 && index<self.tocArray.count) {                
                result = [self.tocArray objectAtIndex:index];
            }else {
                return nil;
            }            
        }else {
            int index = [indexPath indexAtPosition:i];
            if (index>=0 && index<result.subChapters.count) {                
                result = [result.subChapters objectAtIndex:index];
            }else {
                return nil;
            }
        }
    }
    return result;
}
@end
