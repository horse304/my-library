
function Nexto_ChangeBodyMargin(marginNumber){
    var obj = document.getElementsByTagName('body')[0];
    if(obj != null){
        obj.style.margin = '0px ' + marginNumber + 'px !important';
    }
}
function Nexto_ChangeBodyFontZoom(fontZoom){
    var obj = document.getElementsByTagName('body')[0];
    if(obj != null){
        obj.style.webkitTextSizeAdjust = fontZoom + '% !important';
    }
}

function Nexto_ChangeTextAlign(jsAlign){
    var obj = document.getElementsByTagName('p');
    for(var i=0;i<obj.length;i++){
        obj[i].style.textAlign = jsAlign + " !important";
    }
    MyApp_Make_Img_Center();
}

function Nexto_ChangeFontFamily(jsFontName){
    var obj = document.all;
    for(var i=0;i<obj.length;i++){
        obj[i].style.fontFamily = jsFontName + " !important";
    }
}

function MyAppGetHTMLElementsAtPoint(x,y) {
    var tags = "";
    var e;
    var offset = 0;
    while ((tags.search(",(A|IMG),") < 0) && (offset < 20)) {
        tags = ",";
        e = document.elementFromPoint(x,y+offset);
        while (e) {
            if (e.tagName) {
                tags += e.tagName + ',';
            }
            e = e.parentNode;
        }
        if (tags.search(",(A|IMG),") < 0) {
            e = document.elementFromPoint(x,y-offset);
            while (e) {
                if (e.tagName) {
                    tags += e.tagName + ',';
                }
                e = e.parentNode;
            }
        }
        
        offset++;
    }
    return tags;
}
function MyAppGetElementAtPoint(x,y) {
    var e;
    var offset = 0;
    while (!e && (offset < 20)) {
        e = document.elementFromPoint(x,y+offset);
        if (!e) {
            e = document.elementFromPoint(x,y-offset);
        }
        
        offset++;
    }
    return e;
}
function MyAppGet_PositionX_ElementAtPoint(x,y){
    var e = MyAppGetElementAtPoint(x,y);
    return e.x;
}
function MyAppGet_PositionY_ElementAtPoint(x,y){
    var e = MyAppGetElementAtPoint(x,y);
    return e.y;
}
function MyAppGetSelectedString() {
    var text = window.getSelection();
    return text.toString();
    
}

function MyAppGetLinkSRCAtPoint(x,y) {
    var tags = "";
    var e = "";
    var offset = 0;
    while ((tags.length == 0) && (offset < 20)) {
        e = document.elementFromPoint(x,y+offset);
        while (e) {
            if (e.src) {
                tags += e.src;
                break;
            }
            e = e.parentNode;
        }
        if (tags.length == 0) {
            e = document.elementFromPoint(x,y-offset);
            while (e) {
                if (e.src) {
                    tags += e.src;
                    break;
                }
                e = e.parentNode;
            }
        }
        offset++;
    }
    return tags;
}

function MyAppGetLinkHREFAtPoint(x,y) {
    var tags = "";
    var e = "";
    var offset = 0;
    while ((tags.length == 0) && (offset < 20)) {
        e = document.elementFromPoint(x,y+offset);
        while (e) {
            if (e.href) {
                tags += e.href;
                break;
            }
            e = e.parentNode;
        }
        if (tags.length == 0) {
            e = document.elementFromPoint(x,y-offset);
            while (e) {
                if (e.href) {
                    tags += e.href;
                    break;
                }
                e = e.parentNode;
            }
        }
        offset++;
    }
    return tags;
}

function MyAppGetPositionForAnchor_X(anchorname){
    var obj = document.getElementById(anchorname);
    var posX = obj.offsetLeft;
    while(obj.offsetParent){
        obj = obj.offsetParent;
        posX = posX + obj.offsetLeft;
    }
    return posX;
}
function MyAppGetPositionForAnchor_Y(anchorname){
    var obj = document.getElementById(anchorname);
    var posY = obj.offsetTop;
    while(obj.offsetParent){
        obj = obj.offsetParent;
        posY = posY + obj.offsetTop;
    }
    return posY;
}

function MyApp_Make_Img_Center(){
    var imgArray = document.getElementsByTagName("img");
    for(var i=0;i<imgArray.length;i++){
        if(imgArray[i].parentNode.tagName.toLowerCase() == "p"){
            imgArray[i].parentNode.style.textAlign = "center !important";
        }
    }
}