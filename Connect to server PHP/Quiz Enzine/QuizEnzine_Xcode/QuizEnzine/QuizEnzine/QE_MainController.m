//
//  QE_MainController.m
//  QuizEnzine
//
//  Created by Techmaster on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QE_MainController.h"

@implementation QE_MainController
@synthesize appUserDefault = _appUserDefault;
@synthesize fbController = _fbController;

+(QE_MainController *)sharedMainController{
    static QE_MainController *sharedSingleton;
    
    @synchronized(self)
    {
        if (!sharedSingleton)
            sharedSingleton = [[QE_MainController alloc] init];
        
        return sharedSingleton;
    }
}

-(QE_MainController *)init{
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - General functions
-(BOOL)isSessionValid{
    //Check expiration date
    if ([self.appUserDefault.expirationDate timeIntervalSinceNow]>0) {
        return YES;
    }
    else {
        return NO;
    }
}

#pragma mark - UserDefault
-(QE_AppUserDefault *)appUserDefault{
    if (_appUserDefault == nil) {
        _appUserDefault = [[QE_AppUserDefault alloc] init];
    }
    return _appUserDefault;
}

#pragma mark - Facebook
-(QE_FacebookController*)fbController{
    if (_fbController != nil) {
        return _fbController;
    }else {
        _fbController = [[QE_FacebookController alloc] initWithDelegate:self];
        return _fbController;
    }
}
-(BOOL)handleOpenURL:(NSURL *)url{
    return [self.fbController handlerURL:url];
}
-(void)startLoginFacebook{
    
}
@end
