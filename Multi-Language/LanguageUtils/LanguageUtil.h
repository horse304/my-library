//
//  LanguageUtil.h
//  LocalizationDemo2
//
//  Created by raycad sun on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#define NOTIFICATION_Language_Changed @"LanguageUtils_Changed_Language"

typedef enum {
    NONE = -1,
    ENGLISH = 0,
    VIETNAMESE = 1,
    LANGUAGE_CODE_COUNT  
} LanguageCode;

@interface LanguageUtil : NSObject {
    @private
    LanguageCode _currentLanguageCode;
    NSDictionary *_textResourceDictionary; 
    NSArray *_languageNameList;
}

// Declare the singleton
+ (LanguageUtil *)instance;

- (NSString *)getTextByKey:(NSString *)key;

- (LanguageCode)currentLanguageCode;
- (BOOL)setCurrentLanguageCode:(LanguageCode)newValue; // Returns YES if change successes, otherwise returns NO
- (NSString *)getLanguageNameByCode:(LanguageCode)languageCode;
- (NSString *)getLanguageNameByCurrentLanguageCode;

@property (nonatomic, readonly) NSArray *languageNameList;

- (void)initResource;

@end
