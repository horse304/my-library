//
//  LoadPlist.m
//  DemoCoreImage
//
//  Created by Dato on 6/2/12.
//  Copyright (c) 2012 TechMaster.vn. All rights reserved.
//
#define FILTER_PLIST @"Filter"
#import "DATCoreImage.h"

@implementation DATCoreImage
@synthesize cicontext = _cicontext;
@synthesize filterArray = _filterArray;

-(id)init{
    self = [super init];
    if (self) {
        self.cicontext = [CIContext contextWithOptions:nil];
        self.filterArray = [self getFiltersArray];
    }
    return self;
}

-(NSArray *)getFiltersArray{
    NSMutableArray *filtersArray = [[NSMutableArray alloc] init];
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:FILTER_PLIST withExtension:@"plist"];
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    NSArray *filterDataArray = [data objectForKey:@"Filters"];
    for (id i in filterDataArray) {
        CIFilter *filter = [CIFilter filterWithName:[i objectForKey:@"Name"]];
        for (id attribute in [i objectForKey:@"Attributes"]) {
            [filter setValue:[attribute objectForKey:@"DefaultValue"]
                                 forKey:[attribute objectForKey:@"AttributeName"]];
        }
        [filtersArray addObject:filter];
     }
    return [NSArray arrayWithArray:filtersArray];
}
//Get CGImageRef of filter output image
-(CGImageRef)outputCGImageOfFilter:(CIFilter *)filter{
    if (filter != nil) {        
        CIImage *ciimage = filter.outputImage;
        if (self.cicontext != nil) {            
            return [self.cicontext createCGImage:ciimage fromRect:ciimage.extent];
        }
    }
    
    return nil;
    
}
-(UIImage*)outputUIImageOfFilter:(CIFilter *)filter{
    if (filter != nil) {        
        CIImage *ciimage = filter.outputImage; 
        if (self.cicontext != nil) {              
            CGImageRef cgimage = [self.cicontext createCGImage:ciimage fromRect:ciimage.extent];
            return [UIImage imageWithCGImage:cgimage];
        }
    }
    
    return nil;
    
}
@end
