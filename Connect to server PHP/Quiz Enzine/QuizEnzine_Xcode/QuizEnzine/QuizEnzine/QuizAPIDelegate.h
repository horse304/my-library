//
//  QuizAPIDelegate.h
//  QuizEnzine
//
//  Created by Techmaster on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol QuizAPIDelegate <NSObject>
@optional
-(void)quizAPIResponseFacebookRequest:(NSDictionary *)response;
@end
