//
//  LoadPlist.m
//  DemoCoreImage
//
//  Created by Techmaster on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define FILTER_PLIST @"Filter"
#import "LoadPlist.h"

@implementation LoadPlist
+(NSArray *)getFiltersArray{
    NSMutableArray *filtersArray = [[NSMutableArray alloc] init];
    NSURL *plistURL = [[NSBundle mainBundle] URLForResource:FILTER_PLIST withExtension:@"plist"];
    NSDictionary *data = [NSDictionary dictionaryWithContentsOfURL:plistURL];
    NSArray *filterDataArray = [data objectForKey:@"Filters"];
    for (id i in filterDataArray) {
        CIFilter *filter = [CIFilter filterWithName:[i objectForKey:@"Name"]];
        for (id attribute in [i objectForKey:@"Attributes"]) {
            [filter setValue:[attribute objectForKey:@"DefaultValue"]
                                 forKey:[attribute objectForKey:@"AttributeName"]];
        }
        [filtersArray addObject:filter];
     }
    return [NSArray arrayWithArray:filtersArray];
}
@end
