//
//  PolaroidImageView.m
//  SexFunFacts
//
//  Created by Techmaster on 8/7/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "PolaroidImageView.h"
@interface PolaroidImageView()
@property (retain, nonatomic) UIImageView *innerImageView;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@end

@implementation PolaroidImageView
@synthesize                 innerImage = _innerImage;
@synthesize              polaroidStyle = _polaroidStyle;
@synthesize             innerImageView = _innerImageView;
@synthesize        backgroundImageView = _backgroundImageView;

-(id)initWithStyle:(PolaroidStyle)polaroidStyle frame:(CGRect)frame{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        self.frame = frame;
        [self configLayout];
    }
    return self;
}

-(void)configLayout{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    switch (self.polaroidStyle) {
        case Standard:
            self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
            self.backgroundImageView.image = [UIImage imageNamed:POLAROID_BACKGROUND_STANDARD];
            self.backgroundImageView.contentMode = UIViewContentModeScaleToFill;
            [self addSubview:self.backgroundImageView];
            
            self.innerImageView = [[UIImageView alloc] initWithFrame:CGRectMake((1.0/21.8)*self.frame.size.width, 15.0/261.0*self.frame.size.height, 200.0/218.0*self.frame.size.width, 200.0/261.0*self.frame.size.height)];
            self.innerImageView.clipsToBounds = YES;
            self.innerImageView.contentMode = UIViewContentModeScaleAspectFill;
            if (self.innerImage != nil) {
                self.innerImageView.image = self.innerImage;
            }
            [self addSubview:self.innerImageView];
            
            
            [self bringSubviewToFront:self.backgroundImageView];
            break;
            
        default:
            break;
    }
}

#pragma mark  - Encapsulation
-(void)setInnerImage:(UIImage *)innerImage{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    _innerImage = innerImage;
    if (self.innerImageView != nil) {
        self.innerImageView.image = innerImage;
    }
}

@end
