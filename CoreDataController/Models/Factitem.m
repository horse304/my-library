//
//  Factitem.m
//  SexFunFacts
//
//  Created by Techmaster on 8/4/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "Factitem.h"
#import "Category.h"


@implementation Factitem

@dynamic fact_content;
@dynamic fact_id;
@dynamic fact_image_path;
@synthesize full_fact_image_path = _full_fact_image_path;
@dynamic fact_viewcount;
@dynamic fact_createdDate;
@dynamic language_code;
@dynamic toCategory;

-(NSString *)full_fact_image_path{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    return [[[CoreDataController shared] getPathOfImageResources] stringByAppendingPathComponent:self.fact_image_path];
}
@end
