//
//  LanguageUtil.m
//  LocalizationDemo2
//
//  Created by raycad sun on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LanguageUtil.h"

@implementation LanguageUtil
@synthesize languageNameList = _languageNameList;

static LanguageUtil *_instance = nil;

+ (LanguageUtil *)instance
{
	@synchronized([LanguageUtil class]) {
		if (!_instance)
			_instance = [[self alloc] init];
        
		return _instance;
	}
    
	return nil;
}

+ (id)alloc
{
	@synchronized([LanguageUtil class]) {
		NSAssert(_instance == nil, @"Attempted to allocate a second instance of a singleton.");
		_instance = [super alloc];
		return _instance;
	}
    
	return nil;
}

- (id)init {
	self = [super init];
	if (self != nil) {
		// Initialize parameters
        [self setCurrentLanguageCode:ENGLISH];
        [self initResource];
	}
    
	return self;
}

- (LanguageCode)currentLanguageCode
{
    return _currentLanguageCode;
}

- (BOOL)setCurrentLanguageCode:(LanguageCode)newValue 
{
    if (_currentLanguageCode == newValue)
        return NO;
    
    _currentLanguageCode = newValue;
    [self initResource];
    
    //Send notification to all observer
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_Language_Changed object:nil];
    return YES;
}

- (NSString *)languageFileNameByCode:(LanguageCode) lc
{
    switch (lc) {
        case VIETNAMESE:
            return @"vi";
            
        default:
            return @"en";
    }
}

- (NSString *)getLanguageNameByCode:(LanguageCode)languageCode
{
    int index = (int) languageCode;
    if ((index < 0)
        || (index >= [_languageNameList count]))
        return @"";
    return [_languageNameList objectAtIndex:index];
}

- (NSString *)getLanguageNameByCurrentLanguageCode{
    return [self getLanguageNameByCode:self.currentLanguageCode];
}

- (void)initResource 
{
    NSString *languageFileName = [self languageFileNameByCode:_currentLanguageCode];
    // Read in and store as string
    NSString *file = [[NSBundle mainBundle] pathForResource:languageFileName ofType:@""];
    _textResourceDictionary = [NSDictionary dictionaryWithContentsOfFile:file];    
    
    _languageNameList = [[NSArray alloc] initWithObjects:
                         @"en",
                         @"vn",
                         nil];
}

- (NSString *)getTextByKey:(NSString *)key
{
    if (_textResourceDictionary == nil)
        return @"";
    
    return [_textResourceDictionary valueForKey:key];
}

@end
