//
//  EpubUtils.m
//  
//
//  Created by Dato - horseuvn@gmail.com on 6/29/12.
//  Copyright (c) 2012 __TechMaster__. All rights reserved.
//

function MyAppGet_OffsetTop_ForAnchor(anchorname){
    var obj = document.getElementById(anchorname);
    if(obj != null){
        var posY = obj.offsetTop;
        while(obj.offsetParent){
            obj = obj.offsetParent;
            posY = posY + obj.offsetTop;
        }
            return posY;
    }else{
        return -1;
    }
}

function MyAppGet_Content_ScrollHeight(){
    var htmlObj = document.getElementsByTagName("html")[0];
    
    var scrollHeight = htmlObj.scrollHeight;
    return scrollHeight;
}

function MyApp_GetAllImagePath(){
    var imgArray = document.getElementsByTagName("img");
    var returnString = "";
    for(var i=0;i<imgArray.length-1;i++){
        returnString = returnString + imgArray[i].src +",";
    }
    returnString = returnString + imgArray[imgArray.length - 1].src;
    return returnString;
}
