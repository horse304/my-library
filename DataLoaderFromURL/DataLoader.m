//
//  RSSConnection.m
//  RSSReader
//
//  Created by Techmaster on 6/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataLoader.h"
@interface DataLoader()
@property (nonatomic, strong) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *data;
@end

@implementation DataLoader
@synthesize connection = _connection;
@synthesize url = _url;
@synthesize delegate = _delegate;
@synthesize data = _data;

-(id)initWithURL:(NSURL *)URL delegate:(id)delegate{
    self = [super init];
    if (self) {
        self.url = URL;
        self.delegate = delegate;
    }
    return self;
}

-(void)start{
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [connection start];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}
#pragma mark - URL Connection Delegate
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"Connection Failed!");
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    self.data = [[NSMutableData alloc] init];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.data appendData:data];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    //Return data to delegate
    if ([self.delegate respondsToSelector:@selector(dataLoaderDidLoading:data:)]) {
        [self.delegate dataLoaderDidLoading:self data:self.data];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}



@end
