//
//  QE_FacebookController.m
//  QuizEnzine
//
//  Created by Techmaster on 6/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define kFacebookAppId @"375137232547406"
#import "QE_FacebookController.h"


@interface QE_FacebookController()
@property (strong, nonatomic) Facebook *facebook;
@property (strong, nonatomic) NSArray *permissionFB;
@property (assign, nonatomic) enum QE_FacebookControllerRequestType currentRequestType;
@end

@implementation QE_FacebookController
@synthesize delegate = _delegate;
@synthesize quizAPI = _quizAPI;
@synthesize facebook = _facebook;
@synthesize permissionFB = _permissionFB;
@synthesize currentRequestType = _currentRequestType;

-(id)initWithDelegate:(id)delegate{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.facebook = [[Facebook alloc] initWithAppId:kFacebookAppId andDelegate:self];
        self.permissionFB = [NSArray arrayWithObjects:
                             @"offline_access",
                             @"user_birthday",
                             @"email",nil];
    }
    return self;
}
-(BOOL)handlerURL:(NSURL *)url{
    return [self.facebook handleOpenURL:url];
}

-(void)login{
    [self.facebook authorize:self.permissionFB];
}

#pragma mark - FBSessionDelegate
- (void)fbDidLogin{
    //get user info
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"SELECT uid, username, first_name, last_name, pic,sex,email,timezone,profile_url FROM user WHERE uid=me()", @"query",
                                   nil];
    [[self facebook] requestWithMethodName:@"fql.query"
                                 andParams:params
                             andHttpMethod:@"POST"
                               andDelegate:self];    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.currentRequestType = RequestUserInfo;
}

/**
 * Called when the user dismissed the dialog without logging in.
 */
- (void)fbDidNotLogin:(BOOL)cancelled{
    NSLog(@"not logged in");
}

/**
 * Called after the access token was extended. If your application has any
 * references to the previous access token (for example, if your application
 * stores the previous access token in persistent storage), your application
 * should overwrite the old access token with the new one in this method.
 * See extendAccessToken for more details.
 */
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt{
    
}

/**
 * Called when the user logged out.
 */
- (void)fbDidLogout{
    
}

/**
 * Called when the current session has expired. This might happen when:
 *  - the access token expired
 *  - the app has been disabled
 *  - the user revoked the app's permissions
 *  - the user changed his or her password
 */
- (void)fbSessionInvalidated{
    
}

#pragma mark - FBRequestDelegate
- (void)request:(FBRequest *)request didLoad:(id)result{
    if (self.currentRequestType == RequestUserInfo) {
        if ([result isKindOfClass:[NSArray class]]) {
            result = [result objectAtIndex:0];
        }
        
        if ([result isKindOfClass:[NSDictionary class]]) {
            if ([result valueForKey:@"username"] != nil) { 
                
                NSLog(@"%@",[[result valueForKey:@"uid"] class]);
                [QE_UserInfo sharedUserInfo].facebookID = [NSString stringWithFormat:@"%1.0f",[[result valueForKey:@"uid"] doubleValue]];                
                [QE_UserInfo sharedUserInfo].username = [result valueForKey:@"username"];
                [QE_UserInfo sharedUserInfo].first_name = [result valueForKey:@"first_name"];
                [QE_UserInfo sharedUserInfo].last_name = [result valueForKey:@"last_name"];
                [QE_UserInfo sharedUserInfo].gender = [result valueForKey:@"sex"];
                [QE_UserInfo sharedUserInfo].email = [result valueForKey:@"email"];
                [QE_UserInfo sharedUserInfo].timezone = [[result valueForKey:@"timezone"] intValue];
                
                /*NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                 [dateFormat setDateFormat:@"MM/dd/yyyy"];
                 [dateFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:self.ccuser.timezone*3600]];
                 self.ccuser.birthday = [dateFormat dateFromString:[result valueForKey:@"birthday"]];*/
                
                [QE_UserInfo sharedUserInfo].link = [result valueForKey:@"profile_url"];
                [QE_UserInfo sharedUserInfo].picture = [result valueForKey:@"pic"];
            }
        }
        //Send request login facebook to server
        [[QuizAPI sharedQuizAPI] requestLoginWithFacebook:[QE_UserInfo sharedUserInfo] delegate:self];
    }
}

/**
 * Called when an error prevents the request from completing successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - QuizAPIDelegate
-(void)quizAPIResponseFacebookRequest:(NSDictionary *)response{
    if (self.currentRequestType == RequestUserInfo) {
        if ([[response valueForKey:@"result"] intValue] == 1) {
            
        }
    }
}
@end
