<?php
include 'db_structure.php';
class Database{
    public $dbconn;
    public function __construct() {
        $config = $GLOBALS['Application']['config'];
        $this->dbconn = mysqli_connect($config->config_host, $config->config_username, $config->config_password, $config->config_dbname);
        if (!$this->dbconn){
            die('Could not connect mysql');
        }
    }
    private function executeSelectQuery($params, $from, $where){
        $query = "SELECT ".implode(",", $params)." FROM ".$from." WHERE ".$where;
        $query_result = $this->dbconn->query($query);
        return $query_result;
    }
    private function executeInsertQuery($params,$values,$table_name){
        $query = "INSERT INTO ".$table_name."(".  implode(",", $params) .") VALUES (".  implode(",", $values). ")";
        $query_result = $this->dbconn->query($query);
        return $query_result;
    }
    
    private function table_fetch_row($params,$query_result){
        if($query_result->num_rows > 0 && $query_result->field_count == count($params)){            
            $row = $query_result->fetch_row();
            $index = 0;
            $result = NULL;
            foreach ($params as $field){
                $result[$field] = $row[$index];
                $index++;
            }
            return $result;
        }else return NULL;
    }


    public function getAccessToken($access_token_id){
        $accesstoken_table = DB_Structure::$ACCESSTOKEN_TABLE;
        $params = array('accesstoken_id','start_date','expire_date','userid','status');
        $from = $accesstoken_table;
        $where = "accesstoken_id = $access_token_id";
        $query_result = $this->executeSelectQuery($params, $from, $where);
        
        $result = NULL;
        if ($query_result->num_rows == 1){
            $result = $this->table_fetch_row($params, $query_result);
        }
        return $result;
    }
    
    public function getUser($email){
        $result = NULL;        
        $user_table = DB_Structure::$USER_TABLE;
        
        $params = array(
            'userid',
            'user_fullname',
            'user_email',
            'user_password',
            'user_pic',
            'user_fbid',
            'user_join_date',
            'user_status');
        $from = $user_table;
        $where = "user_email = '$email'";
        $query_result = $this->executeSelectQuery($params, $from, $where);
        
        if($query_result->num_rows == 1){
            $result = $this->table_fetch_row($params, $query_result);
        }
        return $result;
    }
    
    public function getUserByFacebook($fbid){
        $result = NULL;
        $user_table = DB_Structure::$USER_TABLE;
        
        $params = array(
            'userid',
            'user_fullname',
            'user_email',
            'user_password',
            'user_pic',
            'user_fbid',
            'user_join_date',
            'user_status');
        $from = $user_table;
        $where = "user_fbid = '$fbid'";
        $query_result = $this->executeSelectQuery($params, $from, $where);
        
        if($query_result->num_rows == 1){
            $result = $this->table_fetch_row($params, $query_result);
        }
        return $result;
    }


    public function create_access_token($userid){
        //Update status for old access token
        $accesstoken_table = DB_Structure::$ACCESSTOKEN_TABLE;
        $query = "UPDATE $accesstoken_table SET status=0 WHERE userid=$userid && status=1";
        $database = $GLOBALS['Application']['database'];
        $query_result = $database->dbconn->query($query);
        if($query_result == TRUE){
            //Insert new access token
            $params = array(
                'start_date',
                'expire_date',
                'userid',
                'status'
            );
            $values = array(
                "UTC_TIMESTAMP()",
                "TIMESTAMPADD(WEEK,1,UTC_TIMESTAMP())",
                $userid,
                1
            );
            $insert_result = $this->executeInsertQuery($params, $values, $accesstoken_table);
            if ($insert_result){
                //Select access token
                $select_params = array(
                    'accesstoken_id',
                    'start_date',
                    'expire_date',
                    'userid',
                    'status'
                );
                $from = $accesstoken_table;
                $where = "userid = $userid && status = true";
                $select_result = $this->executeSelectQuery($select_params, $from, $where);
                if($select_result->num_rows == 1){
                    $result = $this->table_fetch_row($select_params, $select_result);
                    return $result;
                }
            }
        }
        return NULL;
    }
}
?>
