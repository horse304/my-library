//
//  LoadPlist.h
//  DemoCoreImage
//
//  Created by Techmaster on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <CoreImage/CoreImage.h>
#import <Foundation/Foundation.h>

@interface LoadPlist : NSObject
+(NSArray *)getFiltersArray;
@end
