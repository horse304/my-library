//
//  SpringBoardViewController.m
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "SpringBoardViewController.h"
#define SPRING_BOARD_ITEM_TAG 10

@interface SpringBoardViewController ()
@property (assign, nonatomic) int numberColumn;
@property (assign, nonatomic) int numberRow;
@property (assign, nonatomic) CGRect viewFrame;

//UI Properties
@property (strong, nonatomic) UIScrollView *theScrollView;
@end

@implementation SpringBoardViewController
@synthesize            datasource = _datasource;
@synthesize              delegate = _delegate;
@synthesize             itemArray = _itemArray;
@synthesize             numberRow = _numberRow;
@synthesize          numberColumn = _numberColumn;
@synthesize         theScrollView = _theScrollView;
@synthesize             viewFrame = _viewFrame;

- (id)initWithDataSource:(id<SpringBoardDatasource>)datasource frame:(CGRect)frame
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        // Custom initialization
        self.itemArray = [[NSMutableArray alloc] init ];
        self.datasource = datasource;
        self.viewFrame = frame;
    }
    return self;
}

#pragma mark - View Life Cycle
-(void)loadView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super loadView];
    self.view.frame = self.viewFrame;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //Add scrollview;
    self.theScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.theScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.theScrollView setScrollEnabled:YES];
    [self.theScrollView setPagingEnabled:YES];
    [self.theScrollView setUserInteractionEnabled:YES];
    [self.theScrollView setMultipleTouchEnabled:YES];
    [self.theScrollView setShowsVerticalScrollIndicator:NO];
    [self.theScrollView setShowsHorizontalScrollIndicator:NO];
    self.theScrollView.delegate = self;
    [self.view addSubview:self.theScrollView];
}

- (void)viewDidLoad
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewDidLoad];
    [self reloadSpringBoard];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
}

-(void)viewWillDisappear:(BOOL)animated{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
}

- (void)viewDidUnload
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Rotation handling
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    return YES;
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self reloadSpringBoard];
}

#pragma mark - Spring board layout
-(void)reloadSpringBoard{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    for (UIView *subView in self.theScrollView.subviews) {
        if (subView.tag == SPRING_BOARD_ITEM_TAG) {
            [subView removeFromSuperview];
        }
    }
    
    self.numberColumn = 0;
    self.numberRow = 0;
    if ([self.datasource respondsToSelector:@selector(springBoardNumberRowOf:)]
        && [self.datasource respondsToSelector:@selector(springBoardNumberColumnOf:)]
        && [self.datasource respondsToSelector:@selector(springBoardItemArrayOf:)]) {
        self.numberRow = [self.datasource springBoardNumberRowOf:self];
        self.numberColumn = [self.datasource springBoardNumberColumnOf:self];
        self.itemArray = [[self.datasource springBoardItemArrayOf:self] mutableCopy];        
    }
    
    if (self.itemArray.count > 0 && self.numberRow > 0 && self.numberColumn > 0) {
        int rowIndex = 0;
        int columnIndex = 0;
        int pageIndex = 0;
        float item_width = self.theScrollView.frame.size.width / (float)self.numberColumn;
        float item_height = self.theScrollView.frame.size.height / (float)self.numberRow;
#ifdef DEBUGX
        NSLog(@"scrollViewHeight:%f",self.theScrollView.frame.size.height);
#endif
        for (id item in self.itemArray) {            
            if ([item isKindOfClass:[SpringBoardItem class]]) {
                SpringBoardItem *springBoardItem = (SpringBoardItem *)item;
                springBoardItem.view.frame = CGRectMake(columnIndex * item_width + (pageIndex * self.theScrollView.frame.size.width), rowIndex * item_height, item_width, item_height);
                springBoardItem.view.tag = SPRING_BOARD_ITEM_TAG;
                [self.theScrollView addSubview:springBoardItem.view];
                if (columnIndex < self.numberColumn - 1) {
                    columnIndex ++;
                }else {
                    columnIndex = 0;
                    if (rowIndex < self.numberRow - 1) {
                        rowIndex ++;
                    }else {
                        rowIndex = 0;
                        if ([self.itemArray indexOfObject:item] < self.itemArray.count - 1) {
                            pageIndex++;
                        }
                    }
                }
            }
        }
        
        
        self.theScrollView.contentSize = CGSizeMake(((pageIndex+1) * self.theScrollView.frame.size.width), self.theScrollView.frame.size.height);
        if ([self.delegate respondsToSelector:@selector(springBoardGetNumberOfPages:sender:)]) {
            [self.delegate springBoardGetNumberOfPages:(pageIndex + 1) sender:self];
        }
    }
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    int currentPage = round((float)self.theScrollView.contentOffset.x / (float)self.theScrollView.frame.size.width);
    if ([self.delegate respondsToSelector:@selector(springBoardDidScrollToPageIndex:sender:)]) {
        [self.delegate springBoardDidScrollToPageIndex:currentPage sender:self];
    }
}

@end
