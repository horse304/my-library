//
//  FilterView.h
//  DemoCoreImage
//
//  Created by Techmaster on 6/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <CoreImage/CoreImage.h>
#import <UIKit/UIKit.h>
@protocol FilterViewDelegate<NSObject>
-(void)filterViewDidTap:(id)sender;
@end

@interface FilterView : UIView
@property (weak, nonatomic) id<FilterViewDelegate> delegate;
@property (retain, nonatomic) CIFilter *ciFilter;
@property (weak, nonatomic) CIContext *ciContext;
-(id)initWithFilter:(CIFilter *)filter context:(CIContext *)context thumbnail:(UIImage *)thumbnailImg frame:(CGRect)frame;
@end
