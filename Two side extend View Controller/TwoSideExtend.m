//
//  ViewController.m
//  TwoSideExtendViewController
//
//  Created by Techmaster on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TwoSideExtend.h"

@interface TwoSideExtend ()

@property (retain, nonatomic) UIViewController *leftViewController;
@property (retain, nonatomic) UIViewController *rightViewController;
@property (retain, nonatomic) UIViewController *mainViewController;

//Gesture
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) UIPanGestureRecognizer *panGesture;
@end

@implementation TwoSideExtend
@synthesize              leftViewController = _leftViewController;
@synthesize             rightViewController = _rightViewController;
@synthesize              mainViewController = _mainViewController;
@synthesize                           state = _state;
@synthesize                enableTapGesture = _enableTapGesture;
@synthesize                enablePanGesture = _enablePanGesture;
@synthesize                      tapGesture = _tapGesture;
@synthesize                      panGesture = _panGesture;

-(id)initWithLeftViewController:(UIViewController *)leftVC 
            rightViewController:(UIViewController *)rightVC 
             mainViewController:(UIViewController *)mainVC{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self) {
        self.leftViewController = leftVC;
        self.rightViewController = rightVC;
        self.mainViewController = mainVC;
        
        self.state = Normal;
        self.enablePanGesture = YES;
        self.enableTapGesture = YES;
        
        [self addChildViewController:self.leftViewController];
        [self addChildViewController:self.rightViewController];
        [self addChildViewController:self.mainViewController];
    }
    
    return self;
}

#pragma mark - View Life Cycle
-(void)viewDidLayoutSubviews{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0")){
        [super viewDidLayoutSubviews];
    }
    //Layout left view controller
    float leftViewWidth = self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR;
    self.leftViewController.view.frame = CGRectMake(0, 0, leftViewWidth, self.view.bounds.size.height);
    //Layout right view controller    
    float rightViewWidth = self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR;
    self.rightViewController.view.frame = CGRectMake(WIDTH_MAINVIEW_DISAPPEAR, 0, rightViewWidth, self.view.bounds.size.height);
    
    //Layout main view controller
    if (self.state == Normal) {
        self.mainViewController.view.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    }else if(self.state == ShowingLeft){
        self.mainViewController.view.center = CGPointMake(self.view.bounds.size.width/2 + (self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR), self.view.bounds.size.height/2);
    }else {
        self.mainViewController.view.center = CGPointMake(self.view.bounds.size.width/2 - (self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR), self.view.bounds.size.height/2);
    }
}

-(void)loadView{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super loadView];
    //Add left view
    if (self.leftViewController != nil) {
        float leftViewWidth = self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR;
        self.leftViewController.view.frame = CGRectMake(0, 0, leftViewWidth, self.view.bounds.size.height);
        [self.view addSubview:self.leftViewController.view];
    }
    
    //Add right view
    if (self.rightViewController != nil) {
        float rightViewWidth = self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR;
        self.rightViewController.view.frame = CGRectMake(WIDTH_MAINVIEW_DISAPPEAR, 0, rightViewWidth, self.view.bounds.size.height);
        [self.view addSubview:self.rightViewController.view];
    }
    
    //Add main view and swipe gesture
    if (self.mainViewController != nil) {
        self.mainViewController.view.frame = self.view.bounds;
        self.mainViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.mainViewController.view.layer.masksToBounds = NO;
        self.mainViewController.view.layer.shadowColor = [[UIColor blackColor] CGColor];
        self.mainViewController.view.layer.shadowOpacity = 1.0f;
        self.mainViewController.view.layer.shadowRadius = 5.0f;
        [self.view addSubview:self.mainViewController.view];
        [self.view bringSubviewToFront:self.mainViewController.view];
    }
    
    //Add Pan Gesture
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panOnMainView:)];    
    self.panGesture.delegate = self;
    self.panGesture.maximumNumberOfTouches = 1;
    self.panGesture.minimumNumberOfTouches = 1;
    [self.mainViewController.view addGestureRecognizer:self.panGesture];
    
    //Add Tab Gesture
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnMainView:)];
    self.tapGesture.delegate = self;
    [self.mainViewController.view addGestureRecognizer:self.tapGesture];
}


- (void)viewDidLoad
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark - Rotation handling
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.leftViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.rightViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.mainViewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    if(SYSTEM_VERSION_LESS_THAN(@"5.0")){
        [self viewDidLayoutSubviews];
    }
    
    [self.leftViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.rightViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self.mainViewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    return YES;
}

#pragma mark - UIGestureDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    
    if (gestureRecognizer == self.tapGesture
        && ([touch.view isKindOfClass:[UIControl class]]
        || [touch.view isKindOfClass:[UINavigationBar class]])) {
        // we touched a button, slider, or other UIControl
        return NO; // ignore the touch
    }
    return YES;
}
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (gestureRecognizer == self.tapGesture) {        
        return self.enableTapGesture;
    }
    
    if(gestureRecognizer == self.panGesture){
        return self.enablePanGesture;
    }

    return YES;
}

#pragma mark - UIEvent
-(void)tapOnMainView:(UITapGestureRecognizer *)tapGesture{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Switch to normal
    [self animationToNormal];
}

-(void)panOnMainView:(UIPanGestureRecognizer *)panGesture{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (panGesture.state == UIGestureRecognizerStateBegan || panGesture.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [panGesture translationInView:self.view];
        float maxMainView_Center_X = self.view.bounds.size.width/2 + (self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR);
        float minMainView_Center_X = self.view.bounds.size.width/2 - (self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR);
        
        if (self.mainViewController.view.center.x + translation.x <= maxMainView_Center_X
            && self.mainViewController.view.center.x + translation.x >= minMainView_Center_X) {            
            self.mainViewController.view.center = CGPointMake(self.mainViewController.view.center.x + translation.x, self.mainViewController.view.center.y);
            
            if (self.mainViewController.view.center.x > self.view.bounds.size.width/2) {
                self.leftViewController.view.hidden = NO;
                self.rightViewController.view.hidden = YES;
            }else {
                self.leftViewController.view.hidden = YES;
                self.rightViewController.view.hidden = NO;
            }
        }
        
        [panGesture setTranslation:CGPointZero inView:self.view];
    }else {
        float distanceForSwitchNormal = 0;
        if (self.state == Normal) {
            distanceForSwitchNormal = self.view.bounds.size.width/8;
        }else {
            distanceForSwitchNormal = 7*self.view.bounds.size.width/8;
        }
        
        if (abs(self.mainViewController.view.center.x - self.view.bounds.size.width/2) < distanceForSwitchNormal) {
            [self animationToNormal];
        }else if (self.mainViewController.view.center.x < self.view.bounds.size.width/2) {
            [self animationToRight];
        }else {
            [self animationToLeft];
        }
    }
}

#pragma mark - Animation

-(void)animationToNormal{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    //Switch to normal state
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationCurveEaseInOut animations:^{
        self.mainViewController.view.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    } completion:^(BOOL finished) {               
        self.state = Normal;
        self.leftViewController.view.hidden = YES;
        self.rightViewController.view.hidden = YES;
    }];
}

-(void)animationToLeft{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.leftViewController.view.hidden = NO;
    self.rightViewController.view.hidden = YES;
    //Switch to left state
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationCurveEaseInOut animations:^{
        self.mainViewController.view.frame = CGRectMake((self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR), 0, self.mainViewController.view.frame.size.width, self.mainViewController.view.frame.size.height);
    } completion:^(BOOL finished) {
        //Make it cool
        [UIView animateWithDuration:0.05 animations:^{
            self.mainViewController.view.frame = CGRectMake((self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR)-3, 0, self.mainViewController.view.frame.size.width, self.mainViewController.view.frame.size.height);
        }completion:^(BOOL finished) {
            [UIView animateWithDuration:0.05 animations:^{
                self.mainViewController.view.frame = CGRectMake((self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR), 0, self.mainViewController.view.frame.size.width, self.mainViewController.view.frame.size.height);
            } completion:^(BOOL finished) { 
                self.state = ShowingLeft;
            }];
        }];
    }];
}

-(void)animationToRight{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self.leftViewController.view.hidden = YES;
    self.rightViewController.view.hidden = NO;
    //Switch to right state
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationCurveEaseInOut animations:^{
        self.mainViewController.view.frame = CGRectMake(-(self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR), 0, self.mainViewController.view.frame.size.width, self.mainViewController.view.frame.size.height);
    } completion:^(BOOL finished) {
        //Make it cool
        [UIView animateWithDuration:0.05 animations:^{
            self.mainViewController.view.frame = CGRectMake(-(self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR) + 3, 0, self.mainViewController.view.frame.size.width, self.mainViewController.view.frame.size.height);
        }completion:^(BOOL finished) {
            [UIView animateWithDuration:0.05 animations:^{
                self.mainViewController.view.frame = CGRectMake(-(self.view.bounds.size.width - WIDTH_MAINVIEW_DISAPPEAR), 0, self.mainViewController.view.frame.size.width, self.mainViewController.view.frame.size.height);
            } completion:^(BOOL finished) { 
                self.state = ShowingRight;
            }];
        }];
    }];
}

@end
