//
//  QE_UserDefault.h
//  QuizEnzine
//
//  Created by Techmaster on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QE_AppUserDefault : NSObject
@property (retain, nonatomic) NSString *accesstoken;
@property (retain, nonatomic) NSDate *expirationDate;

-(void)save;
-(void)saveValue:(id)object forKey:(NSString *)key;
@end
