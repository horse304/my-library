//
//  AppDelegate.h
//  temp
//
//  Created by Jake Gundersen on 10/14/11.
//  Copyright (c) 2011 University of Utah Hospitals and Clinics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
