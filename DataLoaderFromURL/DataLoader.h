//
//  RSSConnection.h
//  RSSReader
//
//  Created by Techmaster on 6/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataLoaderDelegate <NSObject>
-(void)dataLoaderDidLoading:(id)sender data:(NSData *)data;

@end

@interface DataLoader : NSObject<NSURLConnectionDataDelegate,NSURLConnectionDelegate>
@property (retain, nonatomic) id<DataLoaderDelegate> delegate;
@property (retain, nonatomic) NSURL *url;

-(id)initWithURL:(NSURL *)url delegate:(id)delegate;
-(void)start;
@end
