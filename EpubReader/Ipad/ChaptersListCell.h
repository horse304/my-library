//
//  ChaptersListCell.h
//  nexto
//
//  Created by Dato - Techmaster.vn on 2012-07-27.
//  Copyright 2012 Dato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EpubTOCItem.h"

@interface ChaptersListCell : UITableViewCell
@property (retain, nonatomic) EpubTOCItem *tocItem;
@end
