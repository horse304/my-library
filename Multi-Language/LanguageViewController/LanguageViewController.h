//
//  LanguageViewController.h
//  SexFunFacts
//
//  Created by Techmaster on 8/7/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageUtil.h"
#import "CoreDataController.h"

@interface LanguageViewController : UIViewController

-(void)updateWhenLanguageChanged;
-(void)updateWhenCoreDataChanged;
@end
