//
//  ViewController.h
//  temp
//
//  Created by Jake Gundersen on 10/14/11.
//  Copyright (c) 2011 Interrobang Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreVideo/CoreVideo.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import "GOLModel.h"
#import "CZGPerlinGenerator.h"

@interface ViewController : GLKViewController <AVCaptureVideoDataOutputSampleBufferDelegate> {
    AVCaptureSession *session;
    
    CIContext *coreImageContext;
    
    GLuint _renderBuffer;
    
    CGSize screenSize;
    CGContextRef cgcontext;
    
    CIImage *maskImage;
    
    GOLModel *GOL;
    IBOutlet UIButton *playGOLButton;
    
    NSTimer *refreshTimer;
    
    CZGPerlinGenerator *perlin;
    
    float scl;
}

@property (strong, nonatomic) EAGLContext *context;

-(void)setupCGContext;
-(CIImage *)drawGameOfLife;
- (IBAction)playGOL:(id)sender;
-(void)updateGOL:(NSTimer *)timer;

@end
