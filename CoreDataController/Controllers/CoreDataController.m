//
//  CoreDataController.m
//  SexFunFacts
//
//  Created by Techmaster on 8/4/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "CoreDataController.h"
@interface CoreDataController()

/**
	The Managed Object Context
 */
@property (readonly, nonatomic) NSManagedObjectContext *managedObjectContext;

/**
	The Managed Object Model
 */
@property (readonly, nonatomic) NSManagedObjectModel *managedObjectModel;

/**
	The Persistent Store Coordinator
 */
@property (readonly, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation CoreDataController

/**
	The signleton of CoreDataController
 */
static CoreDataController *_instance = nil;

@synthesize                      managedObjectContext = __managedObjectContext;
@synthesize                        managedObjectModel = __managedObjectModel;
@synthesize                persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize             fact_FetchedResultsController = _fact_FetchedResultsController;
@synthesize         category_FetchedResultsController = _category_FetchedResultsController;

#pragma mark - Initialize the singleton instance
+ (CoreDataController *) shared
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    @synchronized([CoreDataController class])
    {
        if (!_instance)
        {
            _instance = [[self alloc] init];
        }

        return _instance;
    }

    return nil;
}


+ (id) alloc
{
    @synchronized([CoreDataController class])
    {
        NSAssert(_instance == nil, @"Attempted to allocate a second instance of a singleton.");
        _instance = [super alloc];
        return _instance;
    }

    return nil;
}


- (id) init
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    self = [super init];
    if (self)
    {
        [self initializeVariables];
    }

    return self;
}

-(void)initializeVariables{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWhenLanguageChanged) name:NOTIFICATION_Language_Changed object:nil];
    [self insertSampleData];
}

#pragma mark - Multi Languages
-(void)updateWhenLanguageChanged{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self.category_FetchedResultsController updateFetchRequest];
    [self.fact_FetchedResultsController updateFetchRequest];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_COREDATA_UPDATED object:nil];
}

#pragma mark - Core Data stack

/**
    If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
    @returns Returns the managed object context for the application.
 */
- (NSManagedObjectContext *)managedObjectContext

{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
	If the model doesn't already exist, it is created from the application's model.
	@returns Returns the managed object model for the application.
 */
- (NSManagedObjectModel *)managedObjectModel
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif    
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:COREDATA_FILE_NAME withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

/**
	If the coordinator doesn't already exist, it is created and the application's store added to it.
	@returns Returns the persistent store coordinator for the application.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",COREDATA_FILE_NAME]];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }  
    
    //[[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
    return __persistentStoreCoordinator;
}

-(EntityResultsController *)fact_FetchedResultsController{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if(_fact_FetchedResultsController == nil){
        _fact_FetchedResultsController = [[EntityResultsController alloc] initWithEntityName:ENTITY_FACTITEM 
                                                                                     sortKey:ENTITY_FACTITEM_SORTKEYDEFAULT
                                                                                   ascending:NO
                                                                        managedObjectContext:self.managedObjectContext];
    }
    
    return _fact_FetchedResultsController;
}

-(EntityResultsController *)category_FetchedResultsController{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    if (_category_FetchedResultsController == nil) {
        _category_FetchedResultsController = [[EntityResultsController alloc] initWithEntityName:ENTITY_CATEGORY 
                                                                                         sortKey:ENTITY_CATEGORY_SORTKEYDEFAULT
                                                                                       ascending:YES
                                                                            managedObjectContext:self.managedObjectContext];
    }
    
    return _category_FetchedResultsController;
}

#pragma mark - Application's Documents directory
/**
	@returns Return the URL of Application Document Directory
 */
- (NSURL *)applicationDocumentsDirectory

{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Utilites
-(void)insertSampleData{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [self resetData];
    
    //Insert categories
    Category *newCategory_1_en = (Category*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_CATEGORY inManagedObjectContext:self.managedObjectContext];
    newCategory_1_en.category_id = [NSNumber numberWithInt:1];
    newCategory_1_en.category_name = @"Fun Facts";
    newCategory_1_en.category_image_path = @"category_1_en.png";
    newCategory_1_en.language_code = @"en";
    Category *newCategory_1_vn = (Category*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_CATEGORY inManagedObjectContext:self.managedObjectContext];
    newCategory_1_vn.category_id = [NSNumber numberWithInt:1];
    newCategory_1_vn.category_name = @"Vui";
    newCategory_1_vn.category_image_path = @"category_1_vn.png";
    newCategory_1_vn.language_code = @"vn";
    
    Category *newCategory_2_en = (Category*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_CATEGORY inManagedObjectContext:self.managedObjectContext];
    newCategory_2_en.category_id = [NSNumber numberWithInt:2];
    newCategory_2_en.category_name = @"Woman Sex Facts";
    newCategory_2_en.category_image_path = @"category_2_en.png";
    newCategory_2_en.language_code = @"en";
    Category *newCategory_2_vn = (Category*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_CATEGORY inManagedObjectContext:self.managedObjectContext];
    newCategory_2_vn.category_id = [NSNumber numberWithInt:2];
    newCategory_2_vn.category_name = @"Phụ nữ";
    newCategory_2_vn.category_image_path = @"category_2_vn.png";
    newCategory_2_vn.language_code = @"vn";
    
    Category *newCategory_3_en = (Category*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_CATEGORY inManagedObjectContext:self.managedObjectContext];
    newCategory_3_en.category_id = [NSNumber numberWithInt:3];
    newCategory_3_en.category_name = @"Man Sex Facts";
    newCategory_3_en.category_image_path = @"category_3_en.png";
    newCategory_3_en.language_code = @"en";
    Category *newCategory_3_vn = (Category*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_CATEGORY inManagedObjectContext:self.managedObjectContext];
    newCategory_3_vn.category_id = [NSNumber numberWithInt:3];
    newCategory_3_vn.category_name = @"Đàn ông";
    newCategory_3_vn.category_image_path = @"category_3_vn.png";
    newCategory_3_vn.language_code = @"vn";
    
    
    //Insert fact items
    Factitem *newFactItem_1_en = (Factitem*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_FACTITEM inManagedObjectContext:self.managedObjectContext];
    newFactItem_1_en.fact_id = [NSNumber numberWithInt:1];
    newFactItem_1_en.fact_content = @"Male Content";
    newFactItem_1_en.language_code = @"en";
    newFactItem_1_en.fact_image_path = @"newFactitem_1_en.png";
    newFactItem_1_en.fact_viewcount = [NSNumber numberWithInt:0];
    newFactItem_1_en.fact_createdDate = [NSDate dateWithTimeIntervalSinceNow:-100];
    newFactItem_1_en.toCategory = newCategory_1_en;
    
    Factitem *newFactItem_1_vn = (Factitem*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_FACTITEM inManagedObjectContext:self.managedObjectContext];
    newFactItem_1_vn.fact_id = [NSNumber numberWithInt:1];
    newFactItem_1_vn.fact_content = @"Đàn ông Noi dung test";
    newFactItem_1_vn.language_code = @"vn";
    newFactItem_1_vn.fact_image_path = @"newFactitem_1_vn.png";
    newFactItem_1_vn.fact_viewcount = [NSNumber numberWithInt:0];
    newFactItem_1_vn.fact_createdDate = [NSDate dateWithTimeIntervalSinceNow:-100];
    newFactItem_1_vn.toCategory = newCategory_1_vn;
    
    Factitem *newFactItem_2_en = (Factitem*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_FACTITEM inManagedObjectContext:self.managedObjectContext];
    newFactItem_2_en.fact_id = [NSNumber numberWithInt:2];
    newFactItem_2_en.fact_content = @"Female Content";
    newFactItem_2_en.language_code = @"en";
    newFactItem_2_en.fact_image_path = @"newFactitem_2_en.png";
    newFactItem_2_en.fact_viewcount = [NSNumber numberWithInt:0];
    newFactItem_2_en.fact_createdDate = [NSDate dateWithTimeIntervalSinceNow:-200];
    newFactItem_2_en.toCategory = newCategory_1_en;
    
    Factitem *newFactItem_2_vn = (Factitem*)[NSEntityDescription insertNewObjectForEntityForName:ENTITY_FACTITEM inManagedObjectContext:self.managedObjectContext];
    newFactItem_2_vn.fact_id = [NSNumber numberWithInt:2];
    newFactItem_2_vn.fact_content = @"Sex fun fact Noi dung test";
    newFactItem_2_vn.language_code = @"vn";
    newFactItem_2_vn.fact_image_path = @"newFactitem_2_vn.png";
    newFactItem_2_vn.fact_viewcount = [NSNumber numberWithInt:0];
    newFactItem_2_vn.fact_createdDate = [NSDate dateWithTimeIntervalSinceNow:-50];
    newFactItem_2_vn.toCategory = newCategory_1_vn;
    
    //Copy image to document directory    
    NSArray *documentDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *imagesPath = [[documentDirectory objectAtIndex:0] stringByAppendingPathComponent:IMAGES_DIRECTORY_NAME];
#ifdef DEBUGX
    NSLog(@"images path:%@",imagesPath);
#endif
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:imagesPath isDirectory:nil]) {
        [fileManager removeItemAtPath:imagesPath error:nil];
    }
    
    [fileManager createDirectoryAtPath:imagesPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    //Add category image to directory
    NSString *category_1_en_Path = [[NSBundle mainBundle] pathForResource:@"category_1_en" ofType:@"png"];
    NSString *documentCategory_1_en_Path = [imagesPath stringByAppendingPathComponent:@"category_1_en.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:category_1_en_Path 
                                            toPath:documentCategory_1_en_Path 
                                             error:nil];
    
    NSString *category_1_vn_Path = [[NSBundle mainBundle] pathForResource:@"category_1_vn" ofType:@"png"];
    NSString *documentCategory_1_vn_Path = [imagesPath stringByAppendingPathComponent:@"category_1_vn.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:category_1_vn_Path 
                                            toPath:documentCategory_1_vn_Path 
                                             error:nil];
    
    
    NSString *category_2_en_Path = [[NSBundle mainBundle] pathForResource:@"category_2_en" ofType:@"png"];
    NSString *documentCategory_2_en_Path = [imagesPath stringByAppendingPathComponent:@"category_2_en.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:category_2_en_Path 
                                            toPath:documentCategory_2_en_Path 
                                             error:nil];
    
    NSString *category_2_vn_Path = [[NSBundle mainBundle] pathForResource:@"category_2_vn" ofType:@"png"];
    NSString *documentCategory_2_vn_Path = [imagesPath stringByAppendingPathComponent:@"category_2_vn.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:category_2_vn_Path 
                                            toPath:documentCategory_2_vn_Path 
                                             error:nil];
    NSString *category_3_en_Path = [[NSBundle mainBundle] pathForResource:@"category_3_en" ofType:@"png"];
    NSString *documentCategory_3_en_Path = [imagesPath stringByAppendingPathComponent:@"category_3_en.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:category_3_en_Path 
                                            toPath:documentCategory_3_en_Path 
                                             error:nil];
    
    NSString *category_3_vn_Path = [[NSBundle mainBundle] pathForResource:@"category_3_vn" ofType:@"png"];
    NSString *documentCategory_3_vn_Path = [imagesPath stringByAppendingPathComponent:@"category_3_vn.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:category_3_vn_Path 
                                            toPath:documentCategory_3_vn_Path 
                                             error:nil];
    
    NSString *factitem_1_en_Path = [[NSBundle mainBundle] pathForResource:@"newFactitem_1_en" ofType:@"png"];
    NSString *documentFacteitem_1_en_path = [imagesPath stringByAppendingPathComponent:@"newFactitem_1_en.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:factitem_1_en_Path 
                                            toPath:documentFacteitem_1_en_path 
                                             error:nil];
    
    NSString *factitem_1_vn_Path = [[NSBundle mainBundle] pathForResource:@"newFactitem_1_vn" ofType:@"png"];
    NSString *documentFacteitem_1_vn_path = [imagesPath stringByAppendingPathComponent:@"newFactitem_1_vn.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:factitem_1_vn_Path 
                                            toPath:documentFacteitem_1_vn_path 
                                             error:nil];
    
    NSString *factitem_2_en_Path = [[NSBundle mainBundle] pathForResource:@"newFactitem_2_en" ofType:@"png"];
    NSString *documentFacteitem_2_en_path = [imagesPath stringByAppendingPathComponent:@"newFactitem_2_en.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:factitem_2_en_Path 
                                            toPath:documentFacteitem_2_en_path 
                                             error:nil];
    
    NSString *factitem_2_vn_Path = [[NSBundle mainBundle] pathForResource:@"newFactitem_2_vn" ofType:@"png"];
    NSString *documentFacteitem_2_vn_path = [imagesPath stringByAppendingPathComponent:@"newFactitem_2_vn.png"];
    
    [[NSFileManager defaultManager] copyItemAtPath:factitem_2_vn_Path 
                                            toPath:documentFacteitem_2_vn_path 
                                             error:nil];
    
    [self saveContext];
}
-(void)resetData{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",COREDATA_FILE_NAME]];
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
}

#pragma mark - Public API
/**
	Save the managed object context
 */
- (void)saveContext
{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

-(void)changeFactResultsControllerWithCategory:(Category *)category{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    NSPredicate *categoryPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@.%@ == %d",ENTITY_FACTITEM_toCategory, ENTITY_CATEGORY_category_id,[category.category_id intValue]]];
    [self.fact_FetchedResultsController setEntityPredicate:categoryPredicate];
}

-(NSString *)getPathOfImageResources{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:IMAGES_DIRECTORY_NAME];
}

@end
