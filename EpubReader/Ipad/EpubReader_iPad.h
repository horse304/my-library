//
//  EpubReader_iPad.h
//  nexto
//
//  Created by Dato - Techmaster.vn on 2012-07-27.
//  Copyright 2012 Dato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"
#import "Downloader.h"
#import "DatoEpubVC.h"
#import "ItemDefinitions.h"
#import "Settings.h"
#import "Library.h"
#import "SettingsPopover.h"
#import "ChaptersListPopover.h"
#import "UIWebViewAdditions.h"
#import "EpubImageZooming.h"
#import "AppDelegate_iPad.h"
#import "EpubShareFacebook.h"

typedef enum epubreadermode{
    READ_EPUB_FROM_FILE,
    READ_EPUB_FROM_URL
} EpubReaderMode;

typedef enum epubaligntype{
    JUSTIFY = 0,
    LEFT = 1,
    CENTER = 2,
    RIGHT = 3
} EpubAlignType;

typedef enum epubfontfamily{
    GEORGIA = 0,
    PALATINO = 1,
    TIMES_NEW_ROMAN = 2,
    VERDANA = 3,
    ARIAL = 4,
    COCHIN = 5,
    APPLEGOTHIC = 6
} EpubFontFamily;

//Dato @ Techmaster.vn
@interface EpubReader_iPad : DatoEpubVC <UIGestureRecognizerDelegate, DatoEpubVCDelegate, SettingsPopoverDelegate, ChaptersListDelegate>

- (id)initWithSetBook:(Book *)bookToOpen splash:(bool)isSplash;
- (id)initWithDownloadBook:(NSMutableDictionary *)item;

@property (nonatomic, retain) Book *book;

@end