//
//  LanguageViewController.m
//  SexFunFacts
//
//  Created by Techmaster on 8/7/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import "LanguageViewController.h"

@interface LanguageViewController ()

@end

@implementation LanguageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Observer notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWhenLanguageChanged) name:NOTIFICATION_Language_Changed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWhenCoreDataChanged) name:NOTIFICATION_COREDATA_UPDATED object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
#ifdef DEBUGX
    NSLog(@"%s",__FUNCTION__);
#endif
    [super viewWillAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


-(void)updateWhenCoreDataChanged{
    
}
-(void)updateWhenLanguageChanged{
    
}

@end
