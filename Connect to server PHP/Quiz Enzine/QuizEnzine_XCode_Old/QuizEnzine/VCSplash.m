//
//  VCSplash.m
//  QuizEnzine
//
//  Created by Techmaster on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VCSplash.h"
#import "TBCAuthentication.h"

@interface VCSplash ()

@end

@implementation VCSplash

- (id)init
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {        
        self = [super initWithNibName:@"VCSplash_iPhone" bundle:nil];
    } else {
        self = [super initWithNibName:@"VCSplash_iPad" bundle:nil];
    }
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated{
    
    TBCAuthentication *tbcAuthentication = [[TBCAuthentication alloc] init];
    tbcAuthentication.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:tbcAuthentication animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
