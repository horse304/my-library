//
//  HTMLUtils.h
//  nexto
//
//  Created by Techmaster on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTMLUtils : NSObject
typedef enum {
    HTMLPartTypeHTML,
    HTMLPartTypeHead,
    HTMLPartTypeBody
} HTMLPartType;
@property (retain, nonatomic) NSString *headPart;
@property (retain, nonatomic) NSString *bodyPart;
@property (retain, nonatomic) NSString *htmlPart;

-(id)initWithHTMLString:(NSString *)html;
-(NSString *)getHTMLString;
-(void)addString:(NSString *)stringToAdd to:(HTMLPartType)partType;
@end