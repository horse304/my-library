//
//  Factitem.h
//  SexFunFacts
//
//  Created by Techmaster on 8/4/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category;

@interface Factitem : NSManagedObject

@property (nonatomic, retain) NSString * fact_content;
@property (nonatomic, retain) NSNumber * fact_id;
@property (nonatomic, retain) NSString * fact_image_path;
@property (nonatomic, retain) NSString * full_fact_image_path;
@property (nonatomic, retain) NSNumber * fact_viewcount;
@property (nonatomic, retain) NSDate   * fact_createdDate;
@property (nonatomic, retain) NSString * language_code;
@property (nonatomic, retain) Category *toCategory;

@end
