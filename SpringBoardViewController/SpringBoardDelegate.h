//
//  SpringBoardDelegate.h
//  SexFunFacts
//
//  Created by Techmaster on 8/6/12.
//  Copyright (c) 2012 Techmaster VN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpringBoardViewController.h"

@class SpringBoardViewController;
@protocol SpringBoardDelegate <NSObject>
@optional
-(void)springBoardDidScrollToPageIndex:(int)pageIndex sender:(SpringBoardViewController *)springBoardVC;
-(void)springBoardGetNumberOfPages:(int)numberPages sender:(SpringBoardViewController *)springBoardVC;
@end
