//
//  QE_UserInfo.h
//  QuizEnzine
//
//  Created by Techmaster on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QE_UserInfo : NSObject
@property (retain, nonatomic) NSString *facebookID;
@property (retain, nonatomic) NSString *username;
@property (retain, nonatomic) NSString *link;
@property (retain, nonatomic) NSString *first_name;
@property (retain, nonatomic) NSString *last_name;
@property (retain, nonatomic) NSString *gender;
@property (retain, nonatomic) NSDate *birthday;
@property (assign, nonatomic) NSInteger timezone;
@property (retain, nonatomic) NSString *email;
@property (retain, nonatomic) NSString *picture;
@property (assign, nonatomic) NSUInteger *numberFollower;
@property (assign, nonatomic) NSUInteger *numberFollowing;

+(QE_UserInfo *)sharedUserInfo;
-(id)initWithId:(NSString *)fbid;
-(NSString *)fullname;
-(NSDictionary *)getDictionary;
@end
