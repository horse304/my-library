//
//  DatoTabBarController.m
//  customTabBar
//
//  Created by Techmaster on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define STANDARD_TABBAR_HEIGHT 49
#import "DatoTabBarController.h"

@interface DatoTabBarController ()
@end

@implementation DatoTabBarController
@synthesize                 customMainBackgroundColor = _customMainBackgroundColor;
@synthesize                    customTabBarBackground = _customTabBarBackground;
@synthesize             customSelectionIndicatorImage = _customSelectionIndicatorImage;
@synthesize              customSelectedImageTintColor = _customSelectedImageTintColor;
@synthesize                     customTabBarTintColor = _customTintColor;
@synthesize                  customTabBarItemTextFont = _customTabBarItemTextFont;
@synthesize         customTabBarItemSelectedTextColor = _customTabBarItemSelectedTextColor;
@synthesize       customTabBarItemUnselectedTextColor = _customTabBarItemUnselectedTextColor;
@synthesize   customTabBarItemSelectedTextShadowColor = _customTabBarItemSelectedTextShadowColor;
@synthesize customTabBarItemUnselectedTextShadowColor = _customTabBarItemUnselectedTextShadowColor;
@synthesize          customTabBarItemTextShadowOffset = _customTabBarItemTextShadowOffset;

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - customize funtions
-(void) makeCustomize{
    [self.view setBackgroundColor:self.customMainBackgroundColor];
    
    [self.tabBar setBackgroundImage:self.customTabBarBackground];
    [self.tabBar setSelectionIndicatorImage:self.customSelectionIndicatorImage];
    [self.tabBar setSelectedImageTintColor:self.customSelectedImageTintColor];
    [self.tabBar setTintColor:self.customTabBarTintColor];
    [self addSeparationForBarItem];
    [self customizeAllTabBarItem];
}
-(void) addSeparationForBarItem{
    
}
-(void) customizeAllTabBarItem{
    for (UITabBarItem *item in self.tabBar.items) {  
        [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  self.customTabBarItemTextFont, UITextAttributeFont,
                       self.customTabBarItemUnselectedTextColor, UITextAttributeTextColor,
                 self.customTabBarItemUnselectedTextShadowColor, UITextAttributeTextShadowColor,
                          self.customTabBarItemTextShadowOffset, UITextAttributeTextShadowOffset,
                                      nil] forState:UIControlStateNormal]; 
        [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                         self.customTabBarItemSelectedTextColor, UITextAttributeTextColor,
                   self.customTabBarItemSelectedTextShadowColor, UITextAttributeTextShadowColor,
                                      nil] forState:UIControlStateSelected];
    }
}

-(void) customizeTabBarItem:(UITabBarItem *)tabBarItem withSelectedImage:(UIImage *)selectedImg unselectedImage:(UIImage *)unselectedImg{
    [tabBarItem setFinishedSelectedImage:selectedImg withFinishedUnselectedImage:unselectedImg];
}

-(void) setViewControllers:(NSArray *)viewControllers{
    [super setViewControllers:viewControllers];
    [self addSeparationForBarItem];
}
-(void) setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated{
    [super setViewControllers:viewControllers animated:YES];
    [self addSeparationForBarItem];
}

#pragma  mark - Encapsulation
-(void) setCustomTabBarBackground:(UIImage *)customTabBarBackground{
    _customTabBarBackground = [customTabBarBackground scaledToSize:CGSizeMake(self.view.bounds.size.width, STANDARD_TABBAR_HEIGHT)];
}
@end
