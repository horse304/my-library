//
//  TwoSideExtend.h
//  TwoSideExtendViewController
//
//  Created by Techmaster on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "LanguageViewController.h"
#define WIDTH_MAINVIEW_DISAPPEAR 60
#define UNDER_VIEWCONTROLLER_WIDTH_ADJUST 5
#define MainViewController_CoreRadius 8
#define MainViewController_ShadowRadius 10
#define MainViewController_ShadowOpacity 0.8

@interface TwoSideExtend : LanguageViewController <UIGestureRecognizerDelegate>

typedef enum{
    ShowingLeft,
    Normal,
    ShowingRight
} TwoSideState;

-(id)initWithLeftViewController:(UIViewController *)leftVC 
            rightViewController:(UIViewController *)rightVC 
             mainViewController:(UIViewController *)mainVC;

@property (assign, nonatomic) BOOL enableTapGesture;
@property (assign, nonatomic) BOOL enablePanGesture;
@property (assign, nonatomic) TwoSideState state;

-(void)animationToNormal;
-(void)animationToLeft;
-(void)animationToRight;

@end
