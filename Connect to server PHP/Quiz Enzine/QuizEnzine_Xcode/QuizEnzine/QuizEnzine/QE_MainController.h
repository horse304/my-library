//
//  QE_MainController.h
//  QuizEnzine
//
//  Created by Techmaster on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QE_AppUserDefault.h"
#import "QE_FacebookController.h"

@interface QE_MainController : NSObject<QE_FacebookControllerDelegate>
@property (strong, nonatomic) QE_AppUserDefault *appUserDefault;
@property (strong, nonatomic) QE_FacebookController *fbController;

+(QE_MainController *)sharedMainController;
-(BOOL)isSessionValid;

#pragma mark - Facebook
-(BOOL)handleOpenURL:(NSURL *)url;
-(void)startLoginFacebook;
@end
