//
//  Shader.fsh
//  temp
//
//  Created by Jake Gundersen on 10/14/11.
//  Copyright (c) 2011 University of Utah Hospitals and Clinics. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
